﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importer
{
    class Program
    {
        public static object FileReader { get; private set; }

        static void Main(string[] args)
        {
            int rowcount = 0;
            int pagecount = 0;
            string insert_filename = "insert_city";

            string filename = Path.Combine(AppContext.BaseDirectory, "Data\\city_master.tsv");
            Console.Write("Importing " + filename);
            string insert_head = "INSERT INTO country(`Code`, `Name`, `DestinationCode`, `CountryCode`) VALUES ";
            string insert = " ('{0}','{1}','{2}','{3}')";

            StringBuilder sb = new StringBuilder();

            foreach (var item in File.ReadLines(filename))
            {
               
                string[] fields = item.Split('\t');

                if (!fields[0].StartsWith("C!")) continue;
                rowcount++;

                if (rowcount > 1) sb.Append(",");
                sb.AppendFormat(insert, Escape(fields[0]), Escape(fields[1]), Escape(fields[2]), Escape(fields[3]), Escape(fields[4]), Escape(fields[5]), Escape(fields[6]), Escape(fields[7]), Escape(fields[8]), Escape(fields[9]), Escape(fields[10]), Escape(fields[11]), Escape(fields[12]));
                sb.Append(Environment.NewLine);

                if (rowcount == 2400)
                {
                    rowcount = 0;
                    pagecount++;
                    sb.Insert(0,insert_head);
                    sb.Append(";");
                    WriteFile(insert_filename + "_" + pagecount + ".sql", sb.ToString());

                    sb = new StringBuilder();
                }
            }

            Console.Write("writing file " + insert_filename + "_" + pagecount + ".sql");
            sb.Insert(0,insert_head);
            sb.Append(";");
            File.WriteAllText(insert_filename + "_" + pagecount + ".sql", sb.ToString());

            Console.Write("done");

        }

        static void WriteFile(string filename,string text)
        {
            File.WriteAllText(filename,text);
        }

        static string Escape(string val)
        {
            return val.Replace("'", "''");
        }
    }
}
