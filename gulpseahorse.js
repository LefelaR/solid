var gulp = require('gulp')
var svn = require('node-svn-ultimate'); //npm install node-svn-ultimate --save


gulp.task('default', ['directories'], () => {});

// gulp.task('install', () => {
//     return svn.commands.checkout('https://ec2-54-154-196-239.eu-west-1.compute.amazonaws.com/svn/LucidOcean-seahorse/seahorse', process.cwd() + '/seahorse', function(err) {
//         console.log("Checkout complete");
//     });
// });

gulp.task('directories', function() {
    return gulp.src('*.*', { read: false })
        .pipe(gulp.dest('./assets'))
        .pipe(gulp.dest('./assets/img'))
        .pipe(gulp.dest('./assets/css'))
        .pipe(gulp.dest('./assets/js'))
        .pipe(gulp.dest('./controllers'))
        .pipe(gulp.dest('./controllers/index'))
        .pipe(gulp.dest('./controllers/index/IndexController.php'))
        .pipe(gulp.dest('./views/index'))
        .pipe(gulp.dest('./views/index/index.php'))
});