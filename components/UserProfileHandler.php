<?php


class UserProfileHandler implements IProfileHandler
{

    public function getAuthCookieName()
    {
        return "crest_auth";
    }

    public function initProfile($auth)
    {
        if (isset($auth) && !empty($auth)) {

            if (isset($_SESSION['profile'])){
                return $_SESSION['profile'];
            }

            $config = new Config("1.0");
            $siteurl = $config->siteurl;

            $url = "authentication/verifytoken";
            $api = new ExternalAPIHandler($siteurl . "api/v1/");

            $data = new stdClass();

            $data->access_token = $auth;

            $profiledata = $api->Post($url,  array('Content-Type:application/json'), $data);
            if (isset($profiledata["Data"]) && (sizeof($profiledata["Data"]) > 0)) {

                if ($profiledata["Data"][0]["access_token"] == $auth) {
                    $_SESSION['profile'] = $profiledata["Data"][0];
                    return $profiledata["Data"][0];
                } else {
                    return "";
                }
            }
        }
        unset($_SESSION['profile']);
        return null;
    }
}
 