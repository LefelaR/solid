<?php

class Email
{
    public function SendEmail($to, $event, $data)
    {
        $messagetemplate = $this->_GetMessageTemplate($event);
        if($messagetemplate == null) return false;

        $emailTemplate = $this->_GetEmailTemplate();

        $body = $this->_PlaceholderReplace($messagetemplate->body, $data);
        $subject = $this->_PlaceholderReplace($messagetemplate->subject, $data);

        $data->body = $body;
        $data->subject = $subject;

        $body = $this->_PlaceholderReplace($emailTemplate, $data);
    
        require_once './components/Helpers/EmailSender.php';

        $emailSender = new EmailSender();

        $success = $emailSender->SendEmail($to, $subject, $body);

        return $success;
    }

    private function _GetMessageTemplate($event)
    {
        $qry = "SELECT * FROM `message_template` WHERE event_Type = $event;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

         if(sizeof($data)  == 1){
            return (object)$data[0];
        }
        return null;
    }

    private function _GetEmailTemplate(){
        return file_get_contents('emailtemplate.html');
    }

    private function _PlaceholderReplace($message,$data){
        foreach($data as $key => $value){
            $field = "{".$key."}";
            $message = str_replace($field,$value,$message);
        }
        return $message;
    }



    // private function _SendEmail($context, $emailType)
    // {
    //     $config = new Config('1.0');

    //     $url = $config->siteurl;

    //     $obj = new stdClass();
    //     $obj->user_name = $context->FormData->first_name;
    //     $obj->date = date("d-m-Y H:i:s");
    //     $obj->cta_text = "Verify my email";
       
    //     $obj->group_name = $context->FormData->company;
    //     useclass("Email", $context);
    //     $em = new Email();
    //     $result = $em->SendEmail($context->FormData->signupEmail, $emailType, $obj);
    //     return $result;
    // }

        //  $to = $body["holder"]["email"];
        // //  $CC = 'Traderoutes@traderoutes.co.za';
        // $CC = "rakheoana.lucidocean@gmail.com"; 
        // $subject = 'A Booking: '.$body["booking_name"].' Has Been Made';
        //  $body ="From:  info@traderoutes.co.za <br /> <br />" ;
        //  $body .= "Email: ".$data->email . "<br /> <br />" ;
        //  $body .= "Your Booking has Been Made <br />" ;
        //  $body .="Click this link bellow to cancel<br />";
        //  $body .="Cancel Booking: <a href='". url('booking/Cancel/') ."'".$model->result["booking_reference"]."> ".$model->result["booking_name"]." Now! </a>" ;
        //  $msg_enc = quoted_printable_encode($body);
        //  $emailSender = new EmailSender();
        //  $success = $emailSender->SendEmail($to, $subject, $body, $CC );  
}