<?php
include 'seahorse/components/SeahorseConfig.php';
class Config extends SeahorseConfig
{

    public function __construct($ver)
    {
        parent::__construct($ver);
    }

    public function InitEnvironment($state)
    {
        if ($state == 'dev') {
            $this->manifest = 'manifest_dev.json';
            $this->siteroot = '/solid/';
            $this->siteurl = 'http://localhost:8080' . $this->siteroot;
        }
        else if($state == 'staging'){
            // must be http because of aws certificate
            // internally https does not resolve
            // returns error:: SSL certificate problem: unable to get local issuer certificate
            $this->siteurl = 'http://staging.ria.co.za' . $this->siteroot;
        }
        else{
            // must be http because of aws certificate
            // internally https does not resolve
            // returns error:: SSL certificate problem: unable to get local issuer certificate
            $this->siteurl = 'http://www.routesintoafrica.co.za/' . $this->siteroot;
        }
    }

    public function GetDatabase($state)
    {
        if ($state == 'live') {
            return array(
                'dbServer' => 'localhost',
                'dbName' => 'ria-web',
                'dbUser' => 'root',
                'dbPassword' => '',
            );
        }

        if($state == 'dev'){
            return array(
                'dbServer' => '127.0.0.1',
                'dbName' => 'ria-web',
                'dbUser' => 'root',
                'dbPassword' => '',
            );
        }
        if ( $state == 'staging'){
            return array(
                'dbServer' => 'localhost',
                'dbName' => 'ria-web',
                'dbUser' => 'root',
                'dbPassword' => '',
            );
        }
    }
}
?>
