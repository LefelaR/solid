 <?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'components/Helpers/PHPMailer/Exception.php';
    require 'components/Helpers/PHPMailer/PHPMailer.php';
    require 'components/Helpers/PHPMailer/SMTP.php';

    class EmailSender
    {
        private $Mailer = null;
        public function __construct()
        {
            $this->Init();
        }

        public function SendEmail($to, $subject, $body)
        {
            $ret = false;
            try {
                //Recipients           
                $this->Mailer->addAddress($to, $to);
                $this->Mailer->addCC('Traderoutes@traderoutes.co.za');
                // $this->Mailer->addBCC($bcc);
                // $this->Mailer->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                // $this->Mailer->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                $this->Mailer->isHTML(true);                                  // Set email format to HTML
                $this->Mailer->Subject = $subject;
                $this->Mailer->Body    = $body;
                $this->Mailer->send();

                $ret = true;
            } catch (Exception $e) {
                //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }
            return $ret;
        }

        private function Init()
        {
            $mail = new PHPMailer(true);                            // Passing `true` enables exceptions
            $mail->SMTPDebug = 0;                                   // Enable verbose debug output
            $mail->isSMTP();                                        // Set mailer to use SMTP
            $mail->Host = 'smtp.office365.com';                     // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                                 // Enable SMTP authentication
            $mail->Username = 'contact@lucidocean.com';             // SMTP username
            $mail->Password = '#LOstr34m';                          // SMTP password
            $mail->SMTPSecure = 'STARTTLS';
            $mail->Port = 587;                                      // TCP port to connect to
            $mail->setFrom('contact@lucidocean.com', 'Lucid Ocean');

            $this->Mailer = $mail;
        }
    }

    ?> 