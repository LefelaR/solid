<?php

class Booking
{
    public $user_id = 0;
    public $booking_id = 0;
    public $group_id = 0;
    public $address_line1 = '';
    public $address_line2 = '';
    public $address_town = '';
    public $address_city = '';
    public $address_post_code = '';
    public $booking_date = '';
    public $booking_end_date = '';
    public $contact_name = '';
    public $contact_email = '';
    public $contact_telephone = '';
    public $booking_notes = '';
    public $booking_inspector_id = 0;

    public static function Get(int $id, int $group_id, bool $returnResult = true, array $filter = null, int $limit = 0, $role = null, $user_id = 0)
    {


        

        $qry = 'SELECT 
                    booking.*, 
                    CONCAT(`user_profile`.`first_name`, " ", `user_profile`.`last_name`) AS inpector_full_name,
                    contact.*,
                    a.* ,
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(a.line1,",",a.line2,",",a.town,",",a.city,",",a.postcode) ELSE CONCAT(a.line1,",",a.town,",",a.city,",",a.postcode) END AS fulladdress                 
                FROM booking
                left join user_profile on user_profile.user_id = booking.inspector_id
                left join address as a on a.address_id = booking.address_id                
                left join contact on contact.contact_id = booking.contact_id';

        $qry .= " where booking.group_id = $group_id AND booking.deleted = 0 ";

        if (isset($filter['booking_date']) && isset($filter['booking_end_date'])) {
            $start = $filter['booking_date'];
            $end = $filter['booking_end_date'];
            $qry .= " and `booking`.`booking_date` between '$start' and '$end'";
            $qry .= " and `booking`.`booking_end_date` between '$start' and '$end'";
        }

        if ($id > 0) {
            $qry .= " and booking.booking_id = $id";
        }

        if (!isset($role)) {
            if ($role == 'inspector') {
                $qry .= " and booking.inspector_id = $user_id";
            }
        }

        $qry .= ' order by booking.booking_end_date desc';

        if ($limit > 0) {
            $qry .= " limit $limit";
        }

        // $obj = ['qry' => $qry , 'role' => $role, 'user_id' => $user_id];
        // return new Result($obj);

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        // foreach ($data as $key => $val) {

        //     $a = $val['fulladdress'];
        //     $a = preg_replace("/\s+/", "+", $a);
        //     $a = preg_replace("/,/", "%2C", $a);
        //     $data[$key]['map'] = $a;
        //     $data[$key]['fulladdress'] = $val['fulladdress'];
        // }

        if ($returnResult === true) {
            if ($id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        } else {
            return $data;
        }
    }


    public static function GetCalendar(int $id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $config = new Config('1.0');

        $url = $config->siteurl . 'admin/bookings/view/';

        $qry = 'SELECT 
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS title,                    
                    booking_date as start,
                    booking_end_date as end,
                    booking_id                    
                FROM booking 
                inner join address on address.address_id = booking.address_id
                ';


        $qry .= " where booking.group_id = $group_id AND booking.deleted = 0";

        if ($id > 0) {
            $qry .= " and booking.booking_id = $id";
        }

        $qry .= ' order by booking.created_date desc';

        $qry = sprintf($qry, $url);

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $d = [];
        foreach ($data as $item) {
            $item['url'] = $url . $item['booking_id'];
            array_push($d, $item);
        }

        $data = $d;

        if ($returnResult === true) {
            if ($id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        } else {
            return $data;
        }
    }

    public static function Save(Booking $model): Result
    {
        $valid = Booking::_Validate($model);

        if ($valid == true) {
            if ($model->booking_id > 0) {
                return Booking::_Update($model);
            }
            return Booking::_Insert($model);
        }
        return new Result('');
    }

    private static function _Validate(Booking $model): bool
    {
        $ret = false;
        if ($model->booking_date && $model->booking_end_date) {
            $qry = "SELECT * FROM booking 
                    WHERE group_id = $model->group_id 
                    AND deleted = 0
                    AND (booking_date BETWEEN '$model->booking_date' AND '$model->booking_end_date'
                    OR booking_end_date BETWEEN '$model->booking_date' AND '$model->booking_end_date')";

            if ($model->booking_id > 0) {
                $qry .= "AND booking_id != $model->booking_id";
            }

            if ($model->booking_inspector_id > 0) {
                $qry .= " AND inspector_id = $model->booking_inspector_id ";
            }

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            $overlappingBooking = sizeof($data) > 0;

            if ($overlappingBooking == false) {
                $start = strtotime($model->booking_date);
                $start = date('d-M-Y', $start);

                $end = strtotime($model->booking_end_date);
                $end = date('d-M-Y', $end);

                if ($start == $end) {
                    $ret = true;
                } else {
                    $result = new ErrorResult("You cannot have a booking that spans over two days or more. Please ensure booking start and end is on the same day.");
                    $result->FlushResult();
                }
            } else {
                $result = new ErrorResult("You cannot have an overlapping booking active. Please choose a different time.");
                $result->FlushResult();
            }
        } else {
            $ret = true;
        }

        return $ret;
    }

    private static function _Update(Booking $model): Result
    {
        $booking = Booking::Get($model->booking_id, $model->group_id, false);
        if (sizeof($booking) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $address_id = $booking[0]['address_id'];
        $contact_id = $booking[0]['contact_id'];

        Booking::_UpdateAddress($model, $address_id);
        Booking::_UpdateContact($model, $contact_id);

        $qry = "UPDATE `booking` SET 
        `booking_date` = '$model->booking_date',
        `booking_end_date` = '$model->booking_end_date', 
        `notes` = '$model->booking_notes',
        `inspector_id` = $model->booking_inspector_id 
        
        WHERE `booking_id` = $model->booking_id;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('booking', $model->booking_id, $model->user_id, 'Booking updated');

        $booking = (object)$booking[0];

        if (strlen($booking->booking_date) > 0) {
            $Notification = new Notification();
            $Notification->Send(
                'Booking updated',
                "Booking for $booking->booking_date was updated. \n$booking->fulladdress",
                $model->user_id,
                "activity",
                $model->group_id,
                Booking::_GetBookingUrl($model->booking_id)
            );
        }

        return Booking::Get($model->booking_id, $model->group_id);
    }

    private static function _GetBookingUrl($booking_id)
    {
        $config = new Config('1.0');
        return $config->siteurl . 'admin/bookings/view/' . $booking_id;
    }

    private static function _UpdateAddress(Booking $model, int $address_id)
    {
        $qry = "UPDATE `address` set `line1` = '$model->address_line1', `line2` = '$model->address_line2', `town` = '$model->address_town', `city` = '$model->address_city', `postcode` = '$model->address_post_code'
        WHERE `address_id` = $address_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    private static function _UpdateContact(Booking $model, int $contact_id)
    {
        $qry = "UPDATE `contact` set `contactname` = '$model->contact_name', `emailaddress` = '$model->contact_email',`telephone`= '$model->contact_telephone' 
        WHERE `contact_id` = $contact_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    private static function _Insert(Booking $model): Result
    {
        $booking = Booking::_CreateBooking($model);
        if ($booking == null) {
            return new ErrorResult("Oops a server error has occurred. Booking could not be created.");
        }
        return new Result($booking);
    }

    private static function _CreateBooking($model)
    {
        $address_id = Booking::_InsertAddress($model);
        $contact_id = Booking::_InsertContact($model);
        $question_group_id = 0;

        if ($address_id == 0) return null;
        if ($contact_id == 0) return null;

        $qry = '';
        if (strlen($model->booking_date) == 0) {
            $qry = "INSERT INTO `booking`(`address_id`,`contact_id`,`group_id`,`booking_date`,`booking_end_date`,`notes`,`inspector_id`)
            VALUES($address_id, $contact_id, $model->group_id, null,null, '$model->booking_notes', $model->booking_inspector_id);
            ";
        } else {
            $qry = "INSERT INTO `booking`(`address_id`,`contact_id`,`group_id`,`booking_date`,`booking_end_date`,`notes`,`inspector_id`)
            VALUES($address_id, $contact_id, $model->group_id, '$model->booking_date','$model->booking_end_date', '$model->booking_notes', $model->booking_inspector_id);
            ";
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $booking_id = $db->ExecStatement($qry);

        if ($booking_id == 0) return null;

        $qry = "INSERT INTO `inspection`(`booking_id`,`group_id`) VALUES ($booking_id, $model->group_id);";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $inspection_id = $db->ExecStatement($qry);

        if ($inspection_id == 0) return null;

        $qry = "UPDATE booking SET inspection_id = $inspection_id WHERE booking_id = $booking_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('booking', $booking_id, $model->user_id, 'Booking created');

        $bookingX = Booking::Get($booking_id, $model->group_id, false);

        $booking = (object)$bookingX[0];

        if (strlen($booking->booking_date) > 0) {
            $Notification = new Notification();
            $Notification->Send(
                'Booking created',
                "Booking for $booking->booking_date was created. \n$booking->fulladdress",
                $model->user_id,
                "activity",
                $model->group_id,
                Booking::_GetBookingUrl($model->booking_id)
            );
        }

        return $bookingX;
    }

    private static function _InsertAddress(Booking $model): int
    {
        $qry = "INSERT INTO `address`(`group_id`, `line1`, `line2`, `town`, `city`, `postcode`) 
        VALUES ($model->group_id, '$model->address_line1', '$model->address_line2', '$model->address_town', '$model->address_city', '$model->address_post_code');";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    private static function _InsertContact(Booking $model): int
    {
        $qry = "INSERT INTO `contact`(`contactname`, `emailaddress`,`telephone`) 
        VALUES ('$model->contact_name','$model->contact_email','$model->contact_telephone');";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    public static function Delete(Booking $model): Result
    {
        $booking = Booking::Get($model->booking_id, $model->group_id, false);

        if (sizeof($booking) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "UPDATE booking SET deleted = 1 WHERE booking_id = $model->booking_id;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('booking', $model->booking_id, $model->user_id, 'Booking deleted');
        $data = ['message' => Message::ObjectDeleted];
        // $Notification = new Notification();
        // $Notification->Send(
        //     'Booking deleted',
        //     "Booking for $booking[0]['booking_date'] was deleted. \n$booking[0]['fulladdress']",
        //     $model->user_id,
        //     "activity",
        //     $model->group_id,
        //     $config->siteurl . 'admin/bookings/'
        // );




        return new Result($data);

        $queries = [
            "DELETE FROM `booking` WHERE `booking_id` = " . $booking[0]['booking_id'],
            "DELETE FROM `address` WHERE `address_id` = " . $booking[0]['address_id'],
            "DELETE FROM `contact` WHERE `contact_id` = " . $booking[0]['contact_id']
        ];

        foreach ($queries as $qry) {
            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);
        }

        $data = [
            Message::ObjectDeleted
        ];


        $inspection = new Inspection();
        $inspection->inspection_id = $booking[0]['inspection_id'];
        $inspection->group_id = $model->group_id;
        $inspection->user_id = $model->user_id;
        $inspection->booking_id = $booking[0]['booking_id'];
        Inspection::Delete($inspection);

        Audit::Write('booking', $model->booking_id, $model->user_id, 'Booking deleted');

        $booking = (object)$booking[0];
        $Notification = new Notification();
        $Notification->Send(
            'Booking deleted',
            "Booking for $booking->booking_date was deleted. \n$booking->fulladdress",
            $model->user_id,
            "activity",
            $model->group_id,
            $config->siteurl . 'admin/bookings/'
        );

        return new Result($data);
    }

    public static function Search($context)
    {

        $qry = "call api_bookings_search('%s',%d)";
        $qry = sprintf($qry, '%' . $context->FormData->searchtext . '%', $context->Profile->group_id);

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    public static function GetInspectors($group_id)
    {
        $qry = "select distinct user_profile.user_id, first_name,last_name from user_group_access
        inner join user_profile on user_profile.user_id = user_group_access.user_id
        WHERE user_group_access.group_id = $group_id and group_role in (1,3)";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    public static function SendNotification($booking_id, $user_id, $group_id, $notification)
    {
        $notification_type = [
            0 => ['emailType' => 'Arriving soon', 'id' => 12],
            1 => ['emailType' => 'Arrived', 'id' => 13],
            2 => ['emailType' => 'Inspection Starts Soon', 'id' => 14],
        ];

        if (array_key_exists($notification, $notification_type) == false) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $booking = Booking::Get($booking_id, $group_id, false);

        $booking = (object)$booking[0];

        $ret = Booking::_SendNotificationEmail($booking, $booking->emailaddress, $notification_type[$notification]['id']);

        if ($ret == true) {
            Audit::Write('Booking', $booking_id, $user_id, $notification_type[$notification]['emailType'] . "Notification sent to $booking->emailaddress");
            $data = ['Message' => "Notification sent to $booking->emailaddress", 'sent' => 1];
        } else {
            $data = ['Message' => "Notification not sent, it's not time yet for the inspection", 'sent' => 0];
        }
        return new Result($data);
    }


    private static function _SendNotificationEmail($model, $to, $emailType)
    {

        $booking_date = $model->booking_date;

        $date = new DateTime($booking_date);
        $now = new DateTime();
        $diff = $date->diff($now);

        if ($diff->d == 0 && $diff->h <= 1 && $diff->i <= 59) {
            require_once './components/Helpers/EmailSender.php';

            $config = new Config('0.1');


            $obj = new stdClass();
            $obj->user_name = $to;
            $obj->date = date("d-m-Y H:i:s");
            $obj->cta_text = "Nova";
            $obj->cta_link = $config->siteurl;
            $obj->address = isset($model->fulladdress) ? $model->fulladdress : '';

            $booking_date =  date('d F Y H:i a', strtotime($booking_date));

            $obj->booking_start = $booking_date;
            $em = new Email();

            $em->SendEmail($to, $emailType, $obj);
            return true;
        } else {
            return false;
        }
    }

    public static function CreateSnagList($agentEmail, $tenantEmail)
    {
        if (strlen($agentEmail) == 0)
            return new ErrorResult('Cannot create snaglist. Agent email was not specified.');
        if (strlen($tenantEmail) == 0)
            return new ErrorResult('Cannot create snaglist. Tenant email was not specified.');

        $account = new Account();
        $agent_id = $account->SignupLimitedAccount($agentEmail);
        if ($agent_id == 0)
            return new ErrorResult('Oops a server error has occurred. Cannot create snaglist');

        $tenant_id = $account->SignupLimitedAccount($tenantEmail);
        if ($tenant_id == 0)
            return new ErrorResult('Oops a server error has occurred. Cannot create snaglist');

        $booking = Booking::_CreateBookingForSnagList();
        if ($booking == null)
            return new ErrorResult('Oops a server error has occurred. Cannot create snaglist');

        $inspection_id = $booking[0]['inspection_id'];
        if ($inspection_id == 0)
            return new ErrorResult('Oops a server error has occurred. Cannot create snaglist');

        Inspection::ChangeInspectionType($inspection_id, 7);

        $sharingModel = new Sharing();
        $sharingModel->context = 'inspection';
        $sharingModel->context_id = $inspection_id;
        $sharingModel->share_emails = [$agentEmail, $tenantEmail];
        $sharingModel->user_id = 0;
        $sharingModel->group_id = 0;
        $sharingModel->readonly = 0;
        Sharing::Insert($sharingModel);

        $data = [
            'message' => "Snag list created and shared with $agentEmail,$tenantEmail"
        ];

        return $data;
    }

    private static function _CreateBookingForSnagList()
    {
        $model = new Booking();
        $booking = Booking::_CreateBooking($model);
        return $booking;
    }

    public static function GetBookingForInspection($inspection_id)
    {
        $qry = "SELECT * FROM booking WHERE inspection_id = $inspection_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1)
            return $data[0];

        return null;
    }
}
