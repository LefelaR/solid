<?php

class Sharing
{
    public $context;
    public $context_id;
    public $share_emails;
    public $user_id;
    public $group_id;
    public $share_id;
    public $readonly;

    public static function Get(Sharing $model)
    {
        $tableName = Sharing::_GetTableName($model->context);
        $columnName = Sharing::_GetColumnName($model->context);

        $qry = "SELECT $tableName.*, CASE WHEN user.user_id > 0 THEN 1 ELSE 0 END as nova_user 
        FROM $tableName  
        left join user on user.email = $tableName.email        
        WHERE $columnName = $model->context_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    public static function CanShare($context, $context_id)
    {
        $qry = '';
        $result = new stdClass();
        $result->canshare = false;
        $result->message = '';

        switch ($context) {
            case 'inspection':
                $qry = "SELECT * FROM inspection WHERE inspection_id = $context_id";
                $config = new Config('1.0');
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);

                if ($data[0]['inspection_type'] == 7) {
                    $result->canshare = true;
                    return $result;
                } else {
                    $qry = "select image.* from inspection 
                    left join image on image.context = 'inspection' and image.context_id = inspection.inspection_id
                    where (inspection_type != 7 OR inspection_type is null) and inspection_id = $context_id";
                }

                break;
            case 'quote':
                $qry = "SELECT * FROM quote_item WHERE quote_id=$context_id";
                break;
            case 'invoice':
                $qry = "SELECT * FROM invoice_item WHERE invoice_id=$context_id";
                break;
        }

        if (strlen($qry) == 0) {
            $result->message = "You cannot share this type of report.";
            return $result;
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) > 0) {
            $result->canshare = true;
        } else {
            $result->message = "You cannot share this $context, please insure you have added at least one item before sharing.";
            return $result;
        }

        return $result;
    }

    public static function Insert(Sharing $model)
    {
        $tableName = Sharing::_GetTableName($model->context);
        $columnName = Sharing::_GetColumnName($model->context);

        $emails = $model->share_emails;

        $count = 0;
        $result = Sharing::CanShare($model->context, $model->context_id);

        if (!$result->canshare) {
            $result = new ErrorResult($result->message);
            $result->FlushResult();
        }

        foreach ($emails as $email) {

            if (strlen($email) == 0) {
                $result = new ErrorResult("Cannot share to a non existing email.");
                $result->FlushResult();
            }

            $qry = "SELECT * FROM `$tableName` WHERE `email` LIKE '$email' AND `$columnName` = $model->context_id;";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            if (sizeof($data) === 0) {
                $token = Sharing::_GenerateShareToken($email);

                $model->token = $token;
                $url = $config->siteurl . "share/$model->context?token=" . $model->token;
                $model->url = $url;

                $account = new Account();
                $user_id = $account->SignupLimitedAccount($email);

                if ($user_id == $model->user_id) {
                    $result = new ErrorResult("You cannot share a $model->context with yourself.");
                    $result->FlushResult();
                }

                if ($user_id == 0) {
                    $result = new ErrorResult("Could not share with $email and error has occurred.");
                    $result->FlushResult();
                }

                    $qry = "INSERT INTO `$tableName`(`token`,`email`,`$columnName`,`url`,`user_id`,`readonly`) values ('$token','$email', $model->context_id,'$url',$user_id, $model->readonly)";



                $config = new Config('1.0');
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);

                Audit::Write($model->context, $model->context_id, $model->user_id, "$model->context was shared with " . $email);

                if ($model->context == 'invoice') {
                    $model->invoice_ref = Sharing::GetInvoiceRef($model->context_id);
                } elseif ($model->context == 'quote') {
                    $model->quote_ref = Sharing::GetQuoteRef($model->context_id);
                } else {
                    $model->address = Sharing::GetInspectionAddress($model->context_id);
                }

                Sharing::_SendShareEmail($model, $email);
                $count++;
            }
            // Send Email
        }

        if ($count > 0) {
            return ['message' => "Shared with $count emails."];
        } else {
            return ['message' => "Shared with 0 emails. Please ensure you have supplied a valid email address."];
        }
    }

    private static function GetInvoiceRef($invoice_id)
    {
        $qry = "SELECT Invoice_Ref FROM invoice where Invoice_id = $invoice_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return sizeof($data) == 1 ? $data[0]['Invoice_Ref'] : '';
    }

    private static function GetQuoteRef($quote_id)
    {
        $qry = "SELECT Quote_Ref FROM quote where Quote_Id = $quote_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return sizeof($data) == 1 ? $data[0]['Quote_Ref'] : '';
    }

    private static function GetInspectionAddress($inspection_id)
    {
        $qry = 'select CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress from booking
        left join address on address.address_id = booking.address_id 
        where booking.inspection_id = 
        ' . $inspection_id;

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return sizeof($data) == 1 ? $data[0]['fulladdress'] : '';
    }

    public static function Delete(Sharing $model)
    {
        $tableName = Sharing::_GetTableName($model->context);
        $columnName = Sharing::_GetColumnName($model->context);

        $qry = "SELECT * FROM $tableName WHERE $columnName = $model->context_id and share_id = $model->share_id;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $shareItem = $db->ExecStatement($qry);

        if (sizeof($shareItem) === 1) {

            $qry = "DELETE FROM $tableName WHERE $columnName = $model->context_id and share_id = $model->share_id;";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            Audit::Write($model->context, $model->context_id, $model->user_id, "$model->context permission revoked for " . $shareItem[0]['email']);

            // TODO: NEED EMAIL to $shareItem[0]['email'] permission revoked

            $email = $shareItem[0]['email'];

            if ($model->context == 'invoice') {
                $model->invoice_ref = Sharing::GetInvoiceRef($model->context_id);
            } else if ($model->context == 'quote') {
                $model->quote_ref = Sharing::GetQuoteRef($model->context_id);
            } else {
                $model->address = Sharing::GetInspectionAddress($model->context_id);
            }


            Sharing::_SendRevoke($model, $email, $model->context);

            return new Result(['message' => Message::ObjectDeleted]);
        }
        return new ErrorResult(Message::ObjectNotFound, 404);
    }

    private static function _SendRevoke(Sharing $model, $to, $context)
    {

        require_once './components/Helpers/EmailSender.php';

        $config = new Config('0.1');

        $obj = new stdClass();
        $obj->user_name = $to;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Nova";
        $obj->address = isset($model->address) ? $model->address : '';
        $obj->invoice_ref = isset($model->invoice_ref) ? $model->invoice_ref : '';
        $obj->quote_ref = isset($model->quote_ref) ? $model->quote_ref : '';

        $url = $config->siteurl;
        $model->url = $url;

        $obj->cta_link = $model->url;
        $em = new Email();

        $emailType = 0;

        switch ($model->context) {
            case 'inspection':
                $emailType = 11;
                break;
            case 'invoice':
                $emailType = 9;
                break;
            case 'quote':
                $emailType = 15;
                break;
        }

        if ($emailType > 0) {
            $em->SendEmail($to, $emailType, $obj);
        }
    }

    private static function _SendShareEmail(Sharing $model, $to)
    {


        require_once './components/Helpers/EmailSender.php';

        $config = new Config('0.1');


        $obj = new stdClass();
        $obj->user_name = $to;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_link = $model->url;
        $obj->address = isset($model->address) ? $model->address : '';
        $obj->invoice_ref = isset($model->invoice_ref) ? $model->invoice_ref : '';
        $obj->quote_ref = isset($model->quote_ref) ? $model->quote_ref : '';
        $em = new Email();

        $emailType = 0;

        switch ($model->context) {
            case 'inspection':
                $emailType = 3;
                $obj->cta_text = "View Inspection";
                break;
            case 'invoice':
                $emailType = 10;
                $obj->cta_text = "View Invoice";
                break;
            case 'quote':
                $emailType = 15;
                $obj->cta_text = "View Quote";
                break;
        }

        if ($emailType > 0) {
            $em->SendEmail($to, $emailType, $obj);
        }
    }

    private static function _GenerateShareToken($email)
    {
        $tokenmid = Sharing::_GenerateRandomString(10);
        return $tokenmid;
    }

    private static function _GenerateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private static function _GetTableName($context)
    {
        $arr = [
            'inspection' => 'inspection_share',
            'invoice' => 'invoice_share',
            'quote' => 'quote_share'
        ];

        return array_key_exists($context, $arr) ? $arr[$context] : $context;
    }

    private static function _GetLimitedUrl($context)
    {
        $arr = [
            'inspection' => buildurl('limited/inspections/view/'),
            'invoice' => buildurl('limited/invoices/view/'),
            'quote' => buildurl('limited/quotes/view/')
        ];

        return array_key_exists($context, $arr) ? $arr[$context] : $context;
    }

    private static function _GetColumnName($context)
    {
        $arr = [
            'inspection' => 'inspection_id',
            'invoice' => 'invoice_id',
            'quote' => 'quote_id'
        ];

        return array_key_exists($context, $arr) ? $arr[$context] : $context;
    }

    public static function ValidateToken($token, $email, $context)
    {

        $tableName = Sharing::_GetTableName($context);
        $columnName = Sharing::_GetColumnName($context);

        $qry = "SELECT * FROM $tableName where token = '$token' and email = '$email'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) == 1) {
            return ['valid' => true];
        }
        return ['valid' => false];
    }

    public static function GetReportInformation($token, $context)
    {
        $tableName = Sharing::_GetTableName($context);
        $columnName = Sharing::_GetColumnName($context);

        $qry = "SELECT * FROM $tableName where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            return $data[0];
        }
        return null;
    }

    public static function CanViewReport($token, $context, $currentUser = ''): bool
    {
        $tableName = Sharing::_GetTableName($context);
        $columnName = Sharing::_GetColumnName($context);

        $qry = "SELECT * FROM $tableName where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            return $data[0]['email'] == $currentUser;
        }
        return false;
    }

    public static function ReportExists($token, $context): bool
    {
        $tableName = Sharing::_GetTableName($context);
        $columnName = Sharing::_GetColumnName($context);

        $qry = "SELECT * FROM $tableName where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return sizeof($data) == 1;
    }

    public function HandleShare($context)
    {
        $token = isset($_REQUEST['token']) ? $_REQUEST['token'] : null;

        if ($token == null)
            enable_authorize('unauthorized');

        $report = $this->GetReportInformation($token, $context);
        if ($report == null) {
            ob_clean();

            Header('Location: ' . buildurl('share/revoked'));
            die();
        }

        $model = new stdClass();
        $model->token = $token;

        $returnUrl = buildurl('share/' . $context) . '?token=' . $token;

        $reportViewUrl = $this->_GetLimitedUrl($context) . $report[$this->_GetColumnName($context)];
        if ($context == 'inspection') {
            if ($report['readonly'] == 0) {

                $inspection_id = $report['inspection_id'];
                $booking = Booking::GetBookingForInspection($inspection_id);
                if (isset($booking)) {
                    $booking_id = $booking['booking_id'];
                    $reportViewUrl = buildurl("admin/bookings/inspections/index/$inspection_id?booking_id=$booking_id&clearprofile=1");
                }
            }
        }
        if ($this->ReportExists($token, $context)) {
            $currentUser = '';
            global $seahorse;
            if (isset($seahorse->profile)) {
                $currentUser = $seahorse->profile->email;
                $currentUserId = $seahorse->profile->user_id;
                $reportUserId = $report['user_id'];

                if ($reportUserId != $currentUser) {
                    $user = User::GetUserForId($reportUserId);
                    if ($user != null) {
                        if ($user[0]['haspassword'] == 0) {
                            setcookie("crest_auth", $user[0]['token'], time() + 60 * 24 * 2, '/');
                            ob_clean();
                            Header('Location: ' . $reportViewUrl);
                            die();
                        }
                    }
                }
            } else {
                $user = User::GetUserForId($report['user_id']);
                if ($user != null) {
                    if ($user[0]['haspassword'] == 0) {
                        setcookie("crest_auth", $user[0]['token'], time() + 60 * 24 * 2, '/');
                        ob_clean();
                        Header('Location: ' . $reportViewUrl);
                        die();
                    }
                }
                ob_clean();
                Header('Location: ' . buildurl("account/signin?returnUrl=$returnUrl"));
                die();
            }

            if ($this->CanViewReport($token, $context, $currentUser)) {
                ob_clean();
                Header('Location: ' . $reportViewUrl);
                die();
            } else {
                ob_clean();
                Header('Location: ' . buildurl("account/signin?returnUrl=$returnUrl"));
                die();
            }
        } else {

            ob_clean();
            Header('Location: ' . buildurl('share/revoked'));
            die();
        }
    }
}
