<?php
use function GuzzleHttp\json_encode;

class  QuoteItem
{

    public $Quote_Id;
    public $Item_Id;
    public $group_id;
    public $user_id;
    public $Description;
    // public $Quantity;
    public $Price;

    public static function Get(int $Quote_Id, int $Item_Id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                        `quote_item`.`Item_Id`,
                        `quote_item`.`Quote_Id`,
                        `quote_item`.`Description`,
                        `quote_item`.`Price`
                FROM `quote_item`
                left join quote on quote.Quote_Id = quote_item.Quote_Id';


        $qry .= " where `quote_item`.`Deleted` = 0 ";


        $qry .= " and quote_item.Quote_Id = $Quote_Id";

        if ($Item_Id > 0) {
            $qry .= " and quote_item.Item_Id = $Item_Id";
        }


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($Item_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }


    public static function GetReport($token)
    {
        if ($token === null)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $qry = "SELECT * from quote_share where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) == 0)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $Quote_Id = $data[0]['quote_id'];

        $qry = 'SELECT 
                        `quote_item`.`Item_Id`,
                        `quote_item`.`Quote_Id`,
                        `quote_item`.`Description`,
                        `quote_item`.`Price`
                FROM `quote_item`
                left join quote on quote.Quote_Id = quote_item.Quote_Id';


        $qry .= " where `quote_item`.`Deleted` = 0 ";


        $qry .= " and quote_item.Quote_Id = $Quote_Id";


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    public static function Save(QuoteItem $model)
    {
        if ($model->Item_Id > 0) {
            return QuoteItem::_Update($model);
        }
        return QuoteItem::_Insert($model);
    }
    private static function _Insert(QuoteItem $model)
    {

        // $qry = "call api_insert_item(%d,'%s',%d)";
        // $qry = sprintf($qry, $model->Quote_Id, $model->Description, $model->Price);


        $qry = "
        INSERT INTO `quote_item`(`Quote_Id`,`Description`,`Price`)
        VALUES ($model->Quote_Id, '$model->Description', '$model->Price');
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $Item_Id = $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM quote_item where Quote_Id = $model->Quote_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

        $qry3 = "UPDATE `quote` SET `Total_Amount` =  $total WHERE `Quote_Id` = '$model->Quote_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectCreated]);
    }


    private static function _Update(QuoteItem $model)
    {
        $qry = "UPDATE `quote_item` SET `Description` = '$model->Description', `Price` = '$model->Price'
        WHERE `Item_Id` = $model->Item_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM quote_item where Quote_Id = $model->Quote_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

     
        $qry3 = "UPDATE `quote` SET `Total_Amount` =  $total WHERE `Quote_Id` = '$model->Quote_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectUpdated]);
    }


    public static function Delete(QuoteItem $model)
    {
        $quote = QuoteItem::Get($model->Quote_Id, $model->Item_Id, $model->group_id, false);
        if (sizeof($quote) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "DELETE FROM `quote_item` WHERE `Item_Id` = $model->Item_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM quote_item where Quote_Id = $model->Quote_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

        if ($total == "") {
            $total = 0;
        }


        $qry3 = "UPDATE `quote` SET `Total_Amount` =  $total WHERE `Quote_Id` = '$model->Quote_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectDeleted]);
    }
}
