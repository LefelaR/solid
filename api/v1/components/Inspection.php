<?php
class Inspection
{

    public $booking_id = 0;
    public $inspection_id = 0;
    public $group_id = 0;
    public $user_id = 0;
    public $template_type = 0;
    public $inspection_type = 0;
    public $inspection_name = 0;

    public static function Get(int $booking_id, int $inspection_id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'select 
            `inspection`.`inspection_id`,
            iStatus.status as `inspection_status`,    
            iStatus.status as `inspection_status`,    
            `inspection`.`inspection_status` as `inspection_status_id`,
            `inspection`.`template_type`,
            `inspection`.`booking_id`,
            `inspection`.`group_id`,
            iType.type as `inspection_type`,
            `inspection`.`inspection_type` as `inspection_type_id`,
            `inspection`.`inspection_name`,
            `inspection`.`created_date`
        
            from inspection
            left join inspection_status iStatus on iStatus.value = inspection.inspection_status
            left join inspection_type iType on iType.value = inspection.inspection_type
        ';
        //I have set group id to zero so that I can get the inspection for a snaglist linked to a booking without a group
        $qry .= " where (inspection.group_id = $group_id or inspection.group_id = 0) and inspection.booking_id = $booking_id";

        if ($inspection_id > 0) {
            $qry .= " and inspection.inspection_id = $inspection_id";
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($inspection_id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }

    public static function Save(Inspection $model)
    {
        if ($model->inspection_id > 0) {
            return Inspection::_Update($model);
        }
        return Inspection::_Insert($model);
    }

    private static function _Insert(Inspection $model)
    {
        $inspection = Inspection::Get($model->booking_id, 0, $model->group_id, false);
        if (sizeof($inspection) >= 1) {
            return new ErrorResult('You can only have 1 inspection per booking', 400);
        }

        $inspection_id = Inspection::CreateInspection($model);

        return Inspection::Get($model->booking_id, $inspection_id, $model->group_id);
    }

    public static function CreateInspection($model)
    { 
        $qry = "
            INSERT INTO `inspection`(`group_id`,`template_type`,`booking_id`,`inspection_name`, `inspection_type`)
            VALUES ($model->group_id, $model->template_type, $model->booking_id, '$model->inspection_name', $model->inspection_type);
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $inspection_id = $db->ExecStatement($qry);

        Audit::Write('inspection', $inspection_id, $model->user_id, 'Inspection added');
        return $inspection_id;
    }

    private static function _Update(Inspection $model)
    {
        $qry = "UPDATE `inspection` SET `template_type` = $model->template_type, `inspection_name`  = '$model->inspection_name', `inspection_type` = $model->inspection_type
        WHERE `inspection_id` = $model->inspection_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        Audit::Write('inspection', $model->inspection_id, $model->user_id, 'Inspection updated');

        return Inspection::Get($model->booking_id, $model->inspection_id, $model->group_id);
    }

    public static function Delete(Inspection $model)
    {
        $inspection = Inspection::Get($model->booking_id, $model->inspection_id, $model->group_id, false);
        if (sizeof($inspection) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "DELETE FROM `inspection` WHERE `inspection_id` = $model->inspection_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        ImageHandler::DeleteFor('inspection', $model->inspection_id, $model->user_id, $model->group_id);

        Audit::Write('inspection', $model->inspection_id, $model->user_id, 'Inspection deleted');

        return new Result(['message' => Message::ObjectDeleted]);
    }

    public static function GetStatus(string $status = '', $returnResult = true)
    {
        $qry = "SELECT * FROM `inspection_status`";

        if (strlen($status) > 0) {
            $qry .= " WHERE `status` = '$status'";
        }

        $qry .= " ORDER BY `value` ASC";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            return new Result($data);
        }
        return $data;
    }

    public static function GetTypes(string $status = '', $returnResult = true)
    {
        $qry = "SELECT * FROM `inspection_type`";

        if (strlen($status) > 0) {
            $qry .= " WHERE `status` = '$status'";
        }

        $qry .= " ORDER BY `value` ASC";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            return new Result($data);
        }
        return $data;
    }

    public static function ChangeStatus($booking_id, $inspection_id, $group_id, $user_id, $status)
    {
        $statusDetail = Inspection::GetStatus($status, false);
        if (sizeof($statusDetail) === 0) {
            return ErrorResult("Invalid status provided", 400);
        }

        if ($inspection_id > 0) {

            $status_value = $statusDetail[0]['value'];

            $qry = "UPDATE `inspection` SET `inspection_status` = $status_value WHERE `inspection_id` = $inspection_id and `booking_id` = $booking_id;";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            $status = ucfirst($status);

            Audit::Write('inspection', $inspection_id, $user_id, "Inspection status changed to: $status");

            return Inspection::Get($booking_id, $inspection_id, $group_id);
        }
        return new ErrorResult("[inspection_id] is required.", 400);
    }


    public static function GetReport($token)
    {
        if ($token === null)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $qry = "SELECT * FROM inspection_share 
        left join image on image.context_id = inspection_id and image.context = 'inspection' and image.uploaded = 1 and asset_type =2
        where token = '$token';";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $ret = [];
        if (sizeof($data) > 0) {
            foreach ($data as $item) {
                if ($item['uploaded'] == 1) {
                    $key = $item['s3_bucket_key'];
                    $bucketUtility = new BucketUtility();
                    $s3Url = $bucketUtility->GetFile($key);
                    $item['file_url'] = $s3Url;

                    $thumbnail_url = '';
                    if ($item['thumbnail_image_id'] > 0 && isset($item['thumbnail_s3_key'])) {
                        $key = $item['thumbnail_s3_key'];
                        $s3Url = $bucketUtility->GetFile($key);
                        $thumbnail_url = $s3Url;
                    }

                    $item['thumbnail_url'] = $thumbnail_url;

                    array_push($ret, $item);
                } else {
                    array_push($ret, $item);
                }
            }
        }

        return new Result($ret);
    }

    public static function GetInspectionFor($id)
    {
        $qry = "SELECT * FROM inspection WHERE inspection_id = $id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) return (object)$data[0];
        return null;
    }

    public static function GetInspectionShare(int $inspection_id, int $group_id, $user_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                    inspection_share.*,
                    ins2.email as fromemail,
                    inspection.*,
                    company.*,
                    booking.*, 
                    contact.*,
                    address.*

                FROM inspection_share
                join inspection_share as ins2 on ins2.inspection_id = inspection_share.inspection_id
                join inspection on inspection.inspection_id = inspection_share.inspection_id
                left join booking on booking.booking_id = inspection.booking_id
                left join address on address.address_id = booking.address_id

                left join company on company.group_id = booking.group_id
                left join contact on contact.contact_id = booking.contact_id'; 

        $qry .= " where booking.deleted = 0 and inspection_share.user_id = $user_id and inspection_share.email != ins2.email";
$qry .= " union ";
        $readonly = "SELECT
        inspection_share.*,
        company.company_email as fromemail,
        inspection.*,
        company.*,
        booking.*,
        contact.*,
        address.*
    FROM inspection_share
    left join inspection on inspection.inspection_id = inspection_share.inspection_id
    left join booking on booking.booking_id = inspection.booking_id
    left join address on address.address_id = booking.address_id
    left join company on company.group_id = booking.group_id
    left join contact on contact.contact_id = booking.contact_id 
    where booking.deleted = 0 and inspection_share.user_id = $user_id and readonly = 1";

$qry .= $readonly;

        if ($inspection_id > 0) {
            $qry .= " and inspection_share.inspection_id = $inspection_id";
        }
        // $qry .= ' order by inspection_share.sharedate desc';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $logo_id = $data[0]['logo_id'];
            $group_inspection_id = $data[0]['group_id'];
            $avatar = Company::_GetCompanyLogo($logo_id, $group_inspection_id);
            $data[0]['logo'] = $avatar['logo'];
        }

        if ($returnResult === true) {
            if ($inspection_id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }

    public static function GetGroupForInspection($inspection_id)
    {
        if(!isset($inspection_id)) return 0;
        
        $qry = "SELECT `group_id` FROM inspection WHERE inspection_id = $inspection_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1)
            return $data[0]['group_id'];
        return null;
    }

    public static function ChangeInspectionType($inspection_id, $inspectionType)
    {
        if($inspection_id == 0) return; 
        
        $qry = "UPDATE `inspection` SET `inspection_type` = $inspectionType WHERE `inspection_id` = $inspection_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    public static function completeInspection($model)
    {  
      
        $qry = "UPDATE `inspection_share` SET `locked` = 1 WHERE `inspection_id` = $model->inspection_id;";
        $qry2 = "SELECT `inspection_share`.`user_id`, `inspection`.`booking_id` FROM inspection_share 
                JOIN inspection on inspection.`inspection_id` = inspection_share.`inspection_id`
                WHERE inspection_share.`inspection_id` = $model->inspection_id AND inspection_share.`user_id` <> $model->user_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $config = new Config('1.0');
        $userData = new DataService($config->dbConnection);
        $user = $userData->ExecStatement($qry2);
        $user_id_sendNotification = $user[0]["user_id"];
        $booking_id = $user[0]["booking_id"];

            $Notification = new Notification();
            $Notification->Send(
                'Inspection locked',
                "The inspection shared with you has been locked",
                $user_id_sendNotification,
                "Inspection",
                "0",
                "/admin/bookings/inspections/index/$model->inspection_id?booking_id=$booking_id"
            );

            Audit::Write('inspection', $model->inspection_id, $model->user_id, 'Inspection uncompleted');
 
            return new Result(['message' => Message::ObjectUpdated]);
    }

    public static function uncompleteInspection($model)
    {
        $qry = "UPDATE `inspection_share` SET `locked` = 0 WHERE `inspection_id` = $model->inspection_id;";
        $qry2 = "SELECT `inspection_share`.`user_id`, `inspection`.`booking_id` FROM inspection_share 
                JOIN inspection on inspection.`inspection_id` = inspection_share.`inspection_id`
                WHERE inspection_share.`inspection_id` = $model->inspection_id AND inspection_share.`user_id` <> $model->user_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $config = new Config('1.0');
        $userData = new DataService($config->dbConnection);
        $user = $userData->ExecStatement($qry2);
        $user_id_sendNotification = $user[0]["user_id"];
        $booking_id = $user[0]["booking_id"];

            $Notification = new Notification();
            $Notification->Send(
                'Inspection unlocked',
                "The inspection shared with you has been unlocked",
                $user_id_sendNotification,
                "Inspection",
                "0",
                "/admin/bookings/inspections/index/$model->inspection_id?booking_id=$booking_id"
            );

        Audit::Write('inspection', $model->inspection_id, $model->user_id, 'Inspection uncompleted');

        return new Result(['message' => Message::ObjectUpdated]);

    }
}
