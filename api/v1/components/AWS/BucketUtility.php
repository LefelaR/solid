<?php

// Before you can use this you must install composer and the AWS SDK with composer
// Download composer: https://getcomposer.org/download/
// Install AWS SDK package with composer: composer require aws/aws-sdk-php

require 'vendor/autoload.php';

class BucketUtility
{

    private $_AccessKey = '';
    private $_AccessSecret = '';
    private $_Region = 'eu-west-1'; // Ireland
    private $_Bucket = '';
    private $_Client = null;

    public function __construct()
    {
        $this->_AccessKey = 'AKIAQZRROZQDEHWSFZVA';
        $this->_AccessSecret = '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';
        $this->_Region = 'eu-west-1';
        $this->_Bucket = 'prop-web';
    }

    public function GetFile(string $key, bool $generateUrl = true)
    {
        if ($generateUrl === true) {
            return $this->_GenerateFileUrl($key);
        }
        return $this->_GetRealFile($key);
    }

    private function _GenerateFileUrl(string $key)
    {
        $s3Client = $this->_GetS3Client();

        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => $this->_Bucket,
            'Key' => $key            
        ]);

        $request = $s3Client->createPresignedRequest($cmd, '+20 minutes');
        $presignedUrl = (string)$request->getUri();
        return $presignedUrl;
    }

    private function _GetRealFile(string $key)
    {
        if ($this->FileExists($key) === false) return null;

        $s3Client = $this->_GetS3Client();

        $result = $s3Client->getObject([
            'Bucket'                     => $this->_Bucket,
            'Key'                        => $key,
            'ResponseContentType'        => 'text/plain',
            'ResponseContentLanguage'    => 'en-US',
            'ResponseContentDisposition' => 'attachment; filename=testing.txt',
            'ResponseCacheControl'       => 'No-cache',
            'ResponseExpires'            => gmdate(DATE_RFC2822, time() + 3600),
        ]);

        header("Content-Type: {$result['ContentType']}");
        $data = ['data' => $result['Body']];
        return $data;
    }

    public function UploadFile(string $key, string $fullFileName)
    {
        $s3Client = $this->_GetS3Client();
        $data = [
            'Bucket' => $this->_Bucket,
            'Key'    => $key,
            'Body' => $fullFileName, // absolutePath,
            'acl' => 'public-read'
        ];

        $result = $s3Client->putObject($data);

        return $this->GetFile($key);
    }

    public function DeleteFile(string $key)
    {
        if ($this->FileExists($key) === false) return null;

        $s3Client = $this->_GetS3Client();

        $data = [
            'Bucket' => $this->_Bucket,
            'Key'    => $key
        ];

        $result = $s3Client->deleteObject($data);

        return $result;
    }

    public function FileExists($key)
    {
        $s3Client = $this->_GetS3Client();

        return $s3Client->doesObjectExist($this->_Bucket, $key);
    }

    private function _GetS3Client()
    {
        $s3Client = null;

        if ($this->_Client == null) {
            $options = [
                'region'  => $this->_Region,
                'version' => 'latest',
                'credentials' => [
                    'key'    => $this->_AccessKey,
                    'secret' => $this->_AccessSecret,
                    'acl' => 'public-read'
                ]
            ];

            $s3Client = new Aws\S3\S3Client($options);
            $this->_Client = $s3Client;
        } else {
            $s3Client = $this->_Client;
        }

        return $s3Client;
    }
}
