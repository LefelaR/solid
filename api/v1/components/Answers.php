<?php

class Answers
{
    public static function Get(int $question_id, $answer_id, int $group_id, array $filter = null): Result
    {
        $qry = 'select * from answer';

        $qry .= " where group_id = $group_id and question_id = $question_id";

        if ($answer_id > 0) {
            $qry .= " and answer_id = $answer_id";
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($answer_id > 0 && (sizeof($data) == 0)) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }
        return new Result($data);
    }

    public static function Save($id, $group_id, $user_id): Result
    {
        return new Result('');
    }
}
