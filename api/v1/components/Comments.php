<?php



class Comments
{

    static function get($ctx)
    {

        $project_id = $ctx->Route->id[0];
        $comment_id = $ctx->Route->id[1];
        $iteration_id = 0;
        if (isset($ctx->Filter->Options->iteration_id)) {
            $iteration_id = $ctx->Filter->Options->iteration_id;
        }

        $activity_id = 0;
        if (isset($ctx->Route->id[1])) {
            $activity_id = $ctx->Route->id[1];
        }

        $config = new Config('1.0');

        $qry = "select `activity_comments`.* from activity_comments
        join activity on `activity`.`activity_id` = `activity_comments`.`activity_id`   
        left join `user_profile` on `user_profile`.`user_id` = `activity`.`user_id`
        where `activity`.`project_id`= " . $project_id . "
                "; //$context->Route->id[0]

        if($comment_id>0){
            $qry.=" and comment_id = $comment_id ";
        }

         if ($iteration_id > 0) {
            $qry .= " and iteration_id = $iteration_id ";
        }

        if ($activity_id > 0) {
            $qry .= " and `activity_comments`.activity_id = $activity_id ";
        }
      

        $qry .= ' order by `activity`.`created_date` DESC';

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }




    static function save($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $qry = "INSERT INTO activity_comments( `activity_id`, `user_id`, project_id , `comment`) VALUES ( %d, %d, %d,'%s');";
    
        // $iteration_id = 0;
        // if (isset($ctx->FormData->iteration_id)) {
        //     $iteration_id = $ctx->FormData->iteration_id;
        //     $qry = "INSERT INTO activity( `project_id`,`iteration_id` ,`title`, `description`,`user_id`) VALUES ( %d, %d, '%s', '%s', %d);";
        //     $qry = sprintf($qry, $project_id, $iteration_id, $title, $description, $user_id);
        // } else {
        //     $qry = sprintf($qry, $project_id, $title, $description, $user_id);
        // }
        $qry = sprintf($qry, $ctx->FormData->activity_id, $ctx->Profile->user_id, $ctx->FormData->project_id,$ctx->FormData->comment);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }


    static function delete($id = 0)
    {
        $config = new Config('1.0');
        $qry = "DELETE FROM `activity_comments` WHERE `comment_id` = " . $id;
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }
}
