<?php

class UtilityHelper
{
    public static function MapAndCleanPostData($source, $dest)
    {
        
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        foreach ($dest as $key => $value) {

            $postValue = isset($source->$key) ? $source->$key : '';
            $postValue = mysqli_real_escape_string($db->conn, $postValue);

            $dest->$key = $postValue;
        }
        return $dest;
    }
}
