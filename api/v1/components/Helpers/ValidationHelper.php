
<?php

class ValidationHelper
{

    public static function Assert($propertyName, $value)
    {
        if (!isset($value)) {
            $result = new ErrorResult("[$propertyName] was expected but not defined.");
            $result->FlushResult();
        }
    }

    public static function AssertContext($validationContext)
    {
        foreach ($validationContext as $item => $value) {
            Validation::Assert($item, $value);
        }
    }
}

?>