<?php
class FileHandler
{
    public $asset_id;
    public $group_id;
    public $user_id;
    public $context;
    public $context_id;
    public $file_contents;
    public $file_name;
    public $file_type;

    public static function Proxy($asset_id, $group_id)
    {
        $asset = FileHandler::Get($asset_id, $group_id, false);

        $path = $asset[0]['file_url'];
        $mime = $asset[0]['file_type'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = "data:$mime;base64," . base64_encode($data);

        $ret = [
            'base_64' => $base64
        ];

        return new Result($ret);
    }
    public static function Get($asset_id, $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = "SELECT * FROM `asset` WHERE `group_id` = $group_id ";

        if ($asset_id > 0) {
            $qry .= " AND `asset_id` = $asset_id";
        }

        if ($filter != null) {
            foreach ($filter as $key => $value) {
                $qry .= " AND $key = '$value' ";
            }
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);


        $ret = [];

        if (sizeof($data) > 0) {
            foreach ($data as $item) {
                if ($item['uploaded'] == 1) {
                    $key = $item['s3_bucket_key'];
                    $bucketUtility = new BucketUtility();
                    $s3Url = $bucketUtility->GetFile($key);
                    $item['file_url'] = $s3Url;

                    array_push($ret, $item);
                } else {
                    array_push($ret, $item);
                }
            }
        }

        if ($returnResult === true) {
            if ($asset_id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($ret);
        }
        return $ret;
    }

    public static function RegisterFile(FileHandler $model)
    {
        $s3BucketKey = FileHandler::_GenerateS3Key($model);

        $qry = "INSERT INTO `asset`(`context`, `context_id`, `group_id`, `s3_bucket_key`, `file_type`, `asset_type`)
        VALUE('$model->context',$model->context_id, $model->group_id, '$s3BucketKey','$model->file_type', $model->asset_type)";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('asset', $data, $model->user_id, 'Asset registered');

        return FileHandler::Get($data, $model->group_id);
    }

    private static function _GenerateS3Key(FileHandler $model)
    {
        $extension = substr($model->file_name, strrpos($model->file_name, '.'));

        $date = new DateTime();
        $generatedFileName = (string)$date->getTimestamp() . FileHandler::_GenerateRandomString(5) . $extension;

        return "$model->group_id/$model->context/$model->context_id/$generatedFileName";
    }

    private static function _GenerateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function UploadFile(FileHandler $model)
    {
        $contents = substr($model->file_contents, strpos($model->file_contents, ',') + 1);
        $contents = base64_decode($contents);

        $fileInformation = FileHandler::Get($model->asset_id, $model->group_id, false);

        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $key = $fileInformation[0]['s3_bucket_key'];
        $bucketUtility = new BucketUtility();
        $data = $bucketUtility->UploadFile($key, $contents);

        $qry = "UPDATE `asset` SET `uploaded` = 1, `text` = '$model->text' WHERE `asset_id` = $model->asset_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $fileInformation[0]['file_url'] = $data;

        Audit::Write('asset', $model->asset_id, $model->user_id, 'Asset uploaded');
        return new Result($fileInformation);
    }

    public static function Delete($asset_id, $user_id, $group_id)
    {
        $fileInformation = FileHandler::Get($asset_id, $group_id, false);
        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $key = $fileInformation[0]['s3_bucket_key'];

        $bucketUtility = new BucketUtility();
        if ($bucketUtility->FileExists($key) === true) {
            $bucketUtility->DeleteFile($key);
        }

        if ($bucketUtility->FileExists($key) === false) {

            $qry = "DELETE FROM `asset` WHERE `asset_id` = $asset_id";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            Audit::Write('asset', $asset_id, $user_id, 'Asset deleted');
            return new Result(['message' => Message::ObjectDeleted]);
        }

        if (sizeof($fileInformation) === 0) {
            return new ErrorResult('Asset Not deleted', 500);
        }
    }
}
