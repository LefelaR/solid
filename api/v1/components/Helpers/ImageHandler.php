<?php
class ImageHandler
{
    public $image_id;
    public $group_id;
    public $user_id;
    public $context;
    public $context_id;
    public $file_contents;
    public $file_name;
    public $file_type;
    public $original_image_id;
    public $asset_type;

    public static function Proxy($image_id, $group_id)
    {
        $image = ImageHandler::Get($image_id, $group_id, false);

        $path = $image[0]['file_url'];
        $mime = $image[0]['file_type'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = "data:$mime;base64," . base64_encode($data);

        $ret = [
            'base_64' => $base64
        ];

        return new Result($ret);
    }

    public static function Get($image_id, $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = "select image.*, a.s3_bucket_key as thumbnail_s3_key from image
        left join image as a on a.image_id = image.thumbnail_image_id ";


        // $where = 'WHERE `image`.`asset_type` in (2,4)';
        $where = '';

        // This has been cpmmented out to let the tenant to be abble to delete the image regardless the group [for ROB]

        // if ($group_id > 0) {
        //     $where = "WHERE `image`.`group_id` = $group_id";
        // }

        // if ($image_id > 0) {
        //     $where .= " AND `image`.`image_id` = $image_id";
        // }

        // commetnt the code bellow [FOR DOMONIQUE]
        if ($image_id > 0) {
            $where .= " WHERE  `image`.`image_id` = $image_id";
        } else {
            $where .= " WHERE `image`.`asset_type` in (2,4) ";
        }

        ////////////////////////////////////
        if ($filter != null) {
            foreach ($filter as $key => $value) {
                if ($key != 'original_image_id') {
                    $where .= " and `image`.$key = '$value' ";
                }
                //  else {
                //     $where .= " and `image ` .`original_image_id` > 0 ";
                // }
            }
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry . $where);

        $ret = [];

        if (sizeof($data) > 0) {
            foreach ($data as $item) {
                $item['text'] = stripslashes($item['text']);
                $item['tags'] = ImageHandler::_ProcessTags($item['tags']);
                if ($item['uploaded'] == 1) {
                    $key = $item['s3_bucket_key'];
                    $bucketUtility = new BucketUtility();
                    if (strlen($key)) {
                        $s3Url = $bucketUtility->GetFile($key);
                        $item['file_url'] = $s3Url;

                        $thumbnail_url = '';
                        if ($item['thumbnail_image_id'] > 0 && isset($item['thumbnail_s3_key'])) {
                            $key = $item['thumbnail_s3_key'];
                            $s3Url = $bucketUtility->GetFile($key);
                            $thumbnail_url = $s3Url;
                        }

                        $item['thumbnail_url'] = $thumbnail_url;
                    }

                    array_push($ret, $item);
                } else {
                    array_push($ret, $item);
                }
            }
        }

        if ($returnResult === true) {
            if ($image_id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($ret);
        }
        return $ret;
    }

    private static function _ProcessTags($tags)
    {
        $tags = trim($tags);
        $arr = explode(' ', $tags);
        $processedArr  = [];
        foreach ($arr as $item) {
            if (strlen($item) > 0) {
                $item = trim($item);
                $item = "#" . $item;
                array_push($processedArr, $item);
            }
        }
        return implode(' ', $processedArr);
    }

    private static function _ProcessTagsToSave($tags)
    {
        $tags = str_replace(',',' ',$tags);
        $arr = explode(' ', $tags);
        $processedArr  = [];
        foreach ($arr as $item) {
            if (strlen($item) > 0) {
                $item = trim($item);
                if (startsWith($item, '#')) {
                    $item = substr($item, 1);
                }
                array_push($processedArr, $item);
            }
        }
        return implode(' ', $processedArr);
    }

    public static function RegisterFile(ImageHandler $model, bool $returnResult = true)
    {
        $s3BucketKey = ImageHandler::_GenerateS3Key($model);

        $order = ImageHandler::GetNextOrder($model->context_id);

        $qry = "INSERT INTO `image`(`context`, `context_id`, `group_id`, `s3_bucket_key`, `file_type`, `asset_type`,`original_image_id`,`order`)
        VALUE('$model->context',$model->context_id, $model->group_id, '$s3BucketKey','$model->file_type', $model->asset_type, $model->original_image_id, $order)";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        // Audit::Write('image', $data, $model->user_id, 'image registered');


        return ImageHandler::Get($data, $model->group_id, $returnResult);
    }

    private static function _GenerateS3Key(ImageHandler $model)
    {
        $config = new Config('1.0');

        $extension = substr($model->file_name, strrpos($model->file_name, '.'));

        $date = new DateTime();
        $generatedFileName = (string) $date->getTimestamp() . ImageHandler::_GenerateRandomString(5) . $extension;

        return "$config->state/$model->group_id/$model->context/$model->context_id/$generatedFileName";
    }

    private static function _GenerateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function UploadFile(ImageHandler $model, bool $uploadThumbnail = true)
    {
        $contents = substr($model->file_contents, strpos($model->file_contents, ',') + 1);
        $contents = base64_decode($contents);

        $fileInformation = ImageHandler::Get($model->image_id, $model->group_id, false);

        if ($uploadThumbnail === true) {
            //  ImageHandler::_UploadThumbnail($model, $fileInformation, $contents);
        }

        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $key = $fileInformation[0]['s3_bucket_key'];
        $bucketUtility = new BucketUtility();
        $data = $bucketUtility->UploadFile($key, $contents);

        $qry = "UPDATE `image` SET `uploaded` = 1, `text` = '$model->text' WHERE `image_id` = $model->image_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $fileInformation[0]['file_url'] = $data;

        Audit::Write($fileInformation[0]['context'], $fileInformation[0]['context_id'], $model->user_id, 'Image uploaded');
        return ImageHandler::Get($model->image_id, $model->group_id, true);
    }

    private static function _UploadThumbnail($model, $file, $contents)
    {
        $thumbnailImage = ImageHandler::_GenerateThumbnail($file, $contents);
        $model->file_contents = $thumbnailImage;


        if ($file[0]['thumbnail_image_id'] > 0) {
            ImageHandler::UploadFile($model, false);
        } else {
            $model->file_type = 'image/png';
            $model->original_image_id = 0;
            $model->asset_type = 3;
            $model->file_name = 'thumb.png';
            $model->context = $file[0]['context'];
            $model->context_id = $file[0]['context_id'];
            $data = ImageHandler::RegisterFile($model, false);
            $thumbnail_image_id = $data[0]['image_id'];
            $qry = "UPDATE `image` SET thumbnail_image_id=$thumbnail_image_id WHERE `image_id` = $model->image_id;";
            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $db->ExecStatement($qry);

            $model->image_id = $data[0]['image_id'];
            ImageHandler::UploadFile($model, false);
        }
    }

    private static function _GenerateThumbnail($file, $contents)
    {
        $image = imagecreatefromstring($contents);

        $image = imagescale($image, 100);
        ob_start();
        imagejpeg($image);
        $contents = ob_get_contents();
        ob_end_clean();
        return base64_encode($contents);
    }

    public static function Delete($image_id, $user_id, $group_id)
    {
        $fileInformation = ImageHandler::Get($image_id, $group_id, false);
        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $key = $fileInformation[0]['s3_bucket_key'];
        if (strlen($key) > 0) {
            $bucketUtility = new BucketUtility();
            if ($bucketUtility->FileExists($key) === true) {
                $bucketUtility->DeleteFile($key);
            }

            if ($bucketUtility->FileExists($key) === false) {
                $qry = "DELETE FROM `image` WHERE `image_id` = $image_id";

                $config = new Config('1.0');
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);

                Audit::Write($fileInformation[0]['context'], $fileInformation[0]['context_id'], $user_id, 'Image deleted');
                return new Result(['message' => Message::ObjectDeleted]);
            }
        } else {
            $qry = "DELETE FROM `image` WHERE `image_id` = $image_id";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            Audit::Write($fileInformation[0]['context'], $fileInformation[0]['context_id'], $user_id, 'Image deleted');
            return new Result(['message' => Message::ObjectDeleted]);
        }

        if ($fileInformation['original_image_id'] > 0) {
            ImageHandler::Delete($fileInformation['original_image_id'], $user_id, $group_id);
        }
        if ($fileInformation['thumbnail_image_id'] > 0) {
            ImageHandler::Delete($fileInformation['thumbnail_image_id'], $user_id, $group_id);
        }

        if (sizeof($fileInformation) === 0) {
            return new ErrorResult('image Not deleted', 500);
        }
    }

    public static function Update($image_id, $user_id, $group_id, $text)
    {
        $fileInformation = ImageHandler::Get($image_id, $group_id, false);
        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $text = mysqli_real_escape_string($db->conn, $text);

        $qry = "UPDATE image SET `text` = '$text' WHERE image_id = $image_id";

        $data = $db->ExecStatement($qry);
        Audit::Write($fileInformation[0]['context'], $fileInformation[0]['context_id'], $user_id, 'Image updated');

        $fileInformation = ImageHandler::Get($image_id, $group_id, false);

        return new Result($fileInformation);
    }

    public static function UpdateTags($image_id, $user_id, $group_id, $tags)
    {
        $fileInformation = ImageHandler::Get($image_id, $group_id, false);
        if (sizeof($fileInformation) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $tags = ImageHandler::_ProcessTagsToSave($tags);

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $text = mysqli_real_escape_string($db->conn, $tags);

        $qry = "UPDATE image SET `tags` = '$tags' WHERE image_id = $image_id";

        $data = $db->ExecStatement($qry);
        Audit::Write($fileInformation[0]['context'], $fileInformation[0]['context_id'], $user_id, 'Image updated');

        $fileInformation = ImageHandler::Get($image_id, $group_id, false);

        return new Result($fileInformation);
    }

    public static function CreateTextAsset(ImageHandler $model)
    {
        $order = ImageHandler::GetNextOrder($model->context_id);

        $qry = "INSERT INTO `image`(`context`, `context_id`, `group_id`, `asset_type`,`text`,`uploaded`,`order`)
        VALUE('$model->context',$model->context_id, $model->group_id, $model->asset_type, '$model->text',1, $order)";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $image_id = $db->ExecStatement($qry);
        Audit::Write($model->context, $model->context_id, $model->user_id, 'Image asset with text only created.');

        return ImageHandler::Get($image_id, $model->group_id);
    }

    public static function DeleteFor($context, $context_id, $user_id, $group_id)
    {
        $filter = [
            'context' => $context,
            'context_id' => $context_id
        ];

        $images = ImageHandler::Get(0, $group_id, false, $filter);


        foreach ($images as $image) {
            ImageHandler::Delete($image['image_id'], $user_id, $group_id);
        }
    }


    public static function GetNextOrder($inspection_id)
    {
        $qry = "SELECT * FROM image WHERE context_id = $inspection_id AND context = 'inspection' ORDER BY `order` DESC";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $order = 1;
        if (sizeof($data) > 0) {
            $lastOrder = (int) $data[0]['order'];
            if ($lastOrder > 0) {
                $order = $lastOrder++;
            }
        }

        return $order;
    }
}
