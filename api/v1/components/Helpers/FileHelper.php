<?php
class FileHelper
{
    public function GetFile(int $context_id, string $context, string $filename): string
    {
        $file = '';

        $path = getcwd() . "\\content\\images\\$context\\$context_id\\$filename";
        if (isset($filename) && file_exists($path) === true) {
            if (strlen($filename) > 0) {
                $file = $this->getMime($path) . ';base64,' . base64_encode(file_get_contents($path));
            }
        }

        return $file;
    }

    private function getMime($path)
    {
        $mime = "";

        $pos = strpos($path, '.');
        $ext = substr($path, $pos);
        $ext = strtolower($ext);

        switch ($ext) {
            case '.jpg':
                $mime = "image/jpg";
                break;
            case '.jpeg':
                $mime = "image/jpeg";
                break;
            case '.png':
                $mime = "image/png";
                break;
        }

        return $mime;
    }

    private function getFileNameFromPath($path)
    {
        $pos = strrpos($path, '\\');
        $fn = substr($path, $pos+1);
        return strtolower($fn);
    }

    private function SaveOriginalToDisk($filePath, $contents)
    {
        $mime = $this->getMime($filePath);

        if ($mime == 'image/png') {
            file_put_contents($filePath, base64_decode($contents));

            $image = imagecreatefrompng($filePath);
            $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
            imagealphablending($bg, TRUE);
            imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
            imagedestroy($image);
            $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
            imagejpeg($bg, $filePath . ".jpg", $quality);
            imagedestroy($bg);

            unlink($filePath);

            $filePath = $filePath . ".jpg";

            return (object)['filename' => $this->getFileNameFromPath($filePath), 'filepath' => $filePath];
        }

        file_put_contents($filePath, base64_decode($contents));
        return (object)['filename' => $this->getFileNameFromPath($filePath), 'filepath' => $filePath];
    }

    private function correctImageOrientation($filename)
    {
        if (function_exists('exif_read_data')) {
            $exif = exif_read_data($filename);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if ($orientation != 1) {
                    $img = imagecreatefromjpeg($filename);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                    if ($deg) {
                        $img = imagerotate($img, $deg, 0);
                    }
                    // then rewrite the rotated image back to the disk as $filename 
                    imagejpeg($img, $filename, 50);
                } // if there is some rotation necessary
            } // if have the exif orientation info
        } // if function exists      
    }

    function resize_image($file, $w, $h, $crop = FALSE)
    {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        ob_start();
        imagejpeg($dst, null, 100);
        $data = ob_get_clean();
        file_put_contents($file, $data);

        return $dst;
    }

    public function SaveFile($context, $context_id, $filename, $contents)
    {
        $dir = getcwd() . "\\content\\images\\$context\\$context_id\\";

        $fullPath = $dir . $filename;

        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $obj = $this->SaveOriginalToDisk($fullPath, $contents);
        $this->correctImageOrientation($obj->filepath);
        // $resizedImage = $this->resize_image($obj->filepath, 100, 100, true);

        return $obj->filename;
        // return $this->CompressFile($fullPath, 75);
    }

    public function DeleteFile($context, $context_id, $filename)
    {
        $path = getcwd() . "\\content\\images\\$context\\$context_id\\$filename";

        if ($this->FileExists($context, $context_id, $filename)) {
            if (strlen($filename) > 0) {
                unlink($path);
            }
        }
    }

    public function UpdateFile($context, $context_id, $fileName, $contents)
    {
        $this->DeleteFile($context, $context_id, $fileName, $contents);
        return $this->SaveFile($context, $context_id, $fileName, $contents);
    }

    private function FileExists($context, $context_id, $filename)
    {
        $path = getcwd() . "\\content\\images\\$context\\$context_id\\$filename";

        return isset($filename) && file_exists($path) === true;
    }

    public function CompressFile($source, $quality)
    {

        $info = getimagesize($source);
        $image = null;
        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }

        $path = substr($source, 0, strrpos($source, '.'));

        $filename = substr($path, strrpos($path, '\\') + 1);

        $extension = substr($source, strrpos($source, '.'));

        $destination = substr($source, 0, strrpos($source, '.')) . '.jpg';

        imagejpeg($image, $destination, $quality);

        $ignoreList = ['.jpg', '.jpeg'];

        if ($source != $destination) {
            unlink($source);
        }

        return $filename . $extension;
    }

    public function GenerateToken()
    {
        $date = new DateTime();
        return (string)$date->getTimestamp() . $this->generateRandomString(5);
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function Insert($context, $context_id, $fileName, $contents)
    {
        $config = new Config('1.0');

        $fileName = $this->GenerateToken() . substr($fileName, strripos($fileName, '.'));

        $fileName = $this->SaveFile($context, $context_id, $fileName, $contents);

        $qry = "insert into asset (`filename`, `context`,`context_id`) values ('$fileName','$context','$context_id')";
        $db = new DataService($config->dbConnection);
        $asset_id = $db->ExecStatement($qry);

        return $asset_id;
    }

    public function Update($context, $context_id, $fileName, $contents)
    {
        $this->UpdateFile($context, $context_id, $fileName, $contents);
    }
}
