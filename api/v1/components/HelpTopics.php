<?php

class HelpTopics
{
    private $Context = null;
    public function __construct($context)
    {
        $this->Context = $context;
        useClass('Notification,Helpers/FileHelper', $context);
    }

    private function getFilter(array $filterFields): object
    {
        $data = [];
        $ctx = $this->Context;

        foreach ($filterFields as $key) {

            $value = '';
            if (isset($ctx->Filter->Options->$key)) {
                $value = $ctx->Filter->Options->$key;
            }

            $data[$key] = $value;
        }

        return (object)$data;
    }

    public function Get()
    {
        $ctx = $this->Context;
        $id = 0;

        if (isset($ctx->Route->id[0])) {
            $id = $ctx->Route->id[0];
        }

        $filterFields = [
            'groupid', 'target', 'status', 'view', 'count', 'orderby', 'category', 'offset'
        ];
        $filter = $this->getFilter($filterFields);

        $qry = 'select t.*, c.`category_name`, s.`status`, t.`status` as `status_id`, REPLACE(t.title," ","") as slug, asset.filename
                from help_topic t
                left join help_category c on c.category_id = t.category_id
                left join help_topic_status s on t.status = s.value
                left join help_category_target hct on hct.help_category_id = c.category_id
                left join help_target ht on ht.value = hct.help_target_id
                left join asset on asset.asset_id = t.cover_image_id
         ';

        if (isset($ctx->Profile)) {
            $groupid = $ctx->Profile->group_id;
            $qry .= ' where t.`group_id` = %d ';
            $qry = sprintf($qry, $groupid);
        } else {
            $qry .= ' where t.`group_id` >= 0 ';
        }

        if ($id > 0) {
            $qry .= ' and help_topic_id = ' . $id;
        }

        if ($filter->status > 0) {
            $qry .= " and t.`status` = $filter->status ";
        }

        if (strlen($filter->category) > 0) {
            $qry .= " and t.category_id = $filter->category ";
        }

        if (isset($filter->target)) {
            if (strlen($filter->target) > 0) {
                $qry .= " and ht.targetname = '$filter->target'";
                $qry .= ' and t.status = 2 ';
            }
        }

        $qry .= ' group by t.help_topic_id ';

        if (strlen($filter->orderby) > 0) {
            $orderby = urldecode($filter->orderby);
            $qry .= " order by $orderby ";
        } else {
            $qry .= ' order by created_date desc ';
        }


        if ($filter->count > 0) {
            if ($filter->offset > 0) {
                $qry .= " limit $filter->offset,$filter->count ";
            } else {
                $qry .= " limit $filter->count ";
            }
        } else {
            if ($filter->offset > 0) {
                $qry .= " limit $filter->offset, 10";
            }
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($filter->status > 0) {
            $this->RecordImpression($data);
        }

        if ($filter->view == 1) {
            $this->RecordView($data);
        }

        if ($id > 0) {
            if (sizeof($data) == 1) {
                $data[0]['cover_image'] = $this->GetCoverImage($data[0]);
            }
        }

        return new Result($data);
    }

    private function GetCoverImage($topic)
    {
        if (strlen($topic['filename']) > 0) {
            $topic_id = $topic['help_topic_id'];
            $filename = $topic['filename'];

            $fileHelper = new FileHelper();
            return $fileHelper->GetFile($topic_id, 'help_topic', $filename);
        }
        return '';
    }

    private function RecordImpression($data)
    {
        $config = new Config('1.0');

        foreach ($data as $item) {
            $id = $item['help_topic_id'];

            $qry = "update help_topic set impressions = impressions+1 where help_topic_id = $id";

            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);
        }
    }

    private function RecordView($data)
    {
        $config = new Config('1.0');

        foreach ($data as $item) {
            $id = $item['help_topic_id'];

            $qry = "update help_topic set views = views+1 where help_topic_id = $id";

            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);
        }
    }

    // used internally by this class only for update
    private function _GetTopic()
    {
        $ctx = $this->Context;
        $id = 0;
        $groupid = 0;

        if (sizeof($ctx->Route->id) == 1) {
            $id = intval($ctx->Route->id[0]);
        }

        $groupid = $ctx->Profile->group_id;

        $qry = 'select t.*
        from help_topic t
        where t.`group_id` = %d';
        $qry = sprintf($qry, $groupid);

        if ($id > 0) {
            $qry .= ' and help_topic_id = ' . $id;
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return $data;
    }

    public function Save()
    {
        $ctx = $this->Context;

        $id = 0;

        if (sizeof($ctx->Route->id) == 1) {
            $id = intval($ctx->Route->id[0]);
        }

        if ($id > 0) {
            return HelpTopics::Update();
        } else {
            return HelpTopics::Insert();
        }
    }

    public function Insert()
    {

        $ctx = $this->Context;

        $groupid = $ctx->Profile->group_id;
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        if (strlen($ctx->FormData->title) == 0) {
            $ctx->FormData->title = substr($ctx->FormData->body, 0, 50);
        }

        if (strlen($ctx->FormData->abstract) == 0) {
            $ctx->FormData->abstract = substr($ctx->FormData->body, 0, 255);
        }

        if (strlen($ctx->FormData->order) == 0) {
            $ctx->FormData->order = 0;
        }

        $htmlbody = $ctx->FormData->htmlbody;

        $qry = "insert into help_topic (`title`,`abstract`,`tags`,`body`,`htmlbody`, `category_id`, `group_id`,`order`) values ('%s', '%s','%s','%s','%s', %d, %d, %d)";
        $qry = sprintf(
            $qry,
            mysqli_real_escape_string($db->conn, $ctx->FormData->title),
            mysqli_real_escape_string($db->conn, $ctx->FormData->abstract),
            mysqli_real_escape_string($db->conn, $ctx->FormData->tags),
            mysqli_real_escape_string($db->conn, $ctx->FormData->body),
            mysqli_real_escape_string($db->conn, $ctx->FormData->htmlbody),
            $ctx->FormData->category_id,
            $groupid,
            $ctx->FormData->order
        );


        $data = $db->ExecStatement($qry);

        $this->SaveCover($ctx->FormData, $data);

        return new Result($data);
    }

    public function update()
    {
        $ctx = $this->Context;
        $topicid = intval($ctx->Route->id[0]);
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $topic = $this->_GetTopic();

        if (isset($ctx->FormData)) {
            if (strlen($ctx->FormData->title) == 0) {
                $ctx->FormData->title = substr($ctx->FormData->body, 0, 50);
            }

            if (strlen($ctx->FormData->abstract) == 0) {
                $ctx->FormData->abstract = substr($ctx->FormData->body, 0, 255);
            }

            if (strlen($ctx->FormData->order) == 0) {
                $ctx->FormData->order = 0;
            }
        }

        $qry = "update help_topic set `title` = '%s',`abstract` = '%s',`tags` = '%s', `body` = '%s', `category_id` = %d, `order` = %d ";
        $qry = sprintf(
            $qry,
            mysqli_real_escape_string($db->conn, $ctx->FormData->title),
            mysqli_real_escape_string($db->conn, $ctx->FormData->abstract),
            mysqli_real_escape_string($db->conn, $ctx->FormData->tags),
            mysqli_real_escape_string($db->conn, $ctx->FormData->body),
            $ctx->FormData->category_id,
            $ctx->FormData->order
        );


        $htmlbody = $ctx->FormData->htmlbody;
        $qry .= ", htmlbody = '$htmlbody'";


        $qry .= " where `help_topic_id` = $topicid";

         $this->SaveCover($ctx->FormData, $topicid);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    public function Delete()
    {
        $ctx = $this->Context;

        $topicid = intval($ctx->Route->id[0]);
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $qry = 'delete from help_topic where help_topic_id = ' . $topicid;

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    public function Status()
    {
        $ctx = $this->Context;

        $topicid = intval($ctx->Route->id[0]);

        if ($ctx->FormData->status == 'offline') {
            $status = 3;
        }

        if ($ctx->FormData->status == 'online') {
            $status = 2;

            $this->SendFirstTimePublishNotification($topicid);
        }

        $qry = "update `help_topic` set `status` = %d ";

        if ($status === 2) {
            if (isset($ctx->FormData->htmlbody)) {
                $htmlbody = $ctx->FormData->htmlbody;
                $qry .= " ,published_date = now(), htmlbody =  '$htmlbody'";
            }
        }
        $qry .= " where help_topic_id = %d ";

        $qry = sprintf($qry, $status, $topicid);
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function SaveCover($formData, $topic_id)
    {
        if (strlen($formData->file_name) == 0) {
            return;
        };

        $config = new Config('1.0');
        $qry = "select * from help_topic where help_topic_id = $topic_id";
        $db = new DataService($config->dbConnection);
        $topic = $db->ExecStatement($qry);

        $fileHelper = new FileHelper();

        $fileContents = $formData->file_contents;

        if ($topic[0]['cover_image_id'] > 0) {
            $qry = "select asset.* from help_topic
            left join asset on asset.asset_id = help_topic.cover_image_id
            where help_topic_id = $topic_id";
            $db = new DataService($config->dbConnection);
            $asset = $db->ExecStatement($qry);

            $fileName = $asset[0]['filename'];
            $fileHelper->Update($fileName, $fileContents, 'help_topic', $topic_id);
        } else {
            $fileName = $formData->file_name;
            $asset_id = $fileHelper->Insert($fileName, $fileContents, 'help_topic', $topic_id);

            $qry = "update help_topic set cover_image_id = $asset_id where help_topic_id = $topic_id";
            $db = new DataService($config->dbConnection);
            $db->ExecStatement($qry);
        }
    }

    private function SendFirstTimePublishNotification($topic_id)
    {
        $config = new Config('1.0');
        $qry = "select * from help_topic where help_topic_id = $topic_id";
        $db = new DataService($config->dbConnection);
        $topic = $db->ExecStatement($qry);

        if ($topic[0]['published_date'] === "") {
            $notification = new Notification();
            $notification->Send('Lucid Ocean', 'Topic ' . $topic[0]['title'] . ' has been published.', $this->Context->Profile->user_id, 'notification', 0);
        }
    }
}
