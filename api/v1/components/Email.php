<?php

class Email
{
    public function SendEmail($to, $event, $data)
    {
        $messagetemplate = $this->_GetMessageTemplate($event);
        if($messagetemplate == null) return false;
        $emailTemplate = $this->_GetEmailTemplate();
        $body = $this->_PlaceholderReplace($messagetemplate->body, $data);
        $subject = $this->_PlaceholderReplace($messagetemplate->subject, $data);
        $data->body = $body;
        $data->subject = $subject;
        $body = $this->_PlaceholderReplace($emailTemplate, $data);
        require_once './components/Helpers/EmailSender.php';
        $emailSender = new EmailSender();
        $success = $emailSender->SendEmail($to, $subject, $body);
        return $success;
    }

    private function _GetMessageTemplate($event)
    {
        $qry = "SELECT * FROM `message_template` WHERE event_Type = $event;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
         if(sizeof($data)  == 1){
            return (object)$data[0];
        }
        return null;
    }
    private function _GetEmailTemplate(){
        return file_get_contents('emailtemplate.html');
    }
    private function _PlaceholderReplace($message,$data){
        foreach($data as $key => $value){
            $field = "{".$key."}";
            $message = str_replace($field,$value,$message);
        }
        return $message;
    }
}
