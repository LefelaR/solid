<?php

class TemplateText
{

    public $template_type = 0;
    public $group_id = 0;
    public $template_id = 0;

    public function __construct()
    { }

    public static function Get(int $template_id, int $group_id,$returnResult = true)
    {
        $qry = "SELECT * FROM `template_text` WHERE `group_id` = $group_id";

        if ($template_id > 0) {
            $qry .= " and template_text.template_id = $template_id";
        }

        $qry .= " order by created_date desc";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($template_id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;

        // return new Result($data);
    }


    
    public static function Insert($model)
    { 
        $qry = "
            INSERT INTO `template_text`(`group_id`,`text`, `title`)
            VALUES ($model->group_id, '$model->template', '$model->title');
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $template_id = $db->ExecStatement($qry);
        return TemplateText::Get($template_id, $model->group_id);
    }

    public static function Update(TemplateText $model)
    {
        $qry = "UPDATE `template_text` SET `text` = '$model->template', `title`  = '$model->title'
        WHERE `template_id` = $model->template_id AND `group_id` = $model->group_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        return TemplateText::Get($model->template_id, $model->group_id);
    }

    public static function Delete(TemplateText $model)
    {

        $template = TemplateText::Get($model->template_id, $model->group_id, false);

        if (sizeof($template) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "DELETE FROM `template_text` WHERE `template_id` = $model->template_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        return new Result(['message' => Message::ObjectDeleted]);
    }
}