<?php

class Questions
{
    public static function Get(int $id, int $group_id, array $filter = null): Result
    {
        $qry = 'select * from question';

        $qry .= " where group_id = $group_id";

        if ($id > 0) {
            $qry .= " and question_id = $id";
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($id > 0 && (sizeof($data) == 0)) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $data = Questions::_GetPossibleAnswers($data);

        return new Result($data);
    }

    private static function _GetPossibleAnswers(array $data): array
    {
        $dataX = [];
        foreach ($data as $item) {
            $question_id = $item['question_id'];
            $group_id = $item['group_id'];

            $item['answers'] = Questions::_GetAnswersForQuestion($question_id,$group_id);            
            array_push($dataX, $item);
        }        
        return $dataX;
    }

    private static function _GetAnswersForQuestion($question_id, $group_id)
    {
        $qry = "select qti.* from question q
            left join question_type_item qti on qti.question_type_id = q.question_type_id
            where q.question_id = $question_id and q.group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    public static function Save($id, $group_id, $user_id): Result
    {
        return new Result('');
    }
}
