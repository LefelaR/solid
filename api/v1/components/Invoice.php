<?php


class  Invoice
{

    public $Booking_Id;
    public $Invoice_Id;
    public $group_id;
    public $user_id;
    public $Invoice_To;
    public $Invoice_Ref;
    public $Due_Date;
    public $email;

    public static function Get(int $Booking_Id, int $Invoice_Id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                    invoice.*,
                    company.`vatamount`,
                    company.`invoice_prefix`,
                    booking.*, 
                    contact.*,
                    address.*,
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress
                FROM invoice
                left join booking on booking.Booking_Id = invoice.Booking_Id
                left join address on address.address_id = booking.address_id
            
                left join company on company.group_id = booking.group_id
                left join contact on contact.contact_id = booking.contact_id';


        $qry .= " where booking.group_id = $group_id AND invoice.deleted = 0 ";


        $qry .= " and invoice.Booking_Id = $Booking_Id";

        if ($Invoice_Id > 0) {
            $qry .= " and invoice.Invoice_Id = $Invoice_Id";
        }
        $qry .= ' order by invoice.Created_Date desc';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($Invoice_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }

    public static function GetReport($token)
    {
        if ($token === null)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $qry = "SELECT * from invoice_share where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) == 0)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $invoice_id = $data[0]['invoice_id'];

        $qry = 'SELECT 
            invoice.*,
            booking.*, 
            contact.*,
            address.*,
            CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress,
            `user_group`.`group_name`,
            company.*
        FROM invoice
        left join booking on booking.Booking_Id = invoice.Booking_Id
        left join address on address.address_id = booking.address_id    
        left join company on company.group_id = booking.group_id
        left join user_group on `user_group`.`group_id` = `company`.`group_id`
        left join contact on contact.contact_id = booking.contact_id';

        $qry .= ' WHERE invoice.Invoice_id = ' . $invoice_id;


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }


    public static function Save(Invoice $model)
    {
        if ($model->Invoice_Id > 0) {
            return Invoice::_Update($model);
        }
        return Invoice::_Insert($model);
    }
    private static function _Insert(Invoice $model)
    {

        $qry = "
        INSERT INTO `invoice`(`Booking_Id`)
        VALUES ('$model->Booking_Id');
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $Invoice_Id = $db->ExecStatement($qry);


        $qry2 = " UPDATE `booking` SET `invoice_id`=  $Invoice_Id WHERE `booking_id`= $model->Booking_Id;";
        $db->ExecStatement($qry2);

        Audit::Write('invoice', $Invoice_Id, $model->user_id, 'Invoice added');

        return Invoice::Get($model->Booking_Id, $Invoice_Id, $model->group_id);
    }


    private static function _Update(Invoice $model)
    {
        $qry = "UPDATE `Invoice` SET `Invoice_To` = '$model->Invoice_To', `Invoice_Ref`  = '$model->Invoice_Ref', `Due_Date` = '$model->Due_Date'
        WHERE `Invoice_Id` = $model->Invoice_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        Audit::Write('Invoice', $model->Invoice_Id, $model->user_id, 'Invoice updated');

        return Invoice::Get($model->Booking_Id, $model->Invoice_Id, $model->group_id);
    }


    public static function Delete(Invoice $model)
    {
        $Invoice = Invoice::Get($model->Booking_Id, $model->Invoice_Id, $model->group_id, false);
        if (sizeof($Invoice) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "UPDATE `invoice` SET `invoice`.`Deleted` = 1  WHERE `Invoice_Id` = $model->Invoice_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);


        $qry2 = " UPDATE `booking` SET `invoice_id`=NULL WHERE `booking_id`= $model->Booking_Id;";
        $db->ExecStatement($qry2);


        Audit::Write('Invoice', $model->Invoice_Id, $model->user_id, 'Invoice deleted');

        return new Result(['message' => Message::ObjectDeleted]);
    }

    public static function GetInvoiceShare(int $Invoice_Id, int $group_id, $user_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                    invoice_share.*,
                    invoice.*,
                    company.*,
                    booking.*, 
                    contact.*,
                    address.*,
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress
                FROM invoice_share
                join invoice on invoice.invoice_id = invoice_share.invoice_id
                left join booking on booking.Booking_Id = invoice.Booking_Id
                left join address on address.address_id = booking.address_id
            
                left join company on company.group_id = booking.group_id
                left join contact on contact.contact_id = booking.contact_id';

        $qry .= " where invoice.deleted = 0 and invoice_share.user_id = $user_id";


        // $qry .= " and invoice.Booking_Id = $Booking_Id";

        if ($Invoice_Id > 0) {
            $qry .= " and invoice_share.invoice_id = $Invoice_Id";
        }
        $qry .= ' order by invoice_share.sharedate desc';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $logo_id = $data[0]['logo_id'];
            $group_invoice_id = $data[0]['group_id'];
            $avatar = Company::_GetCompanyLogo($logo_id, $group_invoice_id);
            $data[0]['logo'] = $avatar['logo'];
        }

        if ($returnResult === true) {
            if ($Invoice_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }
}
