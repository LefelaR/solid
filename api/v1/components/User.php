<?php

abstract class UserRole
{
    const SuperAdministrator = -999;
    const Administrator = 1;
    const User = 2;
    const Inspector = 3;
    const user_limited = 4;
}
class User
{
    private $fieldlist = ' `user`.`user_id`, `email`, `password`, `remember_me`, `access_token`, `group_id`, `signed_in_date`, `locked`';

    static function AuthenticateToken($token)
    {
        $config = new Config('1.0');
        $qry = "
        select  `user`.`user_id`,`email`, `user_group_access`.`token` as `access_token`, `user_profile`.`first_name`,`user_profile`.`last_name`, `user_profile`.`group_name`, `user_group`.`personal`, `user_group_access`.`group_id`, `user_group_access`.`locked`,  GROUP_CONCAT(role.role) as user_role from `user` 
        join user_profile on `user_profile`.`user_id` = `user`.`user_id` 
        join user_group_access on `user_group_access`.`user_id` =  `user_profile`.`user_id` 
        join role on role.value = user_group_access.group_role
        join user_group on `user_group`.`group_id` =  `user_group_access`.`group_id`     
        where `user_group_access`.`token` = '%s' 
        group by user.user_id
            ";
        $qry = sprintf($qry, $token);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    function Register($ctx)
    {
        $config = new Config('1.0');
        $qry = "insert into `user` (`email`, `password`, `terms`, `locked`) values ('%s', '%s', '%b', %d)";
        $qry = sprintf($qry, $ctx->FormData->email, $ctx->FormData->password, $ctx->FormData->terms, 0);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    function UserExists($email)
    {
        $config = new Config('1.0');
        $qry = "select * from user where email like '%s'";
        $qry = sprintf($qry, $email);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return sizeof($data) === 1;
    }

    function SignIn($email, $password,  $remember_me )
    {

        $pass = md5($password);

        $config = new Config('1.0');
        // if ($config->state == "live") {
        //     $data = [
        //         'Message' => 'Nova will be launching soon. You cannot sign in yet.',
        //         'Code' => 5
        //     ];
        //     $ret = new Result($data);
        //     $ret->FlushResult();
        // }
        $qry = "select 
                    `user`.`user_id`,
                    `user`.`email`,
                    `user`.`password`,
                    `user`.`validated`,
                    `user_group_access`.`group_id`,
                    `user`.`created_date`,
                    `user_group_access`.`token` as `access_token`,
                    `user`.`signed_in_date`,
                    `user_group_access`.`locked`,
                    `user`.`confirmation_token`,
                    `user`.`remember_me`,
                    `user_group`.group_name
                     from user 
                     left join user_group_access on user_group_access.user_id = user.user_id
                     left join user_group on user_group.group_id = user_group_access.group_id
                where email = '%s' and password = '%s'";
        $qry = sprintf($qry,  htmlspecialchars($email), htmlspecialchars($pass));
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 0) {
            return new ErrorResult(Message::InvalidCredentials, 401);
        }

        if ($data[0]['email'] == $email && $data[0]['password'] == $pass) {

            if ($data[0]['locked'] != "0") {
                return new ErrorResult(Message::AccountLocked);
            }

            if ($data[0]['confirmation_token']) {
                return new ErrorResult('Your account is not verified. Please check your spam and inbox for a verification email.');
            }

            $profiles = [];

            foreach ($data as $dataItem) {
                $accessToken = $dataItem['access_token'];
                if (isset($dataItem['access_token'])) {
                    if (strlen($dataItem['access_token']) <= 5) {
                        $accessToken = md5(microtime() . rand());
                    }
                }

                $qry = "UPDATE `user_group_access` SET `token` = '%s' WHERE `user_id` = %d AND `group_id` = %d";
                $qry = sprintf($qry, $accessToken, $dataItem['user_id'], $dataItem['group_id']);
                $db = new DataService($config->dbConnection);
                $reply = $db->ExecStatement($qry);

                $date = date('Y-m-d G:i:s');
                $dataItem['signed_in_date'] = $date;
                $dataItem['access_token'] = $accessToken;
                $dataItem['locked']  = boolval($dataItem['locked']);
                $dataItem['remember_me']  = boolval($remember_me);

                unset($dataItem['validated']);
                unset($dataItem['confirmation_token']);

                array_push($profiles, $dataItem);
            }

            $accessToken =  $profiles[0]['access_token'];
            $qry = "UPDATE `user` SET `signed_in_date`='%s', `access_token` = '%s', `remember_me` = %b WHERE `user_id` = %d";
            $date = date('Y-m-d G:i:s');
            $qry = sprintf($qry, $date, $accessToken, $remember_me, $data[0]['user_id']);
            $db = new DataService($config->dbConnection);
            $reply = $db->ExecStatement($qry);

            if ($remember_me == true) {
                setcookie("crest_auth", $accessToken, time() + 60 * 60 * 24 * 2, '/');
            }

            if ($remember_me == false) {

                setcookie("crest_auth", $accessToken, time() + 60 * 30, '/');
            }

            $obj = new stdClass();
            $obj->user_name = $data[0]['email'];
            $obj->date = date("d-m-Y H:i:s");
            $obj->cta_text = "Change Password";
            $obj->cta_link = $config->siteurl . "account/changePassword";
            $em = new Email();

         
            return new Result($profiles);
        }




        return false;
    }

    static function LoginLimitedUser($user_id, $token)
    {
        $accessToken = $token;
        if(strlen($token) == 0){
            $accessToken = $s;
        }

        setcookie("crest_auth", $accessToken, time() + 60 * 60, '/');
    }

    function SubscribeToPush($ctx)
    {
        
        useClass('Email',$ctx );

        $config = new Config('1.0');
        $qry = "select * from `user` where `email` like '".$ctx->FormData->email."'";
        $qry = sprintf($qry, $ctx->FormData->email);
        $db = new DataService($config->dbConnection);
        $result = $db->ExecStatement($qry);
       
        if(sizeof($result) == 0){   
        // subscribe the user
        $qry = "insert into `subscriptions` (`email`, `send`) values ('%s', %d)";
        $qry = sprintf($qry, $ctx->FormData->email, 1);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $obj = new stdClass();
        $obj->user_name =  $ctx->FormData->email;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Subscription";
        $obj->cta_link = $config->siteurl . "";
        $em = new Email();
        $em->SendEmail($ctx->FormData->email, 16, $obj);

        return ('Success');

     }else{

        $user_id = $result[0]['user_id'];
        $email = $result[0]['email'];
      
        $qry = "select * from `subscriptions` where `email` like '".$email."'";
        $qry = sprintf($qry, $ctx->FormData->email);
        $db = new DataService($config->dbConnection);
        $sub = $db->ExecStatement($qry);
        
         $active = $sub[0]['send'];
         $id = $sub[0]['id'];

      if (sizeof($sub) == 0){
        
        $qry = "insert into `subscriptions` (`email`, `send`) values ('%s', %d)";
        $qry = sprintf($qry, $ctx->FormData->email, 1);
        $db = new DataService($config->dbConnection);
        $data =   $db->ExecStatement($qry);

        $qry ="update `account_setting` set `newsletter` = %d  where `user_id`=".$user_id;
        $qry = sprintf($qry, 1);
        $db = new DataService($config->dbConnection);
       $db->ExecStatement($qry);

       

      }else{

        if($active == 1 ){
        
            return ('Exist');
     
        }else{
            $qry ="update `subscriptions` set `send` = %d  where `id`=".$id;
            $qry = sprintf($qry, 1);
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            return ('Subscribed');
        }
      }

        $obj = new stdClass();
        $obj->user_name =  $ctx->FormData->email;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Newsletter";
        $obj->cta_link = $config->siteurl . "";
        $em = new Email();
        $em->SendEmail($ctx->FormData->email, 17, $obj);

     }

        return $data;
    }


    static  function Unsubscribe($ctx)
    {

        useClass('Email',$ctx );

    $email = $ctx->FormData->email;
    $config = new Config('1');
    $qry="select * from `subscriptions` where `email` like '".$email."'";
    $db = new DataService($config->dbConnection);
    $result = $db->ExecStatement($qry);
    if(sizeof($result) > 0){
        $id = $result[0]['id'];
        $send = $result[0]['send'];

        if($send == 1){

        $qry = "update  `subscriptions` SET `send`= %d where `id`=". $id;
        $qry = sprintf($qry, 0);
        $db = new DataService($config->dbConnection);
        $subscription = $db->ExecStatement($qry);
        
    }else{

        return ("Unsubscribed");
    }

    }   
    $qry="select * from `user` where `email` like '".$email."'";
    $db = new DataService($config->dbConnection);
    $user = $db->ExecStatement($qry);
    $user_id = $user[0]['user_id'];

//unsubscribe the user
    $qry = "update `account_setting` SET `notifications`= %d, `articles` = %d, `newsletters` = %d, `activities` =%d  where `user_id`=".$user_id;
    $qry = sprintf($qry, 0,0,0,0);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);

    $obj = new stdClass();
    $obj->user_name =  $email;
    $obj->date = date("d-m-Y H:i:s");
    $obj->cta_text = "Unsubscribed";
    $obj->cta_link = $config->siteurl . "";
    $em = new Email();
    $em->SendEmail($email, 18, $obj);

    return $data;
    }


   function encryptData($data)
    {
        $iv = "1234567812345678";
        $pass = '#LOstr34m';
        $method = 'aes-128-cbc';
        return openssl_encrypt($data, $method, $pass, true, $iv);
    }

    function decryptData($data)
    {
        $iv = "1234567812345678";
        $pass = '#LOstr34m';
        $method = 'aes-128-cbc';
        return openssl_decrypt($data, $method, $pass, true, $iv);
    }

    function AssignToGroup($user_id, $group_id)
    {
        $config = new Config('1.0');
        $qry = 'Insert into `user_group` ( ) VALUES ()';
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
    }



    function Validate($model)
    {
        $config = new Config('1.0');
        $qry = "select * from `user` where `confirmation_token` like  '%s'";
        $qry = sprintf($qry, $model);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
    }


    function Update($ctx)
    {
        $config = new Config('1.0');
        $qry = "update  user set `email` = '%s'  where  `access_token` like '%s'";
        $qry = sprintf($qry, $ctx->email, $ctx->access_token);
        $db = new DataService($config->dbConnection);
        $result = $db->ExecStatement($qry);
        // get the user id
        $qry = "select * from user where  `access_token` like '%s'";
        $qry = sprintf($qry, $ctx->access_token);
        $db = new DataService($config->dbConnection);
        $info = $db->ExecStatement($qry);
        $user_id = $info[0]['user_id'];
        // update user profile
        $qry = "update  user_profile set `first_name` = '%s' , `last_name` = '%s'  where  `user_id` =" . $user_id;
        $qry = sprintf($qry, $ctx->first_name, $ctx->last_name);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        //  send email

        $obj = new stdClass();
        $obj->user_name = $info[0]['email'];
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Profile Update";
        $obj->cta_link = $config->siteurl . "account/signin";
        $em = new Email();

        return new Result($data);
    }

    function updateUser($ctx)
    {

        useClass('Helpers/FileHelper', $ctx);

        $formData = $ctx->FormData;

        $user_id = $ctx->Profile->user_id;
        $firstName = $formData->name;
        $lastName = $formData->last_name;
        $email = $formData->email;

        $config = new Config('1.0');
        $qry = "update  user set `email` = '%s'  where  `user_id` = %d";
        $qry = sprintf($qry, $email, $user_id);

        $db = new DataService($config->dbConnection);
        $result = $db->ExecStatement($qry);

        $qry = "update  user_profile set `first_name` = '%s' , `last_name` = '%s'  where  `user_id` =" . $user_id;
        $qry = sprintf($qry, $firstName, $lastName);

        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        // send email


        $obj = new stdClass();
        $obj->user_name = $$email;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Profile Update";
        $obj->cta_link = $config->siteurl . "account/signin";
        $em = new Email();

        // $this->UpdateProfileImage($formData,$user_id);

        return new Result($data);
    }

    public static function GetUserForId($user_id)
    {
        if ($user_id == 0)
            return null;

        $qry = "SELECT user.user_id,user.password,CASE WHEN length(password) > 0 THEN 1 ELSE 0 END as haspassword
        , uga.token FROM `user` 
        left join user_group_access uga on uga.user_id = `user`.`user_id`
        where user.user_id = $user_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) > 0) {
            return $data;
        }
        return null;
    }
}