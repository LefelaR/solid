<?php



class ProjectsActivity
{

    static function get($ctx)
    {

        $project_id = $ctx->Route->id[0];

        $iteration_id = 0;
        if (isset($ctx->Filter->Options->iteration_id)) {
            $iteration_id = $ctx->Filter->Options->iteration_id;
        }

        $activity_id = 0;
        if (isset($ctx->Route->id[1])) {
            $activity_id = $ctx->Route->id[1];
        }

        $config = new Config('1.0');

        $qry = "select `activity`.*, user_profile.first_name from `project`
                join `activity` on `activity`.`project_id` = `project`.`project_id`   
                left join `user_profile` on `user_profile`.`user_id` = `activity`.`user_id`
                where `activity`.`project_id`= " . $project_id . "
                "; //$context->Route->id[0]

        if ($iteration_id > 0) {
            $qry .= " and iteration_id = $iteration_id ";
        }

        if ($activity_id > 0) {
            $qry .= " and activity_id = $activity_id ";
        }

        $qry .= ' order by `activity`.`created_date` DESC';

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }




    static function save($ctx)
    {

        $activity_id = 0;
        if (isset($ctx->Route->id[1])) {
            $activity_id = $ctx->Route->id[1];
        }

        if ($activity_id > 0) {
            return ProjectsActivity::update($ctx);
        } else {
            return ProjectsActivity::insert($ctx);
        }
    }

    static function update($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $qry = "UPDATE activity set `title` = '%s', `description` = '%s' WHERE activity_id = %d;";



        $activity_id = $ctx->Route->id[1];        
        $title = mysqli_real_escape_string($db->conn, $ctx->FormData->title);
        $description = mysqli_real_escape_string($db->conn, $ctx->FormData->description);        
        $qry = sprintf($qry, $title, $description, $activity_id);
        
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }


    static function insert($ctx)
    {
        $config = new Config('1.0');

        $db = new DataService($config->dbConnection);

        $qry = "INSERT INTO activity( `project_id`, `title`, `description`,`process`, `user_id`) VALUES ( %d, '%s', '%s', %d, %d);";

        $project_id = mysqli_real_escape_string($db->conn, $ctx->FormData->project_id);
        $title = mysqli_real_escape_string($db->conn, $ctx->FormData->title);
        $description = mysqli_real_escape_string($db->conn, $ctx->FormData->description);
        $process = mysqli_real_escape_string($db->conn, $ctx->FormData->process);
        $user_id = $ctx->FormData->user_id;

        $iteration_id = 0;
        if (isset($ctx->FormData->iteration_id)) {
            $iteration_id = $ctx->FormData->iteration_id;
            $qry = "INSERT INTO activity( `project_id`,`iteration_id` ,`title`, `description`,`process`,`user_id`) VALUES ( %d, %d, '%s', '%s', %d, %d);";
            $qry = sprintf($qry, $project_id, $iteration_id, $title, $description, $process, $user_id);
        } else {
            $qry = sprintf($qry, $project_id, $title, $description, $process, $user_id);
        }

        $data = $db->ExecStatement($qry);

        return new Result($data);
    }


    static function delete($id = 0)
    {
        $config = new Config('1.0');
        $qry = "DELETE FROM `activity` WHERE activity_id = " . $id;
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    // static function activity_iterations($ctx)
    // {

    //     $project_id = $ctx->Route->id[0];

    //     $iteration_id = 0;
    //     if (isset($ctx->Filter->Options->iteration_id)) {
    //         $iteration_id = $ctx->Filter->Options->iteration_id;
    //     }

    //     $activity_id = 0;
    //     if (isset($ctx->Route->id[1])) {
    //         $activity_id = $ctx->Route->id[1];
    //     }

    //     $config = new Config('1.0');

    //     $qry = "SELECT * FROM `activity` where `iteration_id` = %d";


    //     $db = new DataService($config->dbConnection);

    //     $data = $db->ExecStatement($qry);
    //     return new Result($data);
    // }
}
