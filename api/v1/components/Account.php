<?php
class Account
{
    public function __construct()
    { }

    public function SignUp($context)
    {
        // Would fail if exists and quit gracefully
        $user_id = $this->_AccountExists($context->FormData->signupEmail);
        $group_id =  $this->_CompanyExists($context->FormData->company);

        $new_user = $user_id == 0 ? true : false;
        /*
            Exists? error
            Create Personal Group and assign user as admin
            Create Company Group and assign user as admin
        */

        for ($s = '', $i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz0123456789') - 1; $i != 32; $x = rand(0, $z), $s .= $a{
            $x}, $i++);

        $access_token = $s;

        $config = new Config('1.0');

        if ($user_id == 0) {
            // $user_id = $this->_CreateUser($context, $access_token);
            $user_id = $this->_CreateUser($context->FormData->signupEmail, $access_token, $context->FormData->signupPassword);
            $profile_id = $this->_CreateProfile($context->FormData->first_name, $context->FormData->last_name, $context->FormData->company, $user_id);
            $settings_id = $this->_CreateSettings($context->FormData->notifications, $user_id, $profile_id);

            $this->_CreateGroup($context->FormData->signupEmail, $user_id, 'Personal', true);

            if ($group_id > 0) {
                $this->_AddToExistingCompany($context, $user_id, $group_id);
                // if ($config->state != "live") {
                    $this->_SendEmail($context, $access_token, 7);
                // }
            } else {
                $this->_CreateGroup($context->FormData->signupEmail, $user_id, $context->FormData->company, false);
                // if ($config->state != "live") {
                    $this->_SendEmail($context, $access_token, 1);
                // }
            }            
        } else {
            if ($group_id > 0) {
                $this->_AddToExistingCompany($context, $user_id, $group_id);
                // if ($config->state != "live") {
                    $this->_SendEmail($context, $access_token, 7);
                // }
            } else {
                $new_user = true;
                $this->_CreateGroup($context->FormData->signupEmail, $user_id, $context->FormData->company, false);
                $this->ChangePassword($context->FormData->signupPassword, $user_id);
                $this->UpdateProfile($context->FormData->first_name, $context->FormData->last_name, $user_id);
                // if ($config->state != "live") {
                    $user = (object) $this->GetUser($user_id);
                    if ($user->confirmation_token) {
                        $access_token = $user->confirmation_token;
                    }

                    $this->_SendEmail($context, $access_token, 1);
                // }
            }
        }


        // if ($config->state == "live") {
        //     return new Result(['message' => 'Thank you for your interest. We will contact you when Nova has been launched.']);
        // } else {
            if ($new_user == true) {
                return new Result(['message' => 'Welcome to Nova']);
            } else {
                $company = $context->FormData->company;
                return new Result(['message' => "You have been added to $company"]);
            }
        // }
    }

    private function UpdateProfile($first_name, $last_name, $user_id)
    {
        $qry = "UPDATE user_profile SET first_Name = '$first_name', last_name = '$last_name' WHERE user_id = $user_id";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return $data;
    }

    private function GetUser($user_id)
    {
        $qry = "SELECT * FROM user WHERE user_id = $user_id";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return $data[0];
    }

    private function ChangePassword($password, $user_id)
    {
        if (strlen($password) > 0) {
            $pass = md5($password);
        }

        $qry = "UPDATE User SET Password = '$pass' WHERE User_id = $user_id AND Password = ''";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
    }

    public function SignupLimitedAccount($email)
    {
        // Would fail if exists and quit gracefully
        $user_id = $this->_AccountExists($email);
        if ($user_id > 0)
            return $user_id;

        for ($s = '', $i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz0123456789') - 1; $i != 32; $x = rand(0, $z), $s .= $a{
            $x}, $i++);

        $access_token = $s;

        if ($user_id == 0) {
            $user_id = $this->_CreateUser($email, $access_token);
            $profile_id = $this->_CreateProfile('', '', '', $user_id);
            $settings_id = $this->_CreateSettings(1, $user_id, $profile_id);
            $this->_CreateGroup($email, $user_id, 'Personal', true, 4);
        }

        return $user_id;
    }

    private function _AddToExistingCompany($context, $user_id, $group_id)
    {
        $config = new Config('1.0');

        $qry = "SELECT * FROM user_group_access WHERE group_id = $group_id AND user_id = $user_id;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 0) {
            $qry = "INSERT INTO user_group_access (`user_id`, `group_id`,`group_role`,`locked`) VALUES (%d, %d ,%d ,%d );";
            $qry = sprintf($qry, $user_id,  $group_id, 2, 1);
            $db = new DataService($config->dbConnection);
            $group_access_id = $db->ExecStatement($qry);
        } else {
            $result = new ErrorResult("You are already a member of this group. Please contact the admin of this group if you cannot access your account.");
            $result->FlushResult();
        }
    }

    private function _CreateUser($email, $access_token, $password = '')
    {
        $config = new Config('1.0');
        $pass = '';
        $confirmation_token = $access_token;

        if (strlen($password) > 0) {
            $pass = md5($password);
        }

        $qry = "INSERT INTO user (`email`, `password`,`group_id`,`locked`,`confirmation_token`) VALUES ('%s', '%s',%d ,%d,'%s' ); ";
        $qry = sprintf($qry, $email, $pass, 0, 0, $confirmation_token);
        $db = new DataService($config->dbConnection);
        $user_id = $db->ExecStatement($qry);

        return $user_id;
    }

    private function _CreateProfile($first_name, $last_name, $company, $user_id)
    {
        $config = new Config('1.0');
        $qry = "INSERT INTO user_profile (`user_id`, `first_name`, `last_name`, `group_name`) VALUES ( %d,'%s', '%s', '%s'); ";
        $qry = sprintf($qry,  $user_id, $first_name, $last_name, $company);
        $db = new DataService($config->dbConnection);
        $profile_id = $db->ExecStatement($qry);
        return $profile_id;
    }

    private function _CreateSettings($enableNotifications, $user_id, $profile_id)
    {
        $config = new Config('1.0');

        $setting = new stdClass();
        if ($enableNotifications == 1) {
            $setting->articles = 1;
            $setting->newsletters = 1;
            $setting->activities = 1;
        } else {
            $setting->articles = 0;
            $setting->newsletters = 0;
            $setting->activities = 0;
        }

        $qry = "INSERT INTO account_setting (`profile_id`, `user_id`,`notifications`,`articles`,`newsletters`,`activities`) VALUES ( %d,%d,%d,%d,%d,%d);";
        $qry = sprintf($qry, $profile_id, $user_id, $enableNotifications, $setting->articles, $setting->newsletters, $setting->activities);
        $db = new DataService($config->dbConnection);
        $settings_id = $db->ExecStatement($qry);
        return $settings_id;
    }

    private function _CreateGroup($email, $user_id, $group_name, $personal, $group_role = 1)
    {
        $config = new Config('1.0');
        $qry = "INSERT INTO user_group (`group_name`, `created_by`,`personal`) VALUES ('%s', %d, %d); ";
        $qry = sprintf($qry, $group_name, $user_id, $personal == true ? 1 : 0);
        $db = new DataService($config->dbConnection);
        $group_id = $db->ExecStatement($qry);

        $accessToken = md5(microtime() . rand());

        $qry = "INSERT INTO user_group_access (`user_id`, `group_id`,`group_role`,`locked`,`token`) VALUES (%d, %d ,%d ,%d, '%s');";
        $qry = sprintf($qry, $user_id,  $group_id, $group_role, 0, $accessToken);
        $db = new DataService($config->dbConnection);
        $group_access_id = $db->ExecStatement($qry);

        $qry = "INSERT INTO address (`group_id`) VALUES (%d);";
        $qry = sprintf($qry, $group_id);
        $db = new DataService($config->dbConnection);
        $address_id = $db->ExecStatement($qry);

        $qry = "INSERT INTO company (`group_id`,`company_name`,`address_id`,`company_email`) VALUES (%d ,'%s', %d, '%s');";
        $qry = sprintf($qry, $group_id,  $group_name, $address_id,  $email);
        $db = new DataService($config->dbConnection);
        $company_id = $db->ExecStatement($qry);

        return $group_id;
    }

    private function _AccountExists($email)
    {
        $email = trim($email);
        $email = strtolower($email);

        $config = new Config('1.0');
        $qry = "select * from user where email like '%s'";
        $qry = sprintf($qry, $email);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        $ret = 0;
        if (sizeof($data) > 0) {
            $ret = $data[0]['user_id'];
        }
        return $ret;
    }

    private function _CompanyExists($companyName)
    {
        $companyName = trim($companyName);

        $config = new Config('1.0');
        $qry = "select * from user_group where group_name like '%s'";
        $qry = sprintf($qry, $companyName);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        $ret = 0;
        if (sizeof($data) > 0) {
            $ret = $data[0]['group_id'];
        }
        return $ret;
    }

    private function _SendEmail($context, $access_token, $emailType)
    {
        $config = new Config('1.0');

        $url = $config->siteurl;

        $obj = new stdClass();
        $obj->user_name = $context->FormData->first_name;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Verify my email";
        $obj->cta_link = $url . "account/verify?code=" . $access_token;
        $obj->group_name = $context->FormData->company;
        useclass("Email", $context);
        $em = new Email();
        $result = $em->SendEmail($context->FormData->signupEmail, $emailType, $obj);

        return $result;
    }
}
