<?php

class Audit
{
    public static function Write($context, $context_id, $user_id, $description)
    {
        $qry = "INSERT INTO `audititem`(`description`, `context`, `contextid`, `actionedby`)
                VALUES('$description', '$context' , $context_id, $user_id)";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $data = ['message' => Message::ObjectCreated];

        return $data;
    }

    public static function Get(int $id, int $group_id, $returnResult = true, array $filter = null)
    {
        $where = " where `user`.`group_id` = $group_id";
        if ($filter != null) {
            foreach ($filter as $key => $value) {
                $where .= " AND `audititem`.$key = '$value' ";
            }
        }

        if ($id > 0) {
            $where .= " AND `audititem`.`audit_id` = $id ";
        }

        $qry = "SELECT `audititem`.`id`, `description`, `user_profile`.`first_name`, `user_profile`.`last_name`, `email` as 'actionedby', `actioneddate`,`audititem`.`context`,`audititem`.`contextid` FROM audititem
        INNER JOIN `user` on `user`.`user_id` = `audititem`.`actionedby`
        LEFT JOIN `user_profile` on `user`.`user_id` = `user_profile`.`user_id`
 ";

        $qry .= $where;

        $qry .= ' order by actioneddate desc LIMIT 5';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult == true) {
            return new Result($data);
        }
        return $data;
    }
}
