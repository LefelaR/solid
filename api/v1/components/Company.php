<?php

class Company
{
    public $user_id;
    public $vatamount;
    public $vatnumber;
    public $group_id;
    public $line1;
    public $line2;
    public $town;
    public $city;
    public $postcode;
    public $file_contents;
    public $file_name;

    public static function Get($group_id, $fetchResult = true)
    {
        $qry = "SELECT `company`.*, `address`.* FROM `company` 
                left join `address` on `address`.`address_id` = `company`.`address_id`
                WHERE company.`group_id` = $group_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $logo_id = $data[0]['logo_id'];
            $avatar = Company::_GetCompanyLogo($logo_id, $group_id);
            $data[0]['logo'] = $avatar['logo'];
        }

        if ($fetchResult === true) {
            return new Result($data);
        }
        return $data;
    }

    // public static function Save(Company $model): Result
    // {
    //     if ($model->company_id > 0) {
    //         return Company::_Update($model);
    //     }
    //     return Company::_Insert($model);

    //     return new Result('');
    // }

    public static function _Update(Company $model): Result
    {
        $company = Company::Get($model->group_id, false);
        if (sizeof($company) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $address_id = $company[0]['address_id'];

        Company::_UpdateAddress($model, $address_id);

        $qry = "UPDATE `company` SET 
        `company_name` = '$model->company_name',
        `vatnumber` = '$model->vatnumber', 
        `vatamount` = '$model->vatamount',
        `company_telephone` = '$model->company_telephone',
        `company_email` = '$model->company_email',
        `invoice_prefix` = '$model->invoice_prefix'

        WHERE `group_id` = $model->group_id;";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);



        $fileContents = $model->file_contents;
        $fileName = $model->file_name;

        if (strlen($fileContents) > 0) {
            $fileHelper = new FileHelper();
            if ($company[0]['logo_id'] > 0) {

                $logo_id = $company[0]['logo_id'];
                $config = new Config('1.0');
                $qry = "select * from asset where asset_id = $logo_id";
                $db = new DataService($config->dbConnection);
                $asset = $db->ExecStatement($qry);

                $fileHelper->Update('company', $model->group_id, $asset[0]['filename'], $fileContents);
            } else {
                $asset_id = $fileHelper->Insert('company', $model->group_id, $fileName, $fileContents);
                $qry = "update company set logo_id = $asset_id where group_id = $model->group_id";
                $db = new DataService($config->dbConnection);
                $c = $db->ExecStatement($qry);
            }
        }

        Audit::Write('company', $model->group_id, $model->user_id, 'Company updated');

        // $company = (object)$company[0];

        return Company::Get($model->group_id);
    }

    public static function RemoveLogo($group_id, $user_id)
    {
        $company = Company::Get($group_id, false);
        if (sizeof($company) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        if ($company[0]['logo_id'] > 0) {
            $qry = "update company set logo_id = null where group_id = $group_id";
            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            Company::_DeleteLogo($company[0]['logo_id'], $group_id);
            Audit::Write('company', $group_id, $user_id, 'Company logo removed');
        }

        $data = [
            'Message' => Message::ObjectDeleted
        ];

        return new Result($data);
    }

    private static function _DeleteLogo($logo_id, $group_id)
    {
        if ($logo_id > 0) {
            $qry = "select asset.* from asset         
            where asset_id = $logo_id";
            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            if (sizeof($data) == 1) {
                $fileData = $data[0];
                $filename = $fileData['filename'];
                $fileHelper = new FileHelper();
                $file_contents = $fileHelper->DeleteFile('company', $group_id, $filename);

                $qry = "DELETE FROM `asset` WHERE `asset_id`= $logo_id;";

                $config = new Config('1.0');
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);

                return $data;
            }
        }
    }

    public static function _GetCompanyLogo($logo_id, $group_id)
    {
        $file_contents = '';

        if ($logo_id > 0) {
            $qry = "select asset.* from asset         
            where asset_id = $logo_id";
            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            if (sizeof($data) == 1) {
                $fileData = $data[0];
                $filename = $fileData['filename'];
                $fileHelper = new FileHelper();
                $file_contents = $fileHelper->GetFile($group_id, 'company', $filename);
            }
        }

        $data = [
            'logo' => $file_contents
        ];

        return $data;
    }

    private static function _UpdateAddress(Company $model, int $address_id)
    {
        $qry = "UPDATE `address` set `line1` = '$model->line1', `line2` = '$model->line2', `town` = '$model->town', `city` = '$model->city', `postcode` = '$model->postcode'
        WHERE `address_id` = $address_id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return $data;
    }

    // public static function _Insert(Company $model): Result
    // {
    //     $address_id = Company::_InsertAddress($model);
    //     $contact_id = Company::_InsertContact($model);

    //     $qry = "INSERT INTO `company`(`address_id`,`contact_id`,`group_id`,`company_date`,`company_end_date`,`notes`,`inspector_id`)
    //     VALUES($address_id, $contact_id, $model->group_id, '$model->company_date','$model->company_end_date', '$model->company_notes', $model->company_inspector_id);
    //     ";


    //     $config = new Config('1.0');
    //     $db = new DataService($config->dbConnection);
    //     $company_id = $db->ExecStatement($qry);

    //     $qry = "INSERT INTO `inspection`(`company_id`,`group_id`) VALUES ($company_id, $model->group_id);";

    //     $config = new Config('1.0');
    //     $db = new DataService($config->dbConnection);
    //     $inspection_id = $db->ExecStatement($qry);

    //     $qry = "UPDATE company SET inspection_id = $inspection_id WHERE company_id = $company_id";

    //     $config = new Config('1.0');
    //     $db = new DataService($config->dbConnection);
    //     $data = $db->ExecStatement($qry);

    //     $qry2 = "
    //     INSERT INTO `invoice`(`company_Id`)
    //     VALUES (' $company_id');
    //     ";
    //     $config = new Config('1.0');
    //     $db = new DataService($config->dbConnection);
    //     $Invoice_Id = $db->ExecStatement($qry2);


    //     $qry2 = " UPDATE `company` SET `invoice_id`=  $Invoice_Id WHERE `company_id`= $company_id;";
    //     $db->ExecStatement($qry2);

    //     Audit::Write('company', $company_id, $model->user_id, 'company created');

    //     $companyX = Company::Get($company_id, $model->group_id, false);

    //     $company = (object)$companyX[0];

    //     return new Result($companyX);
    // }
}
