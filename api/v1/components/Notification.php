<?php

class Notification
{
    public function __construct()
    { }

    public function Send($title, $body, $user_id, $category, $group_id = 0, $url = "")
    {
        if ($group_id == 0) {
            $this->SendUserMessage($title, $body, $category, $user_id, $url);
        } else {
            $this->SendGroupMessage($title, $body, $category,  $group_id, $url);
        }
    }

    private function SendGroupMessage($title, $body, $category,  $group_id, $url)
    {
        $config = new Config('1.0');
        $qry = "select * from user_group_access where `locked` = 0 AND `group_id` = " . $group_id;
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        foreach ($data as $item) {
            $this->SendUserMessage($title, $body, $category,  $item['user_id'], $url);
        }
    }

    private function CanSend($user, $category)
    {
        if ($user['notifications'] == 0) return false;

        switch (strtolower($category)) {
            case 'newsletters':
                return $user['newsletters'] == 1;
                break;
            case 'articles':
                return $user['articles'] == 1;
                break;
            case 'activity':
                return $user['activities'] == 1;
                break;
            default:
                return true;
                break;
        }
    }

    private function WriteLog($deviceJson, $data_sent, $fcm_response)
    {
        $config = new Config('1.0');
        // $qry = "select * from device where `user_id` = " . $user_id;
        $qry = "INSERT INTO notification_log (registrationkey, data_sent, fcm_response) values('$deviceJson','$data_sent', '$fcm_response')";
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
    }

    private function SendUserMessage($title, $body, $category,  $user_id, $notification_url)
    {
        $config = new Config('1.0');

        $qry = " select setting.* from account_setting as setting
        left join device on setting.user_id = device.user_id
        where setting.user_id = $user_id";

        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) > 0) {
            $canSend = $this->CanSend($data[0], $category);
            if ($canSend === false) {
                return;
            }
        }


        foreach ($data as $item) {
            if(!isset($item['registrationkey'])){
                // notifications was enabled but no device registered - something went wrong with registration
                return;
            }

            $deviceJson = $item['registrationkey'];

            $url = "/api/send-push-msg";
            $api = new ExternalAPIHandler("http://ec2-54-154-196-239.eu-west-1.compute.amazonaws.com:8000");
            $sub = $deviceJson;
            $data = array(
                "subscription" => json_decode($sub, true)['subscription'],
                "data" => array(
                    "title" => $title,
                    "body" => $body,
                    "icon" => "fav.png",
                )
            );

            if (strlen($notification_url) > 0) {
                $data['data']['url'] = $notification_url;
            }

            $encodedData = json_encode($data, JSON_UNESCAPED_SLASHES);

            $data['data'] = json_encode($data['data']);
            $data =  $api->Post($url,  array('Content-Type:application/json'), $data);

            $worked = false;
            if (isset($data['success'])) {
                if ($data['success'] == true) {
                    $this->WriteLog($deviceJson, $encodedData, json_encode($data));
                    $worked = true;
                }
            }


            if ($worked == false) {
                $device_id = $item['device_id'];
                $qry = "DELETE FROM `device` WHERE `device_id` = $device_id";

                $config = new Config('1.0');
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);

                return $data;
            }
        }
    }
}
