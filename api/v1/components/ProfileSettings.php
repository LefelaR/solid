<?php



class ProfileSettings
{

    static function get($ctx)
    {
        $user_id = $ctx->Profile->user_id;

        $config = new Config('1.0');
        $qry = "select * from account_setting  where `user_id`= $user_id ";
        $qry = sprintf($qry, $user_id);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 0) {
            $data = ProfileSettings::CreateDefaultForUser($user_id);
        }

        return new Result($data);
    }

    static function CreateDefaultForUser($user_id)
    {
        $qry = "SELECT profile_id FROM user_profile WHERE user_id = $user_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) > 0) {
            $profile_id = $data[0]['profile_id'];

            $qry = "INSERT INTO account_setting(`profile_id`, `user_id`,`notifications`, `articles`, `newsletters`,`status`,`activities`)
                    VALUES($profile_id,$user_id,0,0,0,0,0)";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            $qry = "select * from account_setting where `user_id` = $user_id";

            $config = new Config('1.0');
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            return $data;
        }

        return $data;
    }

    static function save($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $user_id = $ctx->Profile->user_id;

        $notifications = $ctx->FormData->notifications;
        $articles = $ctx->FormData->articles;
        $newsletters = $ctx->FormData->newsletters;
        $activities = $ctx->FormData->activities;

        $qry = "UPDATE account_setting SET  `notifications` = $notifications, `articles` = $articles, `newsletters` = $newsletters, `activities` = $activities WHERE `user_id` = $user_id";

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function subscribe($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $user_id = $ctx->Profile->user_id;

        $registrationkey = $ctx->FormData->registrationkey;

        $qry = "SELECT * FROM `device` WHERE `user_id` = $user_id and `registrationkey` = '$registrationkey'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 0) {
            $qry = "INSERT INTO device (`registrationkey`,`user_id`) values ('$registrationkey', $user_id);";
            $data = $db->ExecStatement($qry);
            $notification = new Notification();
            $notification->Send('Welcome to Nova', 'You have been subscribed to receive push notifications', $user_id, 'settings', 0, '');
            return new Result($data);
        }
        return new Result(['message' => "Already registered"]);
    }

    static function unsubscribe($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $user_id = $ctx->Profile->user_id;

        $registrationkey = $ctx->FormData->registrationkey;

        $qry = "DELETE FROM device WHERE `registrationkey` = '$registrationkey' AND `user_id` = $user_id;";

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }
}
