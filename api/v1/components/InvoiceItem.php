<?php
use function GuzzleHttp\json_encode;

class  InvoiceItem
{

    public $Invoice_Id;
    public $Item_Id;
    public $group_id;
    public $user_id;
    public $Description;
    // public $Quantity;
    public $Price;

    public static function Get(int $Invoice_Id, int $Item_Id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                        `invoice_item`.`Item_Id`,
                        `invoice_item`.`Invoice_Id`,
                        `invoice_item`.`Description`,
                        `invoice_item`.`Price`
                FROM `invoice_item`
                left join invoice on invoice.Invoice_Id = invoice_item.Invoice_Id';


        $qry .= " where `invoice_item`.`Deleted` = 0 ";


        $qry .= " and invoice_item.Invoice_Id = $Invoice_Id";

        if ($Item_Id > 0) {
            $qry .= " and invoice_item.Item_Id = $Item_Id";
        }


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($Item_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }


    public static function GetReport($token)
    {
        if ($token === null)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $qry = "SELECT * from invoice_share where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) == 0)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $invoice_id = $data[0]['invoice_id'];

        $qry = 'SELECT 
                        `invoice_item`.`Item_Id`,
                        `invoice_item`.`Invoice_Id`,
                        `invoice_item`.`Description`,
                        `invoice_item`.`Price`
                FROM `invoice_item`
                left join invoice on invoice.Invoice_Id = invoice_item.Invoice_Id';


        $qry .= " where `invoice_item`.`Deleted` = 0 ";


        $qry .= " and invoice_item.Invoice_Id = $invoice_id";


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    public static function Save(InvoiceItem $model)
    {
        if ($model->Item_Id > 0) {
            return InvoiceItem::_Update($model);
        }
        return InvoiceItem::_Insert($model);
    }
    private static function _Insert(InvoiceItem $model)
    {

        // $qry = "call api_insert_item(%d,'%s',%d)";
        // $qry = sprintf($qry, $model->Invoice_Id, $model->Description, $model->Price);


        $qry = "
        INSERT INTO `invoice_item`(`Invoice_Id`,`Description`,`Price`)
        VALUES ($model->Invoice_Id, '$model->Description', '$model->Price');
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $Item_Id = $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM invoice_item where Invoice_Id = $model->Invoice_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

        $qry3 = "UPDATE `Invoice` SET `Total_Amount` =  $total WHERE `Invoice_Id` = '$model->Invoice_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectCreated]);
    }


    private static function _Update(InvoiceItem $model)
    {
        $qry = "UPDATE `invoice_item` SET `Description` = '$model->Description', `Price` = '$model->Price'
        WHERE `Item_Id` = $model->Item_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM invoice_item where Invoice_Id = $model->Invoice_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

        $qry3 = "UPDATE `Invoice` SET `Total_Amount` =  $total WHERE `Invoice_Id` = '$model->Invoice_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectUpdated]);
    }


    public static function Delete(InvoiceItem $model)
    {
        $Invoice = InvoiceItem::Get($model->Invoice_Id, $model->Item_Id, $model->group_id, false);
        if (sizeof($Invoice) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "DELETE FROM `invoice_item` WHERE `Item_Id` = $model->Item_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        $qry2 = "SELECT SUM(Price) As result FROM invoice_item where Invoice_Id = $model->Invoice_Id";
        $result = $db->ExecStatement($qry2);

        $total = $result[0]["result"];

        if ($total == "") {
            $total = 0;
        }

        $qry3 = "UPDATE `Invoice` SET `Total_Amount` =  $total WHERE `Invoice_Id` = '$model->Invoice_Id';";
        $db->ExecStatement($qry3);

        return new Result(['message' => Message::ObjectDeleted]);
    }
}
