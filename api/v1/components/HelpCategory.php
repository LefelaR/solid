<?php

class HelpCategory
{
    static function get($ctx)
    {

        $id = 0;
        $groupid = 0;
        if (sizeof($ctx->Route->id) == 1) {
            $id = intval($ctx->Route->id[0]);
        }

        $groupid = $ctx->Profile->group_id;

        $qry = '
        select hc.*, count(ht.category_id) as topic_count, group_concat(hct.help_target_id) as targets from `help_category` hc
        left join `help_topic` ht on ht.`category_id` = hc.`category_id`
        left join `help_category_target` hct on hct.help_category_Id = hc.category_id
                where hc.`group_id` in (0, %d)
                            
                ';
        $qry = sprintf($qry, $groupid);

        if ($id > 0) {
            $qry .= ' and hc.category_id = ' . $id;
        }
        $qry .=  '  group by hc.`category_id` order by hc.category_name asc   ';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function save($ctx)
    {

        $id = 0;

        if (sizeof($ctx->Route->id) == 1) {
            $id = intval($ctx->Route->id[0]);
        }

        if ($id > 0) {
            return HelpCategory::update($ctx);
        } else {
            return HelpCategory::insert($ctx);
        }
    }

    static function insert($ctx)
    {

        $groupid = $ctx->Profile->group_id;

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $qry = "insert into help_category (`category_name`, `group_id`) values ('%s', %d)";
        $qry = sprintf($qry, mysqli_real_escape_string($db->conn, $ctx->FormData->category_name), $groupid);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);


        if(isset($ctx->FormData->targets) && ($data > 0)){
            $targets = explode(',', $ctx->FormData->targets);            

            foreach($targets as $target){
                $qry = "insert into help_category_target (`help_category_id`, `help_target_id`) values (%d, %d)";
                $qry = sprintf($qry, $data, $target);
                $db = new DataService($config->dbConnection);
                $db->ExecStatement($qry);
            }
        }

        
        return new Result($data);
    }

    static function update($ctx)
    {

        $categoryid = intval($ctx->Route->id[0]);


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $qry = "update help_category set `category_name` = '%s' where `category_id` = %d";
        $qry = sprintf($qry, mysqli_real_escape_string($db->conn, $ctx->FormData->category_name), $categoryid);
        $data = $db->ExecStatement($qry);

        if(isset($ctx->FormData->targets) && ($categoryid > 0)){
            $targets = explode(',', $ctx->FormData->targets);            

            $qry = "delete from help_category_target where help_category_id = $categoryid";                
            $db = new DataService($config->dbConnection);
            $db->ExecStatement($qry);

            foreach($targets as $target){
                $qry = "insert into help_category_target (`help_category_id`, `help_target_id`) values (%d, %d)";
                $qry = sprintf($qry,$categoryid, $target);
                $db = new DataService($config->dbConnection);
                $db->ExecStatement($qry);
            }
        }

        return new Result($data);
    }

    static function delete($id = 0)
    {

        $qry = 'delete from help_category where category_id = ' . $id;

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        
        $data = $db->ExecStatement($qry);

        
        return new Result($data);
    }
}
 