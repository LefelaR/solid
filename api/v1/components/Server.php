<?php 


class Server{      
    private $Context;
    public function __construct($context)
    {
            $this->Context =$context;
    }
    function pingall(){  
        
        $config = new Config('1.0');
        $qry = 'select * from domains';
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry); 
        foreach ($data as $key => $value){
            $domain = $value['url'];
            $id = $value['id'];
            $ret = $this->_ping($domain, 80, 10,$id);
            if ($ret == 'drowned'){
                $qry = "UPDATE domains SET live_status='offline', ping_status = 0 WHERE id= '$id'";
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);
                
            }
            else if($ret > 0){
                $qry = "UPDATE domains SET live_status='online', ping_status = '$ret', `date` = DATE_FORMAT(NOW(),'%i:%s')  WHERE id= '$id'";
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);
                
            } 
                
        }
        return $ret;
            
    }

    function checkall(){  
        
        $config = new Config('1.0');
        $qry = 'select * from domains';
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry); 
        foreach ($data as $key => $value){
            $domain = $value['url'];
            $id = $value['id'];
            
            $date1=date_create($value['date'],new DateTimeZone('Africa/Johannesburg'));
            $date2=new DateTime("now", new DateTimeZone('Africa/Johannesburg'));
            $diff=date_diff($date2,$date1,false);

            if($diff->i >= 5)
            {
                $qry = "UPDATE domains SET live_status='offline', ping_status = 0 WHERE id= '$id'";
                $db = new DataService($config->dbConnection);
                $data = $db->ExecStatement($qry);
            }                           
        }
        return [];
            
    }

    private function _ping($host, $port, $timeout,$id) {            
        $tB = microtime(true); 
        $fP = fSockOpen($host, $port, $errno, $errstr, $timeout); 
        if (!$fP) { return "drowned";  } 
        $tA = microtime(true); 
        return round((($tA - $tB) * 1000), 0)." ms";     
    }   

    public function ping(){   
        
        $config = new Config('1.0');

        $id = intval($this->Context->FormData->domain_id);

        $qry = "UPDATE domains SET live_status='online', ping_status = '', `date` = NOW() WHERE id= '$id'";
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        
        return $data;
    }

}
   
?>