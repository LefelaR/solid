<?php 

class Rating{

    public static function GetRating()
    {

            $qry = "select 
            `inspection`.`inspection_id`,
            `inspection`.`inspection_status` as `inspection_status_id`,
            `inspection`.`template_type`,
            `inspection`.`booking_id`,
            `inspection`.`group_id`,
            iType.type as `inspection_type`,
            `inspection`.`inspection_type` as `inspection_type_id`,
            `inspection`.`inspection_name`,
            `inspection`.`created_date`
        
            from inspection
            left join inspection_type iType on iType.value = inspection.inspection_type
            left join rating on `inspection`.`inspection_id` = `rating`.`model_id` AND `model` like `inspection_type`;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    public static function InsertRating($context){

     $model = 'inpsection';
     $model_id = 1;

        $qry = "INSERT INTO `rating`(`model`,`model_id`, `rating`, `comments`) VALUES ('%s', %d, %d, '%s');";

        $config = new Config('1.0');
        $qry = sprintf($qry, $model, $model_id , $context->FormData->stars, $context->FormData->comment);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }
}

?>