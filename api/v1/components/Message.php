<?php

class Message
{
    const Verified = 'You are verified';
    const InvalidToken = 'Invalid token supplied. Please try again.';
    const AlreadyRegistered = 'This email is already registered. Please try again or use a different email.';
    const AccountLocked = 'This account is locked.';
    const InvalidCredentials = 'Invalid Credentials.';

    const ObjectCreated = "Object Created.";
    const ObjectDeleted = "Object Deleted.";
    const ObjectUpdated = "Object Updated";
    const ObjectNotFound = "Object not found.";       
    const InvalidFormData = "Invalid form data provided.";            
}
