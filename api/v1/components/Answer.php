<?php

class Answer
{
    public $question_id;
    public $answer_id;
    public $group_id;
    public $user_id;
    public $answer;
    public $comment;

    public static function Get(int $question_id, $answer_id, int $group_id, array $filter = null): Result
    {
        $qry = 'select * from answer';

        $qry .= " where group_id = $group_id and question_id = $question_id";

        if ($answer_id > 0) {
            $qry .= " and answer_id = $answer_id";
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($answer_id > 0 && (sizeof($data) == 0)) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }
        return new Result($data);
    }

    public static function Save(Answer $model): Result
    {
        if($model->question_id === 0) {
            return new ErrorResult('[question_id] is required', 404);
        }

        if($model->answer_id > 0){
            return Answer::_Update($model);
        }
        return Answer::_Insert($model);
    }

    private static function _Update(Answer $model) : Result{
        return new Result("Updated");
    }

    private static function _Insert(Answer $model) : Result{
        return new Result("Inserted");
    }
}
