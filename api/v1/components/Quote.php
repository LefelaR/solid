<?php


class  Quote
{

    public $Booking_Id;
    public $Quote_Id;
    public $group_id;
    public $user_id;
    public $Quote_To;
    public $Quote_Ref;
    public $email;

    public static function Get(int $Booking_Id, int $Quote_Id, int $group_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                    quote.*,
                    company.`vatamount`,
                    company.`invoice_prefix`,
                    booking.*, 
                    contact.*,
                    address.*,
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress
                FROM quote
                left join booking on booking.Booking_Id = quote.Booking_Id
                left join address on address.address_id = booking.address_id
            
                left join company on company.group_id = booking.group_id
                left join contact on contact.contact_id = booking.contact_id';


        $qry .= " where booking.group_id = $group_id AND quote.deleted = 0 ";


        $qry .= " and quote.Booking_Id = $Booking_Id";

        if ($Quote_Id > 0) {
            $qry .= " and quote.Quote_Id = $Quote_Id";
        }
        $qry .= ' order by quote.Created_Date desc';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if ($returnResult === true) {
            if ($Quote_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }

    public static function GetReport($token)
    {
        if ($token === null)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $qry = "SELECT * from quote_share where token = '$token'";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) == 0)
            return new ErrorResult(Message::ObjectNotFound, 404);

        $quote_id = $data[0]['quote_id'];

        $qry = 'SELECT 
            quote.*,
            booking.*, 
            contact.*,
            address.*,
            CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress,
            `user_group`.`group_name`,
            company.*
        FROM quote
        left join booking on booking.Booking_Id = quote.Booking_Id
        left join address on address.address_id = booking.address_id    
        left join company on company.group_id = booking.group_id
        left join user_group on `user_group`.`group_id` = `company`.`group_id`
        left join contact on contact.contact_id = booking.contact_id';

        $qry .= ' WHERE quote.Quote_Id = ' . $quote_id;


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }


    public static function Save(Quote $model)
    {
        if ($model->Quote_Id > 0) {
            return Quote::_Update($model);
        }
        return Quote::_Insert($model);
    }
    private static function _Insert(Quote $model)
    {

        $qry = "
        INSERT INTO `quote`(`Booking_Id`)
        VALUES ('$model->Booking_Id');
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $Quote_Id = $db->ExecStatement($qry);


        $qry2 = " UPDATE `booking` SET `quote_id`=  $Quote_Id WHERE `booking_id`= $model->Booking_Id;";
        $db->ExecStatement($qry2);

        Audit::Write('quote', $Quote_Id, $model->user_id, 'Quote added');

        return Quote::Get($model->Booking_Id, $Quote_Id, $model->group_id);
    }


    private static function _Update(Quote $model)
    {
        $qry = "UPDATE `quote` SET `Quote_To` = '$model->Quote_To', `Quote_Ref`  = '$model->Quote_Ref'
        WHERE `Quote_Id` = $model->Quote_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);

        Audit::Write('Quote', $model->Quote_Id, $model->user_id, 'Quote updated');

        return Quote::Get($model->Booking_Id, $model->Quote_Id, $model->group_id);
    }


    public static function Delete(Quote $model)
    {
        $Quote = Quote::Get($model->Booking_Id, $model->Quote_Id, $model->group_id, false);
        if (sizeof($Quote) === 0) {
            return new ErrorResult(Message::ObjectNotFound, 404);
        }

        $qry = "UPDATE `quote` SET `quote`.`Deleted` = 1  WHERE `Quote_Id` = $model->Quote_Id;";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $db->ExecStatement($qry);


        $qry2 = " UPDATE `booking` SET `quote_id`=NULL WHERE `booking_id`= $model->Booking_Id;";
        $db->ExecStatement($qry2);


        Audit::Write('Quote', $model->Quote_Id, $model->user_id, 'Quote deleted');

        return new Result(['message' => Message::ObjectDeleted]);
    }

    public static function GetQuoteShare(int $Quote_Id, int $group_id, $user_id, bool $returnResult = true, array $filter = null)
    {
        $qry = 'SELECT 
                    quote_share.*,
                    quote.*,
                    company.*,
                    booking.*, 
                    contact.*,
          
                    address.*,
                    CASE WHEN LENGTH(line2) > 2 THEN CONCAT(line1,",",line2,",",town,",",city,",",postcode) ELSE CONCAT(line1,",",town,",",city,",",postcode) END AS fulladdress
                FROM quote_share
                join quote on quote.quote_id = quote_share.quote_id
                left join booking on booking.Booking_Id = quote.Booking_Id
                left join address on address.address_id = booking.address_id
            
                left join company on company.group_id = booking.group_id
                left join contact on contact.contact_id = booking.contact_id';

        $qry .= " where quote.deleted = 0 and quote_share.user_id = $user_id";

        if ($Quote_Id > 0) {
            $qry .= " and quote_share.quote_id = $Quote_Id";
        }
        $qry .= ' order by quote_share.sharedate desc';

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $logo_id = $data[0]['logo_id'];
            $group_quote_id = $data[0]['group_id'];
            $avatar = Company::_GetCompanyLogo($logo_id, $group_quote_id);
            $data[0]['logo'] = $avatar['logo'];
        }

        if ($returnResult === true) {
            if ($Quote_Id > 0 && (sizeof($data) == 0)) {
                return new ErrorResult(Message::ObjectNotFound, 404);
            }
            return new Result($data);
        }
        return $data;
    }
}
