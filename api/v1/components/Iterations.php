<?php 

class Iterations
{
    public function __construct()
    { }


    static function get($ctx)
    {

        $config = new Config('1.0');

        $project_id = $ctx->Route->id[0];
        $qry = "select * from iteration where project_id = $project_id"; //$context->Route->id[0]

        $iteration_id = 0;
        if(sizeof($ctx->Route->id) == 2){
            $iteration_id = $ctx->Route->id[1];
            if($iteration_id > 0){
                $qry .= " and iteration_id = $iteration_id";
            }
        }


        $qry .= ' order by created_date desc'; 

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function iterationsAll($ctx)
    {
 
    // static function iterations_activity($ctx)
    // {

        $config = new Config('1.0');
        $project_id = $ctx->Route->id[0];
        $iteration_id = 0;
        $qry = "select `description` from `iteration` where project_id = $project_id"; //$context->Route->id[0]

        $qry .= ' order by created_date desc'; 
    //     $qry = "SELECT * FROM `activity` where `iteration_id` = %d and `project_id` = %d";

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }


static function activity($ctx){

    $config = new Config('1.0');
    $project_id = $ctx->Route->id[0];
    $iteration_id = 0;
    $qry = "SELECT * FROM `activity` where `iteration_id` = %d and `project_id` = %d"; //$context->Route->id[0]
    $qry .= ' order by created_date desc'; 
    $qry = sprintf($qry, $iteration_id, $project_id );
    $db = new DataService($config->dbConnection);

    $data = $db->ExecStatement($qry);

    return new Result($data);
}


    static function save($ctx)
    {
        $iteration_id = 0;
        if (isset($ctx->Route->id[1])) {
            $iteration_id = $ctx->Route->id[1];
        }
        if ($iteration_id > 0) {
            return Iterations::update($ctx);
        }
        return Iterations::create($ctx);
    }

    private static function create($ctx)
    {
        $config = new Config('1.0');

        $db = new DataService($config->dbConnection);

        $project_id = $ctx->Route->id[0];

        $name = mysqli_real_escape_string($db->conn,$ctx->FormData->description);
        $qry = "INSERT INTO iteration (`description`,`project_id`) VALUES ('%s', %d);";
        $qry = sprintf($qry, $name, $project_id);



        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    private static function update($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $description = mysqli_real_escape_string($db->conn,$ctx->FormData->description);
        $iteration_id = $ctx->Route->id[1];

        $qry = "UPDATE iteration SET  `description` = '$description' WHERE iteration_id = $iteration_id;";


        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function delete($ctx)
    {
        $config = new Config('1.0');

        $db = new DataService($config->dbConnection);

        $iteration_id = $ctx->Route->id[1];

        $qry = "DELETE FROM iteration where iteration_id = $iteration_id;";
        
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

}
 