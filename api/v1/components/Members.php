<?php

class Members
{
    static function get($ctx)
    {
        $current_user_id = $ctx->Profile->user_id;
        $group_id = $ctx->Profile->group_id;

        $user_id = isset($ctx->Route->id[0]) ? $ctx->Route->id[0] : 0;

        $config = new Config('1.0');
        $qry = "select
            `user_profile`.`user_id`,
            `user_profile`.`first_name`,
            `user_profile`.`last_name`,
            `user_profile`.`group_name`,
            `user_profile`.`mobile_number`,
            `user_profile`.`address_1`,
            `user_profile`.`address_2`,
            `user_profile`.`town`,
            `user_profile`.`postal_code`,
            `user_profile`.`city`,
            `user_group_access`.`group_id`,
            `user_group_access`.`locked`,
            `user`.`email`,
            `user`.`created_date`,
            `user`.`signed_in_date`,
            GROUP_CONCAT(role.role) as user_role,
            CASE WHEN `user_profile`.`user_id` = '$current_user_id' THEN 1 ELSE 0 END AS `current_user`
        from user_profile 
        left join user_group_access on `user_profile`.`user_id` = `user_group_access`.`user_id` left join   user_group on `user_group`.`group_id` = `user_group_access`.`user_id`
        left join  user on `user`.`user_id`=`user_profile`.`user_id`         
        left join role on role.value = user_group_access.group_role
        where `user_group_access`.`group_id` like  '$group_id'";

        if ($user_id > 0) {
            $qry .= " AND `user_profile`.`user_id` = $user_id ";
        }

        $qry  .=  "group by user.user_id        
        order by user_profile.last_name, user_profile.first_name

        ";
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data)) {
            foreach ($data as $key => $item) {
                $context = new Context;
                $context->profile = $item;
                useClass('Profile', $context);
                $avatar = Profile::GetProfileImage($item['user_id']);
                $data[$key]['avatar'] = $avatar['avatar'];
            }
        }
        return new Result($data);
    }


    static function update($ctx)
    {
        $config = new Config('1.0');
        $qry = "UPDATE `user_group_access` set `locked` = %d where `user_id`=" . $ctx->FormData->id;
        $qry = sprintf($qry, $ctx->FormData->locked);
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    static function grantaccess($user_id, $current_user, $group_id)
    {
        $qry = "
            UPDATE `user_group_access` set `locked` = 0 WHERE `user_id` = $user_id AND `group_id` = $group_id
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('user', $user_id, $current_user, 'User granted access');

        $data = [
            'Message' => Message::ObjectUpdated
        ];

        return new Result($data);
    }

    static function delete($user_id, $current_user, $group_id, $ctx)
    {

        $config = new Config('1.0');
        $url = $config->siteurl;


        $qry = "SELECT * FROM user where `user_id` =" . $user_id;
        $db = new DataService($config->dbConnection);
        $result = $db->ExecStatement($qry);
        $to = $result[0]['email'];

        $qry = "
            DELETE FROM `user_group_access` WHERE `user_id` = $user_id AND `group_id` = $group_id
        ";
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Members::RemoveSharing($group_id, $user_id);

        $obj = new stdClass();
        $obj->user_name = $to;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Team";
        $obj->cta_link = $url . "account/signin";
        useclass("Email", $ctx);
        $em = new Email();
        $em->SendEmail($to, 8, $obj);



        Audit::Write('user', $user_id, $current_user, 'User removed from group');

        $data = [
            'Message' => Message::ObjectDeleted
        ];

        return new Result($data);
    }

    static function leaveteam($user_id, $current_user, $group_id)
    {
        $qry = "SELECT * FROM `user_group_access` WHERE `group_id` = $group_id AND `user_id` != $user_id AND `group_role` = 1";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 0) {
            $data = [
                'Message' => 'Cannot leave team if you are the only administrator. Assign another administrator to take your place first.'
            ];

            return new ErrorResult($data, 400);
        }


        $qry = "
            DELETE FROM `user_group_access` WHERE `user_id` = $user_id AND `group_id` = $group_id
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Members::RemoveSharing($group_id, $user_id);

        Audit::Write('user', $user_id, $current_user, 'User left group');

        $qry = "SELECT * FROM user_group_access WHERE user_id = $user_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $accessToken = $data[0]['token'];

        setcookie("crest_auth", $accessToken, time() + 60 * 60 * 24 * 2, '/');

        $data = [
            'Message' => Message::ObjectDeleted
        ];

        return new Result($data);
    }

    private static function RemoveSharing($group_id, $user_id)
    {
        $qry = "api_DeleteUserFromTeamSharing($group_id,$user_id)";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->exec($qry);

        return $data;
    }

    static function search($group_id, $searchstring)
    {
        if (strlen($searchstring) === 0) return new ErrorResult("[Search] must be defined. Please try again.", 400);

        $qry = "select user.user_id, first_name,last_name, email from user
        left join user_group_access uga on uga.user_id = user.user_id
        left join user_profile on user_profile.user_id = user.user_id            
        where email like '%$searchstring%' and user.user_id not in (
        select user_id from user_group_access where group_id = $group_id); ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    static function add($user_id, $current_user, $group_id)
    {
        $qry = "SELECT * FROM user_group_access WHERE user_id = $user_id AND group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) return new ErrorResult("This user has already been added to this group.", 400);

        for ($s = '', $i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz0123456789') - 1; $i != 32; $x = rand(0, $z), $s .= $a{
            $x}, $i++);

        $access_token = $s;

        $qry = "INSERT INTO user_group_access(`user_id`,`group_id`, `group_role`, `locked`, `token`)
                VALUES($user_id,$group_id,2, 0, '$access_token');
                ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        Audit::Write('user', $user_id, $current_user, 'User added to group');

        $data = [
            'message' => Message::ObjectCreated
        ];

        return new Result($data);
    }

    static function HasAccess($current_user, $group_id)
    {
        $qry = "SELECT * FROM user_group_access WHERE user_id = $current_user AND group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        if (sizeof($data) > 0) {
            $userGroupDetails = (object)$data[0];           
            return ($userGroupDetails->group_role == 1 && $userGroupDetails->locked == 0);
        }
        return false;
    }

    static function changerole($user_id, $current_user, $group_id, $role)
    {
        if ($role == 0) return new ErrorResult("Invalid role. Please try again.", 400);

        $hasAccess = Members::HasAccess($current_user, $group_id);

        if ($hasAccess == false) {
            return new ErrorResult("You do not have access to perform this action", 401);
        }

        $qry = "UPDATE user_group_access SET group_role = $role WHERE user_id = $user_id and group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);


        $qry = "select `role` from `role` where `value` = $role";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $role = $data[0]['role'];

        Audit::Write('user', $user_id, $current_user, 'User role changed to ' . $role);

        $data = [
            'message' => Message::ObjectUpdated
        ];

        return new Result($data);
    }
}
