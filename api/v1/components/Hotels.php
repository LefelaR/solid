<?php

class Hotels
{
    private $Context = null;
    public function __construct($context)
    {
        $this->Context = $context;
        useClass('Notification,Helpers/FileHelper', $context);
    }

    private function getFilter(array $filterFields): object
    {
        $data = [];
        $ctx = $this->Context;

        foreach ($filterFields as $key) {

            $value = '';
            if (isset($ctx->Filter->Options->$key)) {
                $value = $ctx->Filter->Options->$key;
            }

            $data[$key] = $value;
        }

        return (object)$data;
    }



    public function Get()
    {
        $ctx = $this->Context;
        $id = 0;

        if (isset($ctx->Route->id[0])) {
            $id = $ctx->Route->id[0];
        }

        $filterFields = [
            'groupid', 'target', 'status', 'view', 'count', 'orderby', 'category', 'offset'
        ];
        $filter = $this->getFilter($filterFields);

        $qry = 'select t.*, c.`category_name`, s.`status`, t.`status` as `status_id`, REPLACE(t.title," ","") as slug, asset.filename
                from help_topic t
                left join help_category c on c.category_id = t.category_id
                left join help_topic_status s on t.status = s.value
                left join help_category_target hct on hct.help_category_id = c.category_id
                left join help_target ht on ht.value = hct.help_target_id
                left join asset on asset.asset_id = t.cover_image_id
         ';


        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    
}
