<?php
class Profile
{

    static function get($user_id)
    {
        $config = new Config('1.0');
        $qry = "select first_name, last_name, email, mobile_number, address_1, address_2,town,postal_code, city, CASE WHEN `confirmation_token` is null or length(`confirmation_token`) = 0 THEN 1 ELSE 0 END as verified from user u 
        left join user_profile up on up.user_id = u.user_id        
         where u.`user_id`=" . $user_id;
        $qry = sprintf($qry, $user_id);

        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $access = Profile::GetAccessDetails($user_id);
            $avatar = Profile::GetProfileImage($user_id);
            $data[0]['avatar'] = $avatar['avatar'];
            $data[0]['group_locked'] = $access[0]['locked'];
        }

        return new Result($data);
    }

    static function save($ctx)
    {
        require_once './components/Helpers/EmailSender.php';
        $user_id = $ctx->Profile->user_id;
     
        $config = new Config('1.0');
        $qry = "update  user_profile set `first_name` = '%s' , `last_name` = '%s'  ,`mobile_number` = '%s' , `address_1` = '%s' ,
        `address_2` = '%s', `town` = '%s' , `postal_code`= %d , `city`='%s'  where  `user_id` =" . $user_id;
        $qry = sprintf($qry, $ctx->FormData->first_name, $ctx->FormData->last_name, $ctx->FormData->telephone, $ctx->FormData->address_1, $ctx->FormData->address_2,
        $ctx->FormData->town,$ctx->FormData->postal_code, $ctx->FormData->city );
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        $profile = $ctx->Profile;
        Profile::updateEmail($ctx->FormData->email, $user_id, $profile);
        Profile::UpdateProfileImage($ctx->FormData, $user_id);

        return new Result($data);
    }

    static function UpdateEmail($email, $user_id, $profile)
    {

        $isDifferantEmail = strtolower($profile->email) !== strtolower($email);
        if ($isDifferantEmail === true) {
            $config = new Config('1.0');

            $qry = "update user set `email` = '$email' where  `user_id` =" . $user_id;
            $db = new DataService($config->dbConnection);
            $data = $db->ExecStatement($qry);

            for ($s = '', $i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz0123456789') - 1; $i != 32; $x = rand(0, $z), $s .= $a{
                $x}, $i++);
            $confirmationToken = $s;

            $url = $config->siteurl;
            $to = $email;
            $subject = 'Lucid Ocean:: Email Change Requested.';
            $body = "An email change was requested. Please follow the link below to verify your email account. <br/>";
            $body .= "<a href='" . $url . "account/verify?code=" . $s . "'>Verify your email</a> <br />";
            $msg_enc = quoted_printable_encode($body);

            $emailSender = new EmailSender();
            $success = $emailSender->SendEmail($to, $subject, $body);
        }
    }

    static  function GetAccessDetails($user_id)
    {
        $config = new Config('1.0');
        $qry = "select * from user_group_access where user_id = $user_id";

        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return $data;
    }

    static function GetProfileImage($user_id)
    {
        $file_contents = '';

        $qry = "select asset.* from user_profile 
        left join asset on asset.asset_id = user_profile.asset_id
        where user_id = $user_id";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        if (sizeof($data) == 1) {
            $fileData = $data[0];
            $filename = $fileData['filename'];
            $fileHelper = new FileHelper();
            $file_contents = $fileHelper->GetFile($user_id, 'user_profile', $filename);
        }
        $data = [
            'avatar' => $file_contents
        ];

        return $data;
    }

    static function UpdateProfileImage($formData, $user_id)
    {
        if (!isset($formData->file_name)) return;

        $config = new Config('1.0');
        $qry = "select * from user_profile where user_id = $user_id";
        $db = new DataService($config->dbConnection);
        $profile = $db->ExecStatement($qry);

        $config = new Config('1.0');
        $qry = "select * from asset where context_id = $user_id and context = 'user_profile'";
        $db = new DataService($config->dbConnection);
        $asset = $db->ExecStatement($qry);
        $fileHelper = new FileHelper();

        $fileContents = $formData->file_contents;
        $fileName = $formData->file_name;

        if ($profile[0]['asset_id'] > 0) {


            $fileHelper->Update('user_profile', $user_id, $asset[0]['filename'], $fileContents);
        } else {
            $asset_id = $fileHelper->Insert('user_profile', $user_id, $fileName, $fileContents);

            $qry = "update user_profile set asset_id = $asset_id where user_id = $user_id";
            $db = new DataService($config->dbConnection);
            $profile = $db->ExecStatement($qry);
        }
    }

    static function DeleteProfileImage($ctx)
    {

        // if (!isset($formData->file_name)) return;
        $user_id = $ctx->Profile->user_id;

        $config = new Config('1.0');
        $qry = "select * from user_profile where user_id = $user_id";
        $db = new DataService($config->dbConnection);
        $profile = $db->ExecStatement($qry);
        // $context = "user_profile";
        $qry = "select * from `asset` where context_id =" . $user_id;
        $db = new DataService($config->dbConnection);
        $fileHelp = $db->ExecStatement($qry);
        $fileHelper = new FileHelper();
        $fileName = $fileHelp[0]['filename'];
        $qry = "update `user_profile` set `asset_id` = 0 where `user_id`=" . $user_id;
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $qry = "delete from `asset` where `context_id` = %d and `context` like 'user_profile'";
        $qry = sprintf($qry, $user_id);
        $db = new DataService($config->dbConnection);
        $del =  $db->ExecStatement($qry);

        if ($profile[0]['asset_id'] > 0)
            $fileHelper = new FileHelper();
        return $fileHelper->DeleteFile('user_profile', $user_id, $fileName);


        return $profile;
    }

    static function setgroup($ctx)
    {
        $user_id = $ctx->Profile->user_id;
        $group_id = $ctx->FormData->group_id;

        $qry = "UPDATE user 
        LEFT JOIN user_group_access uag on uag.user_id = user.user_id
        SET user.access_token = uag.token, user.group_id = uag.group_id
        WHERE uag.user_id = $user_id AND uag.group_id = $group_id
        ";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        $_SESSION['profile'] = null;

        $qry = "select * from user where user_id = $user_id and group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        $remember_me = $data[0]['remember_me'] == 1 ? true : false;
        $accessToken = $data[0]['access_token'];

        $datax = [];
        foreach($data as $item){
            unset($item['password']);
            array_push($datax, $item);
        }

        if ($remember_me == true) {
            setcookie("crest_auth", $accessToken, time() + 60 * 60 * 24 * 2, '/');
        }

        if ($remember_me == false) {
            setcookie("crest_auth", $accessToken, time() + 60 * 60, '/');
        }

        return new Result($datax);
    }

    static function getgroup($ctx)
    {
        $group_id = $ctx->Profile->group_id;

        $qry = "SELECT * FROM user_group WHERE group_id = $group_id";

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }

    static function groups($ctx)
    {
        $user_id = $ctx->Profile->user_id;

        $config = new Config('1.0');
        $qry = "select 
                `user`.`user_id`,
                `user`.`email`,
                `user`.`validated`,
                `user_group_access`.`group_id`,
                `user`.`created_date`,
                `user_group_access`.`token` as `access_token`,
                `user`.`signed_in_date`,
                `user_group_access`.`locked`,
                `user`.`confirmation_token`,
                `user`.`remember_me`,
                `user_group`.`group_name`,
                `role`.`role` as `user_role`,
                `user_group`.`personal`
                from `user` 
            left join `user_group_access` on `user_group_access`.`user_id` = `user`.`user_id`
            left join `user_group` on `user_group`.`group_id` = `user_group_access`.`group_id`
            join `role` on `role`.`value` = `user_group_access`.`group_role`
            
            WHERE user.user_id = " . $user_id . " and user_group.personal = 0";
        $qry = sprintf($qry, $user_id);

        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);

        return new Result($data);
    }
}
