<?php 

class Target
{
    private $Context;
    public function __construct($context)
    {
        $this->Context = $context;
    }

    public function Get()
    {
        $ctx = $this->Context;
        $id = 0;
        $groupid = 0;
        if (sizeof($ctx->Route->id) == 1) {
            $id = intval($ctx->Route->id[0]);
        }

        $groupid = $ctx->Profile->group_id;

        $qry = 'select * from help_target';

        if ($id > 0) {
            $qry .= ' and target_id = ' . $id;
        }

        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $data = $db->ExecStatement($qry);
        return new Result($data);
    }
}
 