<?php



class Project
{

    static function get($ctx)
    {
        $role = $ctx->Profile->user_role;
        if (contains($role, 'superadmin')) {
            return Project::getSuperAdmin($ctx);
        }
        else{
            return Project::getAdmin($ctx);
        }
    }

    private static function getAdmin($ctx)
    {
        $config = new Config('1.0');

        $user_id = $ctx->Profile->user_id;

        $qry = "select `project`.`project_id`,count(i.iteration_id) as iterations,`name`,project.`created_date` from project 
                left join iteration i on i.project_id = project.project_id
                left join project_user on project_user.project_id = project.project_id
                where project_user.user_id = $user_id
        ";

        $project_id = 0;
        if (isset($ctx->Route->id[0])) {
            $project_id = $ctx->Route->id[0];
            if ($project_id>0){
                $qry .= " and project.project_id = $project_id";
            }
        }

        $qry .= " group by project.project_id order by project.`created_date` desc"; //$context->Route->id[0]

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    private static function getSuperAdmin($ctx)
    {
        $config = new Config('1.0');

        $qry = "select `project`.`project_id`,count(i.iteration_id) as iterations,`name`,project.`created_date` from project 
                left join iteration i on i.project_id = project.project_id ";

        $project_id = 0;
        if (isset($ctx->Route->id[0])) {
            $project_id = $ctx->Route->id[0];

            $qry .= " where project.project_id = $project_id";
        }

        $qry .= " group by project.project_id order by project.`created_date` desc"; //$context->Route->id[0]

        $db = new DataService($config->dbConnection);

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function save($ctx)
    {
        $project_id = 0;
        if (isset($ctx->Route->id[0])) {
            $project_id = $ctx->Route->id[0];
        }
        if ($project_id > 0) {
            return Project::update($ctx);
        }
        return Project::create($ctx);
    }

    private static function create($ctx)
    {
        $config = new Config('1.0');

        $db = new DataService($config->dbConnection);

        $name = mysqli_real_escape_string($db->conn, $ctx->FormData->name);
        $qry = "INSERT INTO project (`name`) VALUES ('%s');";
        $qry = sprintf($qry, $name);



        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    private static function update($ctx)
    {
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);

        $name = mysqli_real_escape_string($db->conn, $ctx->FormData->name);
        $project_id = $ctx->Route->id[0];

        $qry = "UPDATE project SET  `name` = '$name' WHERE project_id = $project_id;";


        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

    static function delete($ctx)
    {
        $config = new Config('1.0');

        $db = new DataService($config->dbConnection);

        $project_id = $ctx->Route->id[0];

        $qry = "DELETE FROM project where project_id = $project_id;";

        $data = $db->ExecStatement($qry);
        return new Result($data);
    }

}
