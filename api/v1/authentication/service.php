<?php

useClass('User, Message,UserRole, Helpers/FileHelper', $ctx);

function post_authentication_signin($ctx)
{
    // $ctx->FormData->remember_me = '1';
    // $rememberme = isset($ctx->FormData->remember_me) ? $ctx->FormData->remember_me : '0';

    useclass("Email", $ctx);
     $rememberme = boolval($ctx->FormData->rememberme);
     $user = new User();
     $ret = $user->SignIn($ctx->FormData->email, $ctx->FormData->password,  $rememberme);
    return $ret;
}

function post_authentication_cities($ctx)
{

    $cities = new Booking();
    $ret = $cities->Get();
    return $ret;
}

function post_authentication_subscribe($ctx)
{
 
      $user = new User();
      $data = $user->SubscribeToPush($ctx);

    // try {
    //     error_reporting(E_ALL);
    //     $config = new Config('1');
    //     $qry = "insert into `websubscription` (`user_id`, `devicejson`) values ('%d', '%s')";
    //     $qry = sprintf($qry, $ctx->FormData->user_id, json_encode($ctx->FormData->devicejson));
    //     //return new Result($qry);
    //     $db = new DataService($config->dbConnection);
    //     // return new Result($config->dbConnection);
    //     $data = $db->ExecStatement($qry);
    //     if ($ctx->FormData->subscribed == false) {
    //         $url = "/send";
    //         $api = new ExternalAPIHandler("http://ec2-54-154-196-239.eu-west-1.compute.amazonaws.com:8000");
    //         $sub = json_encode($ctx->FormData->devicejson);
    //         $data = array(
    //             "subscription" => json_decode($sub, true),
    //             "data" => array(
    //                 "title" => "Welcome to Lucid Ocean",
    //                 "body" => "You have been successfully subscribed.",
    //                 "icon" => "img/fav.png"
    //             )
    //         );
    //         $data['data'] = json_encode($data['data']);
    //         // $d = json_encode($data);
    //         // $d = json_decode($d);
    //         $data =  $api->Post($url,  array('Content-Type:application/json'), $data);
    //     }
    // } catch (Exception $e) {
    //     return new Result($e->getMessage());
    // }
    return new Result($data);
}


function post_authentication_reset($ctx)
{

    require_once './components/Helpers/EmailSender.php';

    for ($s = '', $i = 0, $z = strlen($a = 'abcdefghijklmnopqrstuvwxyz0123456789') - 1; $i != 32; $x = rand(0, $z), $s .= $a{
        $x}, $i++);

    $config = new Config('1.0');
    $qry = "select * from `user` where `email` like '%s' and `locked` = 0";
    $qry = sprintf($qry, $ctx->FormData->email);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);

    if (sizeof($data) > 0) {
        $data = $data[0];
        $url = $config->siteurl;
        if (isset($data)) {
            $to = $data['email'];
            // $subject = 'Reset Request';
            $token = $s;

            $obj = new stdClass();
            $obj->user_name = $to;
            $obj->date = date("d-m-Y H:i:s");
            $obj->cta_text = "Reset Password";
            $obj->cta_link = $url . "account/resetpassword?code=" . $s;
            useclass("Email", $ctx);
            $em = new Email();
            $em->SendEmail($to, 2, $obj);

            $id = $data['user_id'];
            $qry = "update `user` set `confirmation_token`= '%s' where `user_id` = %d";
            $qry = sprintf($qry,  $s, $id);
            $db = new DataService($config->dbConnection);
            $db->ExecStatement($qry);
        }
    }
    return new Result($data);
}

function post_authentication_unsubscribe($ctx)
{
    $user = new User();
    $data = $user->Unsubscribe($ctx);
    return new Result($data);
}

function post_authentication_newpassword($ctx)
{

    require_once './components/Helpers/EmailSender.php';
    $config = new Config('1');
    // get field
    $qry = "select * from `user`  Where `confirmation_token` like '" . $ctx->FormData->confirmation_token . "'";
    $qry = sprintf($qry,  $ctx->FormData->confirmation_token);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);

    // update password
    $id = $data[0]['user_id'];
    $Email = $data[0]['email'];
    $pass = md5($ctx->FormData->password2);
    $url = $config->siteurl;

    $qry = "update `user` set `password`  = '%s', `confirmation_token` = null  Where `user_id` = '" . $id . "'";
    $qry = sprintf($qry, $pass);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);
    // send email
    $to = $Email;

    $obj = new stdClass();
    $obj->user_name = $to;
    $obj->date = date("d-m-Y H:i:s");
    $obj->cta_text = "Login";
    $obj->cta_link = $url . "account/signin";
    useclass("Email", $ctx);
    $em = new Email();
    $em->SendEmail($to, 6, $obj);

    return new Result($data);
}

function post_authentication_signup($data)
{
    require_once './components/Helpers/EmailSender.php';
    useClass('Account', $data);

    $account = new Account();
    $result = $account->SignUp($data);
    return $result;
}

function post_authentication_signuplimited($data)
{
    require_once './components/Helpers/EmailSender.php';
    useClass('Account', $data);

    $account = new Account();
    $result = $account->SignUpLimited($data);
    return $result;
}

function post_authentication_verify($data)
{

    $config = new Config('1.0');
    $validated = 1;
    $qry = "update `user` set `validated` = %d ,`confirmation_token` = null  Where `confirmation_token` ='%s'";
    $qry = sprintf($qry, $validated, $data->FormData->access_token);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);

    return new Result($data);
}

function post_authentication_verifytoken($ctx)
{

    $user = new User();
    $ret = $user->AuthenticateToken($ctx->FormData->access_token);
    return $ret;
}


function post_authentication_signout($ctx)
{
    $user = new User();
    $ret = $user->Signout($ctx->FormData);
    return $ret;
}

function put_authentication_updateUser($ctx)
{

    $user = new User();
    $ret = $user->Update($ctx->FormData);
    return $ret;
}


function put_authentication_accountUser($ctx)
{
    $user = new User();
    $ret = $user->updateUser($ctx);
    return $ret;
}

function get_authentication_avatar($ctx)
{
    $user = new User();
    $ret = $user->GetProfileImage($ctx->Route->id[0]);
    return $ret;
}


function put_authentication_change($ctx)
{

    require_once './components/Helpers/EmailSender.php';
    $mailing = $ctx->FormData->user;
    $pass = md5($ctx->FormData->password);
    $config = new Config('1.0');
    $qry = "update  `user` set  `password` = '%s' where `user_id` = %d";
    $qry = sprintf($qry, $pass, $ctx->FormData->user_id);
    $db = new DataService($config->dbConnection);
    $result = $db->ExecStatement($qry);


    if ($result > 0) {
        $url = $config->siteurl;
        $obj = new stdClass();
        $obj->user_name = $ctx->FormData->user->first_name;
        $obj->date = date("d-m-Y H:i:s");
        $obj->cta_text = "Password is changed";
        $obj->cta_link = $url . "account/signin";
        useclass("Email", $ctx);
        $em = new Email();
        $email = $ctx->FormData->user->email;

        $em->SendEmail($email, 6, $obj);

        return new Result($result);




        return new Result($result);
    } else {
        $error = new ErrorResult('Could not register, an error has occurred.');
        return $error;
    }


    return new Result($data);
    // $user = new User();
    // $ret = $user->ChagePassword($ctx);
    // return $ret;
}

function put_authentication_changeandauthenticate($ctx)
{
    require_once './components/Helpers/EmailSender.php';

    $user_id = $ctx->Profile->user_id;
    $pass = md5($ctx->FormData->password);
    $config = new Config('1.0');

    $qry = "update  `user` set  `password` = '%s', `confirmation_token` = null where `user_id` = %d";
    $qry = sprintf($qry, $pass, $user_id);

    $db = new DataService($config->dbConnection);
    $db->ExecStatement($qry);


    $url = $config->siteurl;
    $obj = new stdClass();
    $obj->user_name = $ctx->Profile->first_name;
    $obj->date = date("d-m-Y H:i:s");
    $obj->cta_text = "Password is changed";
    $obj->cta_link = $url . "account/signin";

    useclass("Email", $ctx);
    $em = new Email();
    $email = $ctx->Profile->email;

    $em->SendEmail($email, 6, $obj);

    $data = [
        'message' => 'password changed'
    ];

    return new Result($data);
}


function get_authentication_settings($context)
{

    $user_id = $context->Filter->Options->user_id;
    $config = new Config('1.0');
    $qry = "select * from account_setting where `user_id` =  %d";
    $qry = sprintf($qry, $user_id);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);
    return new Result($data);
}


function put_authentication_settings($ctx)
{
    // $user_id = $context->Filter->Options->user_id;
    $config = new Config('1.0');
    $qry = "update account_setting set `notifications` = %d,`articles` = %d, `newsletters`='%d' where `user_id` =  %d";
    $qry = sprintf($qry, $ctx->FormData->notifications, $ctx->FormData->articles, $ctx->FormData->newsletters, $ctx->FormData->user_id);
    $db = new DataService($config->dbConnection);
    $data = $db->ExecStatement($qry);

    return new Result($data);
}