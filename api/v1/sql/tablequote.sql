
CREATE TABLE `quote_status` (
  `Status_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Status` varchar(45) DEFAULT NULL,
  `Value` int(11) DEFAULT NULL,
  PRIMARY KEY (`Status_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `quote` (
  `Quote_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Booking_Id` int(11) DEFAULT NULL,
  `Quote_Ref` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Due_Date` date DEFAULT NULL,
  `Quote_To` varchar(45) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Total_Amount` decimal(12,2) DEFAULT '0.00',
  `Vat` decimal(4,2) DEFAULT '0.00',
  `Created_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `Deleted` bit(1) DEFAULT b'0',
  `Modified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Quote_Id`),
  UNIQUE KEY `Quote_Id_UNIQUE` (`Quote_Id`),
  KEY `Status` (`Status`),
  KEY `quote_ibfk_3` (`Booking_Id`),
  CONSTRAINT `quote_ibfk_2` FOREIGN KEY (`Status`) REFERENCES `quote_status` (`Status_Id`),
  CONSTRAINT `quote_ibfk_3` FOREIGN KEY (`Booking_Id`) REFERENCES `booking` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;


CREATE TABLE `quote_item` (
  `Item_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Quote_Id` int(11) DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Quantity` int(5) NOT NULL DEFAULT '0',
  `Price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`Item_Id`),
  UNIQUE KEY `Item_Id_UNIQUE` (`Item_Id`),
  KEY `Quote_Id` (`Quote_Id`),
  CONSTRAINT `item_quote_ibfk_3` FOREIGN KEY (`Quote_Id`) REFERENCES `quote` (`Quote_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `quote_share` (
  `share_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `sharedate` datetime DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  ALTER TABLE `booking` 
ADD COLUMN `quote_id` INT(11) NULL DEFAULT NULL AFTER `invoice_id`;


INSERT INTO `message_event_type`
(
`event`,
`value`)
VALUES
(
'Quote report shared',
15);


INSERT INTO `message_template` 
(
`event_type`,
`priority`,
`subject`,
`body`) VALUES (15,2,'RE:: Nova - An quote was shared with you: {quote_ref}','<p>A quote was recently shared with you </p> 
<p>To view the quote please follow the link below: </p> ');
