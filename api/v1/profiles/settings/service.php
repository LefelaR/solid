<?php


useClass('ProfileSettings,ProjectsActivity,Notification', $ctx);
function get_settings($ctx){
    return ProfileSettings::get($ctx);
}




function put_settings($ctx){

    return ProfileSettings::save($ctx);
}


function put_settings_subscribe($ctx){

    return ProfileSettings::subscribe($ctx);
}


function put_settings_unsubscribe($ctx){

    return ProfileSettings::unsubscribe($ctx);
}



