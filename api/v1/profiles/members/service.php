<?php
useClass('Members,Audit,Message,Profile', $ctx);

function get_members($ctx)
{
    return Members::get($ctx);
}

function put_members($ctx)
{
    return Members::update($ctx);
}

function put_members_grantaccess($ctx)
{

    return Members::grantaccess($ctx->Route->id[0], $ctx->Profile->user_id, $ctx->Profile->group_id);
}

function delete_members($ctx)
{
    return Members::delete($ctx->Route->id[0], $ctx->Profile->user_id, $ctx->Profile->group_id, $ctx );
}

function delete_members_leaveteam($ctx)
{
    return Members::leaveteam($ctx->Route->id[0], $ctx->Profile->user_id, $ctx->Profile->group_id);
}

function post_members_search($ctx)
{
    $search = isset($ctx->FormData->search) ? $ctx->FormData->search : '';

    return Members::search($ctx->Profile->group_id, $search);
}

function post_members($ctx)
{
    return Members::add($ctx->Route->id[0], $ctx->Profile->user_id, $ctx->Profile->group_id);
}

function put_members_changerole($ctx){

    $role = isset($ctx->FormData->role) ? $ctx->FormData->role : 0;

    return Members::changerole($ctx->Route->id[0], $ctx->Profile->user_id, $ctx->Profile->group_id, $role);
}
