<?php
useClass('Profile, Notification', $ctx);

function get_profiles($ctx)
{
        

$user_id = isset($ctx->Route->id[0]) ? $ctx->Route->id[0] : 0;
   
if(!$user_id)   
    {
        $user_id = $ctx->Profile->user_id;
    }  
    return Profile::get($user_id);
}

function put_profiles($ctx)
{
    return Profile::save($ctx);
}


function delete_profiles_image($ctx)
{
    return Profile::DeleteProfileImage($ctx);
}

function get_profiles_groups($ctx)
{
    return Profile::groups($ctx);
}

function get_profiles_groupdetail($ctx)
{
    return Profile::getgroup($ctx);
}

function post_profiles_groups($ctx)
{
    return Profile::setgroup($ctx);
}