<?php

const Header_Authorization = 'Authorization'; 
const Header_XEntityId = 'X-EntityID'; 

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);

    return $length === 0 || 
    (substr($haystack, -$length) === $needle);
}

function contains($haystack, $needle)
{
    return stripos($haystack, $needle) !== false;
    }

?>