<?php



require_once 'includes/strings.inc.php';
require_once('init.php');

enable_noExecute();

$Context;

class CRest
{
    private $Nouns = [];
    private $Aspects = [];
    private $RestHandlers = array('*' => [], 'noun' => []);
    private $DefaultRestHandler = null;
    private $Options;

    function __construct($options = null, $restHandler = null)
    {
        //we will make options a global on Context, but lets set it up here first.
        $this->Options = new stdClass();
        $this->Options->Debug = false;
        $this->Options->Version = 'v1';
        if (isset($options)) {
            $this->Options = (object)array_merge((array)$this->Options, (array)$options);
        }

        if ($restHandler == null){
            $restHandler = new RestHandler();
        }
        $this->DefaultRestHandler = $restHandler;


    }

    function Init()
    {
        $this->enable_CorsOptions();

        $this->CreateRestRoutingContext();
        $this->enable_Filter();
        $this->ParseUrl($_SERVER['REQUEST_URI']);
        $this->enable_post();
    }

    function ParseURL($url)
    {
        //var_dump(parse_url($url, PHP_URL_PATH));

    }


    function enable_CorsOptions()
    {
        $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'];
        $str = join(",", $allowedMethods);
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && in_array($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'], $allowedMethods)) {
                header('Access-Control-Allow-Origin:' . $_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Allow-Credentials:true');
                header('Access-Control-Allow-Methods: ' . $str);
                header('Access-Control-Allow-Headers: Authorization, Content-Type, X-EntityId, Cache-Control, X-Requested-With, withCredentials');
                header('Access-Control-Max-Age: 3600');
            }
            exit;
        }

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header('Access-Control-Allow-Origin:' . $_SERVER['HTTP_ORIGIN']);
        }
        header('Access-Control-Allow-Credentials:true');
    }



    function CreateRestRoutingContext()
    {
        global $Context;
        $Context = new Context();
        $Context->Options = $this->Options;

        $noun = isset($_REQUEST['noun']) ? $_REQUEST['noun'] : '';
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
        $ver = isset($_REQUEST['ver']) ? $_REQUEST['ver'] : '';

        if ($noun == 'restproxy.php' && $noun == '/') {
            exit;
        }

        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

        if (endsWith($noun, '/')) {
            $noun = str_replace('/', '', $noun);
        }

        if (endsWith($id, '/') || (startsWith($id, '/'))) {
            $id = str_replace('/', '', $id);
        }

        if (endsWith($action, '/') || (startsWith($action, '/'))) {
            $action = trim($action, '/');
            $lastidx = strrpos($action, '/');
            if ($lastidx > 0) {
                $action = substr($action, $lastidx + 1);
            }
        }

        $pos = strrpos($action, "/");
        $action = substr($action, $pos);

        if (endsWith($action, '/') || (startsWith($action, '/'))) {
            $action = str_replace('/', '', $action);
        }

        if (is_numeric($action)) {
            $action = '';
        }

        if ($action == '0') {
            $action = '';
        }

        $idarray = array();
        if ($id != '') {
            $splitid = explode('/', $id);

            foreach ($splitid as $index) {
                if ($index == '') {
                    continue;
                }

                $id = intval($index);

                if ($id > 0) {
                    array_push($idarray, $id);
                }
            }
        }

        $split = explode('/', $noun);


        if (!empty($action)) {
            if (in_array($noun . '/' . $action, $this->Nouns)) {

                $noun = $noun . '/' . $action;
                $split[] = $action;
                $action = '';

            } else if (!in_array($noun, $this->Nouns)) {

                if (sizeof($split) == 2 && sizeof($idarray) == 1) {
                    $action = $split[1];
                    $noun = $split[0];
                    $split = [
                        $split[0]
                    ];

                }
            } else {

                $lastidx = strrpos($noun, $action);
                if ($lastidx > 0) {
                    $action = '';
                }

            }
        } else {

        }


        if ($ver != '') {
            $Context->Route->ver = $ver . '/';
            $Context->Route->version = str_replace('v', '', $ver);
        } else {
            $Context->Route->ver = $this->Options->Version . '/';
            $Context->Route->version = str_replace('v', '', $Context->Route->ver);
        }

        $Context->Route->path = $noun;
        $Context->Route->noun = $split;
        $Context->Route->id = $idarray;
        $Context->Route->action = $action;

        $Context->Route->method = strtolower(isset($_REQUEST['method']) ? $_REQUEST['method'] : $_SERVER['REQUEST_METHOD']);

    }

    function enable_post()
    {
        $formData = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'PUT') {
            if (!empty($_POST)) {
                $formData = (object)$_POST;
            } else if (!empty(file_get_contents("php://input"))) {
                $formData = $this->DecodeJson(file_get_contents("php://input"), false);
            }
            
            global $Context;
            $Context->FormData = $formData;
        }

    }

    function enable_BearerAuthentication($exceptions = [])
    {
        global $Context;

        $containsSearch = count(array_intersect($Context->Route->noun, $exceptions)) == count($Context->Route->noun);

        if ($containsSearch === true) return;

        $headers = getallheaders();
        if (!isset($headers[Header_Authorization])) {
            $ret = new ErrorResult('There is no ' . Header_Authorization . ' present. Bearer Token cannot be found.', 403);
            $ret->flushResult();
        }

        $Context->Token = $headers[Header_Authorization];
        return $Context->Token;

    }

    function enable_Filter()
    {
        $filter = array();
        parse_str($_SERVER['QUERY_STRING'], $filter);
        $page = 1;
        if (isset($filter['page'])) {
            $page = $filter['page'];
            unset($filter['page']);
        }

        unset($filter['noun']); // included from HTAccess QS
        global $Context;
        $Context->Filter = new Filter($page, $filter);
    }


    function DecodeJson($data)
    {
        $json = rtrim($data, "\0");
        $json = html_entity_decode($json);
        $json = json_decode($json);
        return $json;
    }


    function execute_RestHandler()
    {

        global $Context;

        if (!isset($Context))
            $this->logError('Context is null, did you call Crest->Init() before ExecuteHandler() ?');

       
        $this->AssertHandler($Context);
        $this->loadServiceFile($Context);
        $handlerFunction = $this->getHandlerFunction($Context);
       

        if ($Context->Options->Debug == true) {
            // if (!empty($Context->Route->action)) {
            //     // if (file_exists($dir . '/' . $Context->Route->action)) {
            //     //     $result = new ErrorResult('Numbnuts: ' . $Context->Route->action . ' is a folder and an action on ' . $Context->Route->LastNoun());
            //     //     $result->FlushResult(); // get out now.
            //     // }
            // }
        }

        
        $result = $this->callHandlerFunction($handlerFunction, $Context);
        

        if (!isset($result)) {
            $result = new ErrorResult('Handler for ' . json_encode($Context->Route) . ' requires you to return a Result or ErrorResult object', 400);
        }

        $result->FlushResult();

    }

    function loadServiceFile($Context)
    {
       $handled = false;
       $handlers = $this->getListOfHandlers($Context);
       

        foreach ($handlers as $handler) {
            if ($handler->handles($Context)){
                $dir = $handler->getDirectory($Context);
                $handler->loadServiceFile($dir,$Context);
                $Context->Route->handler = $handler;
                $handled=true;
                break;
            }
        }

        if ($handled===false){
            if ($this->DefaultRestHandler->handles($Context)){
                $dir = $this->DefaultRestHandler->getDirectory($Context);
                $this->DefaultRestHandler->loadServiceFile($dir,$Context);
            }
        }
       
     
    }

    function getHandlerFunction($Context)
    {
        $handled = false;
        $handlerFunction ='';

        $handlers = $this->getListOfHandlers($Context);
       

        foreach ($handlers as $handler) {
            if ($handler->handles($Context)){
                $handlerFunction = $handler->getHandlerFunction($Context);
                $handled =true;
            }
        }
        if ($handled===false){
            if ($this->DefaultRestHandler->handles($Context)){
                $handlerFunction = $this->DefaultRestHandler->getHandlerFunction($Context);
            }
            else{
                $result = new ErrorResult('You have not configured a RestHandler that can execute the Route.', 400);
            }
        }
        
        $Context->Route->handlerFunction = $handlerFunction;

        return $handlerFunction;
    }
    
    function callHandlerFunction($handlerFunction, $Context){

        $result=null;
        $handled = false;
        if (function_exists($handlerFunction) || empty($handlerFunction)) { // if empty then the resthandler should handle it.
            $callHandlerResult = $this->beginRequest($Context);
            if ($callHandlerResult == null) {
                $handlers = $this->getListOfHandlers($Context);
                foreach ($handlers as $handler) {
                    if ($handler->handles($Context)){
                        $result = $handler->execute($Context);
                        $handled=true;
                        break;
                    }
                }
                if($handled===false){
                    if ($this->DefaultRestHandler->handles($Context)){
                        $result = $this->DefaultRestHandler->execute($Context);
                    }
                    else{
                        $result = new ErrorResult('You have not configured a RestHandler that can execute the Route.', 400);
                    }
                    //$result = $handlerFunction($Context);
                }
            } else {
                $result = $callHandlerResult;
            }
            $result = $this->endRequest($Context, $result);
        } else {
            $result = new ErrorResult('Handler for ' . $handlerFunction . ' not found', 400);
        }

        return $result;
    }

    function getListOfHandlers($Context){
        $route = $Context->Route->lastNoun();
        $routeHandler = [];

        if (key_exists($route, $this->RestHandlers))
        {
            $routeHandler = $this->RestHandlers[$route];
        }

        $handlers = array_merge($this->RestHandlers['*'], $routeHandler);

        return $handlers;
    }

   

    function beginRequest($ctx)
    {
        $result = null;
        foreach ($this->Aspects as $aspect) {
            $ret = $aspect->begin_request($ctx);
            if (isset($ret)) {
                $result = $ret;
            }
        }

        return $result;
    }
    function endRequest($ctx, $result)
    {
        foreach ($this->Aspects as $aspect) {
            $ret = $aspect->end_request($ctx, $result);
            if (isset($ret)) {
                $result = $ret;
            }
        }

        return $result;
    }



    function logError($msg)
    {
        throw new Exception($msg);
        // if ($this->Options->Debug == true) {

        // } else {
        //     new Result('Server Error: Admin may set Debug = True to view errors');
        // }
    }

    function AssertHandler($ctx)
    {
        $throwError = false;
        if (sizeof($ctx->Route->noun) == 0) {
            $throwError = true;
        }
        if (sizeof($ctx->Route->noun) == 1) {
            if (empty($ctx->Route->noun[0])) {
                $throwError = true;
            }
        }
        if ($throwError) {
            $c = getcwd();
            if (file_exists('api/' . $ctx->Route->ver . '/index.php')) {
                require_once 'api/' . $ctx->Route->ver . '/index.php';
                die();
            } else {
                echo 'There is no API located here.';
                die();
            }
        }
    }

    
   
    /*
        $nounsPath = "noun/noun" - to override action
     */

    function addNouns($nounsPath)
    {
        array_push($this->Nouns, $nounsPath);
    }

    function registerAspect($aspectClass)
    {
        $iAspect = new $aspectClass();
        if ($iAspect instanceof IAspect) {
            array_push($this->Aspects, $iAspect);
        } else {
            throw new Exception('Not an Aspect. Please ensure you pass an object that implements iAspect');
        }
    }
    
    /// $crest->RegisterRestHandler(TestRestHandler::class, ['events', 'bells']);
    function registerRestHandler($restHandlerClass, $route = ['*'])
    {
        $restHandler = new $restHandlerClass();
        if (sizeof($route) == 0){
            $route = '*';
        }
           
        if ($restHandler instanceof IRestHandler) {
            foreach ($route as $key ) {
                $this->RestHandlers[$key] = array();
                array_push($this->RestHandlers[$key], $restHandler);
            }
            
        } else {
            throw new Exception('Not a REST Handler. Please ensure you pass an object that implements iRestHandler');
        }
    }


}
?>