# Readme


## 1. Installing crest to maintain, fix and edit the crest library.
You must pull crest into its own folder. Example : d:\xampp\htdocs\crest

- check out http://Beastie:81/svn/crest into crest folder
- 
## 2. Installing crest for your API

Create your project folder and pull crst/crest folder into your project. The repo is named crest, but you require the sub folder also named crest to be your subfolder. 

- check out http://Beastie:81/svn/crest/crest into yourproject/crest
- create an .htaccess on your root and add the following to it


```
    RewriteEngine On
    

    
RewriteRule ^api/(v1|v2)*/*$ restproxy.php?noun=&ver=$1 [L,QSA]

RewriteRule ^api/(v1|v2)*/*([a-zA-Z]+)/*$ restproxy.php?noun=$2 [L,QSA]
RewriteRule ^api/(v1|v2)*/*([a-zA-Z]+)/([a-zA-Z]+)/(([0-9]+)|([a-zA-Z]+))/*$ restproxy.php?noun=$2&action=$3 [L,QSA]

RewriteRule ^api/(v1|v2)*/*([a-zA-Z]+)*/*(([0-9]+)|([a-zA-Z]+))/*([a-zA-Z]+)*/*$ restproxy.php?noun=$2/$6&id=$4&action=$5/$6  [L,QSA]
RewriteRule ^api/(v1|v2)*/*([a-zA-Z]+)*/*(([0-9]+)|([a-zA-Z]+))/*([a-zA-Z]+)*/*(([0-9]+|[a-zA-Z]+))*/*([a-zA-Z]+)*/*$ restproxy.php?noun=$2/$6&id=$4/$8&action=$5/$6/$8/$9  [L,QSA]
    

```

- create index.php in your app folder root and place the following code

```
<?php
    require_once 'crest/Crest.php';
    $crest = new CRest();
    //$crest->enable_BearerAuthentication();
    $crest->execute_RestHandler();
?>
```

## Creating your first API call in crest

On your project root, create a folder structure for your API.

api/v1/{noun}/service.php

The {noun} is your API noun (must be plural to abide by REST rules).
Example: Companies, Applications, jobs, tickets

The client API URL: {localhost|domain}:/api/{noun}
this url will map to your service.php file you created in the folder
api/v1/{noun}/service.php

### The code in service.php ###

You need to create function handlers for each API call. The function name has the following format:

{httpverb}_{noun}_{action}($context)

example:

```
function get_companies($context){

}

function get_jobs($context){
    
}

function post_jobs_accept($context){
    
}

```

### Context

The function handler will provide you with $context.
The URL will be provided in the Route. QueryString values is placed in the Filter, with Post data, whether its x-www-form-urlencoded or raw JSON, will appear in FormData.

```
{
    "Route": {
        "ver": "v1/",
        "version": "1",
        "noun": [
            "services"
        ],
        "id": [],
        "action": "",
        "method": "GET",
        "path": "services"
    },
    "Filter": {
        "PageLimit": 0,
        "PageOffset": 10,
        "Page": 1,
        "Options": {}
    },
    "FormData" : {
        "email" : "contact@lucidocean.com",
        "password" : "LucidOcean"
    }
}

```