<?php
//encrypt response, and catch errors as exceptions
ob_start("ob_gzhandler");
error_reporting(E_ALL);

function lastresort_handler($e)
{
    global $Context;
    $ret;
    if (!isset($Context)) {
        $ret = new ErrorResult("Line: " . $e->getLine() . ", error: " . $e->getMessage(), 500);
        $ret->FlushResult();
    }
    if ($Context->Options->Debug == true) {
        $ret = new ErrorResult($Context->Route->LastNoun() . ", handler: " . $Context->Route->handlerFunction . ", Line: " . $e->getLine() . ", error: " . $e->getMessage(), 500);
    } else {
        $ret = new ErrorResult('Server Error: Admin may set Debug = True to view errors', 500);
    }

    $ret->FlushResult();

}//Handle unhandled exceptions and return JSON error
set_exception_handler('lastresort_handler');


function enable_noExecute()
{
    $block = true;

    if (isset($_REQUEST['noun'])) {
        if ($_REQUEST['noun'] !== '') {
            $block = false;
        }
    }

    if ($block !== false) {
        header('HTTP/1.0 403 Forbidden');
        exit('Forbidden');
    }
}


/**
 * A simplistic loader for system classes. It also checks the same folder.
 * If you want to load your own dependency class in your code,
 * use useClass
 * @example useClass('WorkOrders, Queue',$context);
 */

function autoload($class_name)
{
    if (file_exists('crest/components/' . $class_name . '.php')) {
        include 'crest/components/' . $class_name . '.php';
        return;
    }
    if (file_exists('crest/components/aspects/' . $class_name . '.php')) {
        include 'crest/components/aspects/' . $class_name . '.php';
        return;
    }
    if (file_exists('crest/components/resthandlers/' . $class_name . '.php')) {
        include 'crest/components/resthandlers/' . $class_name . '.php';
        return;
    }
}
spl_autoload_register('autoload');

require_once 'includes/strings.inc.php';

function useClass($classname, $context)
{

    $classes = explode(',', $classname);
    foreach ($classes as $item) {
        $class = $item;
        if (contains($item, '/')) {
            $items = explode('/', $item);
            $class = $items[1];
        }

        if (class_exists($class) == false) {
            require 'api/' . $context->Route->ver . 'components/' . trim($item) . '.php';
        }
    }

}

