#Version 1.3

Potentially Breaking Change::

JSON is now returning bool and int without string "".

        //previous
        {
            "category_id": "1",
            "online": "true"
        }
        //now
        {
            "category_id": "1",
            "online": true
        }

New route added

api/folder/noun/id



# Version 1.2


- when you navigate to the api/ api/v1, you can now place an index.php file to handle the 'no api in this location'. You can redirect, show documentation or other.
- Added Options for Version
  
```
$options = [
    'Debug' => true,
    'Version' => 'v2'
];

```
- Breaking: Changed handlers. you must now return the Result. Typically your code was 

```
    new Result(data);
```

change to:

```
    return new Result(data);
```
OR
```
    new Result(data)->FlushResult();
```

- Added Aspects including two built-in protector (5 second cache), Performance, writes out execution time

```
//create your aspect.

class Logger implements IAspect
{

    public function __construct()
    {

    }

    public function begin_request($ctx)
    {
        //return new Result() will stop the executionhandler from firing and move straight to end_request
    }

    public function end_request($ctx, $result)
    {
        $result->message .= "Log this sucker";
        return $result;
    }


}

$options = [
    'Debug' => true
];

$crest = new CRest($options);

//register your aspects
$crest->RegisterAspect(Logger::class);


$crest->Init();
$crest->execute_RestHandler();

```

# Version 1.1

- If you don't call $crest->Init() before executeHandler() it now detects and throws system error
- CREST gets a Debug flag. Errors are protected from the client, unless debug = true
- If an expcetion is thrown, the ErrorResult return to client (debug=true), will return, NOUN / handler function, line no. and error message.

```
{
    "Status": {
        "Message": "services, handler: get_services, Line: 6, error: syntax error, unexpected 'var' (T_VAR)",
        "Code": 500
    }
}
```

- New snippets file. Copy into your .vscode project directory. use by typing API and selecting one of the two snippets.

```

$options = [
    'Debug' => true
];

$crest = new CRest($options);

```


## Version 1

Initial release