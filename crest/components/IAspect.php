<?php

enable_noExecute();

interface IAspect
{
    public function begin_request($ctx);
    public function end_request($ctx, $result);
}