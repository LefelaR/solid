<?php

class PostRestHandler implements IRestHandler
{

    public function handles($ctx)
    {

        $verb = $ctx->Route->method;
        
        if ($verb == 'post'){
            return true;
        }

        return false;
    }

    public function getHandlerFunction($ctx)
    {
        $handlerFunction='';
        $verb = $ctx->Route->method;

        if ($ctx->Route->action != '') {
            $handlerFunction = $verb . '_' . $ctx->Route->noun[sizeof($ctx->Route->noun) - 1] . '_' . $ctx->Route->action;
        } else {
            $handlerFunction = $verb . '_' . $ctx->Route->noun[sizeof($ctx->Route->noun) - 1];
        }
        

        return $handlerFunction;
    }

    public function getDirectory($ctx)
    {
        $dir = 'api/' . $ctx->Route->ver . $ctx->Route->path;
        return $dir;
    }

    public function loadServiceFile($dir,$ctx)
    {
        require_once $dir . '/post_service.php';
        
    }

    public function execute($ctx){
        $h =  $ctx->Route->handlerFunction;
        return $h($ctx);
    }


}   