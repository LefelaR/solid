<?php

class QueueRestHandler implements IRestHandler
{

    public function handles($ctx)
    {
        $verb = $ctx->Route->method;

        if ($verb == 'post' || $verb == 'put' || $verb == 'delete')
            return true;

        return false;
    }

    public function getHandlerFunction($ctx)
    {
        return '';
    }

    public function getDirectory($ctx)
    {
        //$dir = 'api/' . $ctx->Route->ver . $ctx->Route->path;
        //return $dir;
        return '';
    }

    public function loadServiceFile($dir, $ctx)
    {

        //require_once $dir . '/service.php';
    }

    public function execute($ctx)
    {

        $crestQueue = new CrestQueue($ctx);
        $crestQueue->Save();

        return new Result('Success');

    }


}