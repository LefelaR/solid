<?php

class TestRestHandler implements IRestHandler
{

    private $test = '_test';

    function __construct()
    {

    }

    public function handles($ctx)
    {
        $headers = getallheaders();

        if (isset($headers['Test'])) {
            if ($headers['Test'] == 'true') {
                $this->test = '_test';
                return true;
            }
        }

        return false;
    }

    public function getHandlerFunction($ctx)
    {
        $handlerFunction = '';
        $verb = $ctx->Route->method;

        if ($ctx->Route->action != '') {
            $handlerFunction = $verb . '_' . $ctx->Route->noun[sizeof($ctx->Route->noun) - 1] . '_' . $ctx->Route->action . $this->test;
        } else {
            $handlerFunction = $verb . '_' . $ctx->Route->noun[sizeof($ctx->Route->noun) - 1] . $this->test;
        }

        return $handlerFunction;
    }

    public function getDirectory($ctx)
    {
        $dir = 'api/' . $ctx->Route->ver . $ctx->Route->path;
        return $dir;
    }

    public function loadServiceFile($dir, $ctx)
    {
        require_once $dir . '/service' . $this->test . '.php';

    }

    public function execute($ctx)
    {
        $h = $ctx->Route->handlerFunction;
        return $h($ctx);
    }


}