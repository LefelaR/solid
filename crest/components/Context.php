<?php

class Context {

    public $Route;
    public $Filter;
    public $EntityId;
    public $Token;

    public function __construct(){
       $this->Route = new Route();
       $this->Filter = new Filter();
    }
}

?>