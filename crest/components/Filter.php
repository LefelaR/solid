<?php

enable_noExecute();

class Filter
{
    public $PageLimit = 0;
    public $PageOffset = 10;
    public $Page = 1;
    public $Options;

    public function __construct($page = 1, $options = null)
    {
        $this->Page = $page;
        $this->Options = (object)$options;
    }
}
