<?php

class Tests implements IAspect
{

    public function begin_request($ctx)
    {

    }

    public function end_request($ctx, $result)
    {

        if ($ctx->Route->handler instanceof TestRestHandler){
            sleep(2);
            $result->custom->testresult = 'true';
            $result->custom->queueresult = 'true';
        
        }
        return $result;
    }


}   