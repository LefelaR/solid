<?php

class LogAll implements IAspect
{

    public function begin_request($ctx)
    {

    }

    public function end_request($ctx, $result)
    {

        Logger::Log(json_encode($ctx));
        Logger::Log(json_encode($result));

        return $result;
    }


}   