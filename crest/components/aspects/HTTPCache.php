<?php

class HTTPCache implements IAspect
{

    public $start;

    public function begin_request($ctx)
    {
        new Cache(2);
    }

    public function end_request($ctx, $result)
    {

        $etag = md5(serialize($result));
        header("ETag: W/ \"{$etag}\"");

        return $result;
    }


}