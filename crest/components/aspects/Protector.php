    <?php

/*
 Protector doesnt use cache-control and IF-Modified, because clients do not have to obey.
 So in order to do a hard protect, lets do session cache.
     */
    class Protector implements IAspect
    {
        private $id;
        private $start;
        private $end;
        private $cacheSeconds = 10;
        public function begin_request($ctx)
        {
            $data = null;

            session_start([
                'cookie_lifetime' => 86400,
            ]);

            $this->end = microtime(true);
            $key = $this->getKey($ctx);
            $this->start = isset($_SESSION[$key . 't']) ? $_SESSION[$key . 't'] : 0;

            if (($this->end - $this->start) < $this->cacheSeconds) {
                if (isset($_SESSION[$key])) {
                    $result = $_SESSION[$key];
                    $result->custom->session = session_id();
                    $result->custom->left = $this->cacheSeconds - ($this->end - $this->start);
                    return $result;
                }
            }
        }

        public function end_request($ctx, $result)
        {

            $key = $this->getKey($ctx);
            $_SESSION[$key] = $result;
            if (($this->end - $this->start) >= $this->cacheSeconds) {
                $_SESSION[$key . 't'] = microtime(true);
            }

        }

        function getKey($ctx)
        {
            $key = implode('_', $ctx->Route->noun);
            $key .= implode('_', $ctx->Route->id);
            $hash = md5(serialize($ctx->Filter));
            return $key . '_' . $hash;
        }
    }


    ?>