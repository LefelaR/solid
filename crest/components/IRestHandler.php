<?php

enable_noExecute();

interface IRestHandler
{
    public function handles($ctx);
    public function getDirectory($ctx);
    public function getHandlerFunction($ctx);
    public function loadServiceFile($dir, $ctx);
    public function execute($ctx);

    
}