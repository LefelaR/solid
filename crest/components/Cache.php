<?php

class Cache
{


    public function __construct($seconds)
    {

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            $if_modified_since = preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']);
        } else {
            $if_modified_since = 0;
        }


        $mtime = strtotime('+ ' . $seconds . ' seconds', time());
        $gmdate_mod = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';

        $datecheck = gmdate('D, d M Y H:i:s', time()) . ' GMT';

        header('Cache-Control: public, max-age=' . $seconds);
        header('Date: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
        header('Expires: ' . $datecheck);
        header('Pragma: cache ');

        if ($if_modified_since > $datecheck) {
            header("HTTP/1.1 304 Not Modified");
            header("Last-Modified: $if_modified_since");
            exit;
        }


        header("Last-Modified: $gmdate_mod");
    }



}

?>