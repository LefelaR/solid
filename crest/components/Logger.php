<?php

enable_noExecute();

class Logger
{

    public static function Log($msg)
    {
        try {

            $logFile = "logs/log.txt";

            if (!file_exists('logs')) {
                mkdir('logs', 0755, true);
            }

            $msg = "\r\n" . date("Y-m-d H:i:s") . "\t:\t" . $msg;
            //error_log($msg, 3, $logFile);

            $fp = fopen($logFile, 'a+');
            stream_set_blocking($fp, 0);


            if (flock($fp, LOCK_EX)) {
                fwrite($fp, $msg);
            }
            flock($fp, LOCK_UN);

            fclose($fp);
            
           // $fileSize = filesize($logFile);
            
            // if ($fileSize > 200000){
            //     $f = @fopen($logFile, "r+");
            //     if ($f !== false) {
            //         ftruncate($f, 0);
            //         fclose($f);
            //     }
            //     echo $fileSize . "\n";
            // }


        } catch (Exception $e) {
           // $x = $e->getMessage();
        }

        return null;
    }


    public static function ClearLog()
    {
        $logFile = "logs/log.txt";
        $f = @fopen($logFile, "r+");
        if ($f !== false) {
            ftruncate($f, 0);
            fclose($f);
        }
    }
}

?>