<?php

enable_noExecute();

/**
 * Maintains the Route state
 */
class Route
{
    /**
     * the main default version if not specified in the url
     * @var string
     */
    public $ver = 'v1/';

    public $version = '1';
    /**
     * the main route consisting of nouns mapped to folder
     * @example 'workorders/invoices' from api/{workorders}/1/{invoices}
     * @var string
     */
    public $noun = '';
    /**
     * the main default version if not specified in the url
     * @var string
     */

    /**
     * defaults to 0, which is invalid value and will throw error.
     * Ensure the route has a valid int
     * @example  api/workorders/{1}/invoices
     * @var int
     */
    public $id = 0;

    /**
     * The main action of the Route
     * @example 'workorders/complete' from api/workorders/1/{complete}
     * @var string
     */
    public $action = '';

    /**
     * The HTTP method from which the call was made     
     * @var string GET,POST,PUT,DELETE
     */
    public $method = '';

    /**
     * The HTTP method from which the call was made     
     * @var string GET,POST,PUT,DELETE
     */
    public $handlerFunction = '';

    public $handler = '';


    public function LastNoun()
    {
        return $this->noun[sizeof($this->noun) - 1];
    }
}
?>