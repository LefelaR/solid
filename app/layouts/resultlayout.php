<!DOCTYPE html>
<html lang="en">
<?php include('layouts/includes/includesCSS.php'); ?>

<body class="">
    <main class="home">
        <?php include "layouts/includes/header.php"; ?>
   
        <section>
            @page
        </section>
        <?php include ('includes/footer.php'); ?> 
    </main>

    <div class="dont-panic">
        <img src="<?php url('assets/img/spinner.gif'); ?>" id="img" alt="" width="50px">
    </div>
    <!-- <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script> -->
   
</body>

</html>