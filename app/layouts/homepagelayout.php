<?php

include('layouts/includes/includesCSS.php'); ?>
 <body class="">
    <main class="home">
        <?php include "layouts/includes/header.php"; ?>
        <section>
            @page
        </section>
        <?php include ('includes/footer.php'); ?>
    </main>
    <div class="dont-panic">
        
    </div>
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
</body>
</html>
<?php
 function display_errors($errors){
     $display = '<li class="bg-danger">';
 foreach ($errors as $error ) {
     $display = '<li class="text-danger">'.$error.'</li>';
}
     $display .= '</li>';
     return $display;
 }
?>


