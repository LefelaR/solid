<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <title>Trade Routes | travel management Solutions</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=5, minimum-scale=1, width=device-width, viewport-fit=cover">
    <script src="<?php url('js/site.js') ?>"></script>
    <link rel="stylesheet" href="<?php url('css/style.css') ?>"> 
    <link rel="stylesheet" href="<?php url('css/custom.css') ?>">
    <link rel="shortcut icon" href="<?php url('assets/img/favicon.jpg')?>">
    <link rel='manifest' href='<?php url("./manifest.json")?>'>
    <link href="<?php url('font/css/open-iconic.css');?> " rel="stylesheet">
   
</head>