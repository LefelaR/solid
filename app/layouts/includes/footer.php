<footer class="mt-5">
    <div class="container pt-2">
        <div class="card-container col3">
        <div class="part">
                <h3>Locations</h3>
                <ul>
                    <li> <a href="<?php url('result?location=Botswana&rooms=1&adults=1&kids=0');?>">Botswana</a></li>
                    <li> <a href="<?php url('result?location=Congo&rooms=1&adults=1&kids=0');?>"> Congo (DRC) </a></li>
                    <li> <a href="<?php url('result?location=Ghana&rooms=1&adults=1&kids=0');?>"> Ghana </a></li>
                    <li> <a href="<?php url('result?location=Egypt&rooms=1&adults=1&kids=0&');?>"> Egypt </a></li>
                    <li> <a href="<?php url('result?location=Ethiopia&rooms=1&adults=1&kids=0');?>"> Ethiopia </a></li>
                    <li> <a href="<?php url('result?location=South Africa&rooms=1&adults=1&kids=0');?>"> South Africa </a></li>
                    <li> <a href="<?php url('result?location=Nigeria&rooms=1&adults=1&kids=0');?>"> Nigeria </a></li>
                </ul>
            </div>
         <div class="part">
            <h3>City</h3>
                <ul>
                    <li> <a href="<?php url('result?location=Cape Town&rooms=1&adults=1&kids=0');?>"> Cape Town </a></li>
                    <li><a href="<?php url('result?location=Johannesburg&rooms=1&adults=1&kids=0');?>"> Johannesburg </a></li>
                    <li><a href="<?php url('result?location=Accra&rooms=1&adults=1&kids=0');?>"> Accra </a></li>
                    <li><a href="<?php url('result?location=Maputo&rooms=1&adults=1&kids=0');?>"> Maputo </a></li>
                    <li><a href="<?php url('result?location=Lobatse&rooms=1&adults=1&kids=0');?>"> Lobatse </a></li>
                    <li><a href="<?php url('result?location=Harare&rooms=1&adults=1&kids=0');?>"> Harare </a></li>
                    <li><a href="<?php url('result?location=Kinshasa&rooms=1&adults=1&kids=0');?>"> Kinshasa </a></li>
                </ul>
            </div>

            <div class="part text-right">
                <h3>Contact Info</h3>
                <p>Telephone: +27 60 662 0257</p>
                <p class="py">Email: info@traderoutes.co.za</p>
                <span class="py bold">Physical Address</span>

                <address class="py">
                    Unit 2, Microtek Business Park <br />
                    86 John Vorster Road <br />
                    Randpark Ridge, 2156 <br />
                </address>
            </div>
        </div>
    </div>

   
</footer>


