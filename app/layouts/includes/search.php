
<style>
.autocomplete {
  position: relative;
  display: inline-block;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>

<div class="card mt-5 p">
    <div class="card-wrapper">
        <h2 class="dark">
            <ion-icon name="search"></ion-icon> Book into a hotel now
        </h2>
        <div class="center"><?php include 'layouts/includes/alerts.php'; 
        ?>
        </div>
        
        <p class="gray">
            Make your best resevation, find it using our search
        </p>
        <?php
               $date = date("y-m-d");
                $newDate = date("Y-m-d", strtotime($date));
                $today = date("Y-m-d", strtotime($date . '+ 1 days' ));
                $nextday = date("Y-m-d", strtotime($date . ' + 3 days'));   
        ?> 
        <form action="<?php url('index/result'); ?>" autocomplete="off" method="Post" id="search" name="search" class="form">
            <div>
                <div class="row">
                    <div class="col3">
                        <div class="autocomplete" >
                        <label for="location">Location</label>
                        <input type="text" class="input" name="location" id="location" value="<?php if(isset($search->search->location )){echo $search->search->location;}else{
                            echo "Cape town";} ?>" >
                    </div>
                    </div>
                    <div class="col3">
                        <label for="checkin">Check-in</label>
                        <input type="date" class="input" name="checkin" id="checkin" value="<?php if(isset($search->result["checkin"])){echo $search->result["checkin"];}else{
                            echo $today;} ?>" >
                    </div>
                    <div class="col3">
                        <label for="checkout">Check-out</label>
                        <input type="date" class="input" name="checkout" id="checkout" value="<?php if(isset($search->result["checkin"])){echo $search->result["checkout"];}else{echo $nextday;} ?>" >
                    </div>
                    <div class="col3">
                            <label for="rooms" class="h4">Number Of Rooms</label>
                            <select name="rooms" id="rooms" class="input" required="required">
                                <option value="<?php if(isset($search->search->rooms)){echo $search->search->rooms;}else{echo "0";} ?>"><?php if(isset($search->search->rooms)){echo $search->search->rooms;}else{echo "0";} ?></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                </div>
                <hr class="separator">
                <div>
           
                    <div id="container" class="row" style="padding-left:2em;">
                    </div>
                </div>

                <div class="right">
                    <label for=""> &nbsp;</label>
                    <input type="submit" name="submit" id="searchBtn" value="Search"   class="primary">
                </div>
            </div>
        </form>

        <!-- new Room COde Here -->
        <div id="room" class="row" style="display:none !important; align-self: start;">
            <p id="nameNewChild"></p>
            <div class="col3">
                <label for="adults">Adults</label>
                <select class="input" name="adults" id="adults">
                    <option value="1" selected="selected">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
            </div>
            <div class="col3">
                <label for="child">Children</label>
                <select class="input" name="child" id="child" onchange="addKid()">
                    <option selected="selected" value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
            </div>
        </div>
        <!-- end new Room COde Here -->
        <!-- new Child Code Here -->
        <div class="" id="kid" style="display: none;">
            <!-- <div class="col3" > -->
            <label for="ages" id="nameNewChild"> </label>
            <select class="input selectId" name="children_ages" id="children_ages">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
            </select>
            <!-- </div> -->
        </div>
        <!-- end new Child Code Here -->
    </div>
</div>
<script>
    document.getElementById("rooms").addEventListener("change", addRoom);
    document.addEventListener("DOMContentLoaded", loadRoom);
    var form = document.forms["search"];
        form.addEventListener("submit", function(){
        var result = validateForm();
        if(result == true){
             showDontPanic();  
        }else{
        event.stopPropagation();
        event.preventDefault();
        }
        });

function loadRoom(){
    var numberOfRooms = document.querySelector("#rooms").value;
    if(numberOfRooms > 0){
        var parent = document.querySelector('#container');
        var child = document.querySelector('#room');
        elements = parent.querySelectorAll('[data-clone]');
        for (let index = 0; index < elements.length; index++) {
            const element = elements[index];
            element.parentNode.removeChild(element);
        }
        for (let index = 0; index < numberOfRooms; index++) {
            addRoomClone(parent, child, index + 1, "Room");
        }
    }


}
    function addRoom() {
        var e = document.getElementById("rooms");
        var nbRoom = e.options[e.selectedIndex].value;
        var parent = document.querySelector('#container');
        var child = document.querySelector('#room');
        elements = parent.querySelectorAll('[data-clone]');
        for (let index = 0; index < elements.length; index++) {
            const element = elements[index];
            element.parentNode.removeChild(element);
        }
        for (let index = 0; index < nbRoom; index++) {
            addRoomClone(parent, child, index + 1, "Room");
        }
 
    }

    function addKid() {
        var nbKids = event.target.value;
        var kidscontainer = event.target.parentNode;
        var child = document.querySelector('#kid');
        elements = kidscontainer.querySelectorAll('[data-clone]');

        for (let index = 0; index < elements.length; index++) {
            const element = elements[index];
            element.parentNode.removeChild(element);
        }
        for (let index = 0; index < nbKids; index++) {
            addRoomClone(kidscontainer, child, index + 1, "Age Child");
        }
    }

    function addRoomClone(parent, child, index, Name) {
        var newChild = child.cloneNode(true);
        newChild.setAttribute('data-clone', 'true');
        parent.appendChild(newChild);
        newChild.style.display = 'block';
        newChild.querySelector("#nameNewChild").textContent = Name + " " + index;
    }

// autofill

const search = document.getElementById("location");
const match = document.getElementById("match");

const searchState = async searchText =>{
    const res = await fetch("list.json");
    const states = await res.json();
// get matches to current text input
let matches = states.filter(state=>{
const regex= new RegExp(`^${searchText}`, 'gi');
return state.name.match(regex) || state.abbr.match(regex);
});
console.log(matches);
};
search.addEventListener('input', ()=> searchState(search.value));










    function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}
// var countries = 
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    debugger
    if (this.readyState == 4 && this.status == 200) {
xhttp.open("GET" , "https://api-sandbox.grnconnect.com/api/v3/cities/" , true);
xhhtp.send();
}
}
//  ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Town","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
autocomplete(document.getElementById("location"), countries);
</script>

