<style>
    [data-id="1"]{
        background: url("<?php url('assets/img/locations/addisababa.jpg')?>")
    }
    
    [data-id="2"]{
        background: url("<?php url('assets/img/locations/kampala-city.jpg')?>")
    }
    [data-id="3"]{
        background: url("<?php url('assets/img/locations/lusaka.jpg')?>")
    }

    [data-id="4"]{
        background: url("<?php url('assets/img/locations/kigali.jpg')?>")
    }

    [data-id="5"]{
        background: url("<?php url('assets/img/locations/windhoek.jpg')?>")
    }
    [data-id="6"]{
        background: url("<?php url('assets/img/locations/lagos.jpg')?>")
    }
    [data-id="7"]{
        background: url("<?php url('assets/img/locations/accra.jpg')?>")
    }
    [data-id="8"]{
        background: url("<?php url('assets/img/locations/capetown.jpg')?>")
    }

    [data-id="9"]{
        background: url("<?php url('assets/img/locations/abidjan.jpg')?>")
    }

.area-name{
background-color: #ffffff3b;

}

.bg-lite [data-id]{
     background-repeat: no-repeat;
    background-size: cover;
}





</style>
<div class="content" id="places">
    <h1 class=" gray py">Our popular locations</h1>
    <div class="card-container col3">
        <div class="bg-lite" data-id="1">
            <p class="area-name p">Addis Ababa</p>
            <span>
            <a class="ml-1" href='<?php url("result?location=Addis Ababa")?>'>
                <img src='https://www.countryflags.io/et/flat/24.png'>
            </a>
        </div>

 
        <div class="bg-lite" data-id="2">
            <p class="area-name p">Kampala</p>
           <a class="ml-1" href='<?php url("result?location=Kampala")?>'>
                <img src='https://www.countryflags.io/ug/flat/24.png'>
           </a>
        </div>
         
          
        
        <div class="bg-lite" data-id="3">
            <p class="area-name p">
                lusaka
            </p>
            <a class="ml-1" href='<?php url("result?location=lusaka")?>'></a>
            <img src='https://www.countryflags.io/zm/flat/24.png'>
        </a>
        </div>
        <div class="bg-lite" data-id="4">
            <p class="area-name p"> Kigali</p>
            <a  class="ml-1" href='<?php url("result?location=Kigali")?>'>
            <img src='https://www.countryflags.io/rw/flat/24.png'>
</a>
        </div>
        <div class="bg-lite" data-id="5">
            <p class="area-name p">
                Winhoek
            </p>
            <a class="ml-1" href='<?php url("result?location=Winhoek")?>'>
            <img src='https://www.countryflags.io/NA/flat/24.png'>
            </a>
        </div>
        <div class="bg-lite" data-id="6">
            <P class="area-name p">
                Lagos
            </P>
            <a class="ml-1" href='<?php url("result?location=Lagos")?>'>
            <img src='https://www.countryflags.io/NG/flat/24.png'>
            </a>
        </div>
        <div class="bg-lite" data-id="7">
            <P class="area-name p">
                Accra
            </P>
            <a class="ml-1" href='<?php url("result?location=Accra")?>'>
            <img src='https://www.countryflags.io/GH/flat/24.png'>
            </a>
        </div>
        <div class="bg-lite" data-id="8">
            <P class="area-name p">Cape Town</P>
            <a class="ml-1" href='<?php url("result?location=Cape Town")?>'>
            <img src='https://www.countryflags.io/ZA/flat/24.png'>
        </a>
        </div>
        <div class="bg-lite" data-id="9">
            <P class="area-name p">
                Abidjan
            </P>
            <a class="ml-1" href='<?php url("result?location=Abidjan")?>'>

            <img src='https://www.countryflags.io/CI/flat/24.png'>
            </a>
        </div>
    </div>
</div>

