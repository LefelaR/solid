function showDontPanic() {
  const body = document.querySelector('body');
    body.classList.add('backdrop');
}
function hideDontPanic() {
    const body = document.querySelector('body');
    body.classList.remove('backdrop');
}

var ShowAlert_timeout = 0;
function ShowAlert(alertType, message, callback) {
    console.log('[ShowAlert]::', alertType, message);
    var alertClass = '';
    switch (alertType) {
        case 'success':
            alertClass = 'alert-success';
            break;
        case 'warning':
            alertClass = 'alert-warning';
            break;
        case 'error':
            alertClass = 'alert-danger';

            break;
        case 'info':
            alertClass = 'alert-info';
            break;
        default:
            alertClass = 'alert-success';
            break;
    }
    var alert = document.getElementById('alert');
    if (!alert)
        return;
    alert.innerHTML = '';
    var classes = ['alert-success', 'alert-warning', 'alert-danger'];
    classes.forEach(function (cls) {
        if (alert.classList.contains(cls)) {
            alert.classList.remove(cls);
        }
    });
    alert.classList.add(alertClass);
    alert.innerHTML = message;
    alert.removeAttribute('hidden');
    if (ShowAlert_timeout > 0) {
        clearTimeout(ShowAlert_timeout);
        console.log("[ShowAlert]:: Clearing Timeout", ShowAlert_timeout, alertType, message);
    }
    var timeout_id = setTimeout(function () {
        clearTimeout(timeout_id);
        console.log("[ShowAlert]:: Running Timeout", alertType, message);
        alert.setAttribute('hidden', '');
        alert.innerHTML = '';
        if (callback) {
            callback();
            console.log("[ShowAlert]:: Running timeout callback", alertType, message);
        }
    }, 3000);
    ShowAlert_timeout = timeout_id;
}

function validateForm() {
    
        var location = document.forms["search"]["location"];
        var checkin = document.forms["search"]["checkin"];
        var checkout = document.forms["search"]["checkout"];
        var rooms = document.forms["search"]["rooms"];
       
                var l = location.value;
                if (l == "") {
                    location.classList.add("validate-danger");
                    ShowAlert( "error" , "Loaction field cannot be empty");
                    location.focus(); 
                    return false;
                  }
           

              var ci = checkin.value;
              if (ci == 0) {
                  checkin.classList.add("validate-danger");
                  ShowAlert( "error" , "you must enter check-in date");
                  location.focus(); 
                  return false;
          }

          
          var co = checkout.value;
          if (co == 0) {
            checkout.classList.add("validate-danger");
              ShowAlert( "error" , "you must enter check-out date");
              location.focus();
              return false;
      }
      
      var r = rooms.value;
      if (r == 0) {
          rooms.classList.add("validate-danger");
          ShowAlert( "error" , "You have to specify the number of rooms");
          location.focus(); 
          return false;
  }


              return true;
  }

  function terms_change(checkbox){
    if(checkbox.checked){
        document.forms["reserve"]["submit"].removeAttribute("hidden");
    }
    else{
        document.forms["reserve"]["submit"].setAttribute("hidden","hidden");
    }
}




