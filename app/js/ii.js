var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/* Copyright Lucid Ocean, Wave Business License, WBL 1.0 */
var iignition;
(function (iignition) {
    let info;
    (function (info) {
        info["version"] = "4.2";
    })(info = iignition.info || (iignition.info = {}));
    let LogLevel;
    (function (LogLevel) {
        LogLevel[LogLevel["Minimal"] = 1] = "Minimal";
        LogLevel[LogLevel["Standard"] = 2] = "Standard";
        LogLevel[LogLevel["Verbose"] = 3] = "Verbose";
    })(LogLevel = iignition.LogLevel || (iignition.LogLevel = {}));
    class Extension {
        constructor(ctx = null) {
            this.Context = ctx;
        }
        handle(ctx) {
            return new Promise((resolve, reject) => {
                resolve();
            });
        }
    }
    iignition.Extension = Extension;
    class DataComponent {
        fetch(input, init) {
            return null;
        }
    }
    iignition.DataComponent = DataComponent;
    class RouteComponent {
        constructor() {
            this._Pipeline = [];
        }
        add(extension) {
            this._Pipeline.push(extension);
        }
        run(ctx) {
            return __awaiter(this, void 0, void 0, function* () {
                //lets not use await here if possible,
                for (let idx = 0; idx < this._Pipeline.length; idx++) {
                    this._Pipeline[idx].handle(ctx);
                }
            });
        }
    }
    iignition.RouteComponent = RouteComponent;
    //export type Extension = RouteExtension | SplashExtension | ViewExtension;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Options {
        constructor() {
            this.preventDoublePosting = true;
            this.enableCache = true;
            this.debug = false;
            this.spa = true;
            this.domainRoot = "";
            this.viewPath = "views";
            this.controllerPath = "viewControllers";
            this.controllerjs = ""; // hard coded from client
        }
        apply() {
            if (this.debug === true || this.debug > 0) {
                iignition.Console.enable();
            }
            else {
                iignition.Console.disable();
            }
        }
    }
    iignition.Options = Options;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Console {
        static disable() {
            Console._console = Object.assign({}, console);
            for (let prop in console) {
                Console._props.push(console[prop]);
                console[prop] = function () { };
            }
        }
        static enable() {
            Object.assign(console, Console._console);
            console.log = function (msg, ...optionalParams) {
                if ($i.Options.debug > 0) {
                    Console._console.log(`%c ${msg}`, 'background: #ffeeee; color: #000', ...optionalParams);
                }
            };
            console.dir = function (value, ...optionalParams) {
                if (value != undefined && $i.Options.debug > 2) {
                    Console._console.dir(value, ...optionalParams);
                }
            };
            console.info = function (message, ...optionalParams) {
                if ($i.Options.debug > 1) {
                    Console._console.info(message, ...optionalParams);
                }
            };
            console.group = function (groupTitle, ...optionalParams) {
                if ($i.Options.debug > 1) {
                    Console._console.group(groupTitle, ...optionalParams);
                }
            };
            console.groupEnd = function () {
                if ($i.Options.debug > 1) {
                    Console._console.groupEnd();
                }
            };
        }
    }
    Console._props = [];
    iignition.Console = Console;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Events {
        static raiseEvent(name, data) {
            console.info(`%c Event ${name}`, 'background: #000; color: #ffff00');
            console.dir(data);
            var evt = new CustomEvent(name, { detail: data });
            window.dispatchEvent(evt);
        }
    }
    iignition.Events = Events;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Data extends iignition.DataComponent {
        constructor() {
            super();
        }
        /**
          * Returns Data from a Server URL
          *
          *
          * @param url - The url to fetch
          * @param input - Request parameters
          * @returns A promise with the Data as param
          *
         */
        fetch(url, input) {
            iignition.Events.raiseEvent('onDataRequested', input);
            let defaultInput = { method: 'GET' };
            input = Object.assign(defaultInput, input);
            if (!input.method) {
                input.method = 'GET';
            }
            return new Promise((resolve, reject) => {
                fetch(url, {
                    method: input.method,
                    headers: input.headers,
                    mode: 'cors',
                    body: input.body
                }).then((response) => {
                    var gettype = response.headers.get("Content-Type");
                    if (response.headers.has("content-type")) {
                        if (response.headers.get("content-type").indexOf("application/json") !== -1) { // checking response header
                            return response.json();
                        }
                        else if (response.headers.get("content-type").indexOf("text/html") !== -1) { // checking response header
                            return response.text();
                        }
                        else if (response.headers.get("content-type").indexOf("application/javascript") !== -1) { // checking response header
                            return response.text();
                        }
                        else if (response.headers.get("content-type").indexOf("application/x-javascript") !== -1) { // checking response header
                            return response.text();
                        }
                        else if (response.headers.get("content-type").indexOf("text/xml") !== -1) { // checking response header
                            return response.text();
                        }
                        else if (response.headers.get("content-type").indexOf("image/jpeg") !== -1) { // checking response header
                            return response.blob();
                        }
                        else if (response.headers.get("content-type").indexOf("image/png") !== -1) { // checking response header
                            return response.blob();
                        }
                        else if (response.headers.get("Content-Type").indexOf("application/pdf") !== -1) { // checking response header
                            return response.clone();
                        }
                        else {
                            const response = new Response();
                            //throw new TypeError('Response from "' + url + '" has unexpected "content-type"');
                        }
                    }
                })
                    .then((data) => {       
                        resolve(data);
                        iignition.Events.raiseEvent('onDataReceived', input);
                    })
                    .catch((err) => {
                        reject();
                    });
            });
        }
    }
    iignition.Data = Data;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class RoutingUtility {
        constructor(url, controllerPath, domainRoot) {
            //public viewPath = 'views';
            this.ControllerPath = '';
            if (controllerPath) {
                if (!controllerPath.endsWith('/')) {
                    controllerPath += '/';
                }
                this.ControllerPath = controllerPath;
            }
            this.domainRoot = domainRoot;
            if (url && url != '') {
                this.processUrl(url);
            }
        }
        processUrl(url) {
            var stdReg = /\/$/;
            this.originalUrl = url;
            url = url.replace('.html', '');
            this.root = url.split('#')[0].replace(stdReg, '/');
            this.root = this.root.replace($i.Options.domainRoot, '');
            this.root = this.root.substr(0, this.root.lastIndexOf('/') + 1);
            if ($i.Options.controllerjs == '') {
                this.controller = url.substr(url.lastIndexOf('/') + 1);
                this.controller = this.controller.replace('.html', '');
                if (this.ControllerPath == '') {
                    this.controllerjs = url.replace('#!', '').replace('.html', '.js');
                }
                else {
                    this.controllerjs = this.controller.replace('#!', '').replace('.html', '.js');
                }
                this.controllerjs = `${this.domainRoot}${this.ControllerPath}${this.root}${this.controllerjs}`;
                if (!this.controllerjs.includes('.js')) {
                    this.controllerjs = `${this.controllerjs}.js`;
                }
                else {
                    this.controllerjs = `${this.controllerjs}`;
                }
            }
            else {
                this.controllerjs = $i.Options.controllerjs;
                this.controller = this.controllerjs.substr(this.controllerjs.lastIndexOf('/') + 1);
                this.controller = this.controller.replace('.js', '');
            }
            if (url.includes('#') && !url.includes('#!')) {
                this.isHash = true;
                this.view = '';
                return;
            }
            this.isHash = false;
            if (url.includes('#!')) {
                this.hash = '#!' + url.split('#!')[1].replace(stdReg, '/');
            }
            else {
                this.hash = '#!' + this.root;
            }
            this.parts = this.hash.replace('#!', '').split('/');
            if (this.parts[0] != $i.Options.viewPath) {
                this.parts.splice(0, 0, $i.Options.viewPath);
            }
            if (!this.parts[this.parts.length - 1].toLowerCase().includes('.html')) {
                this.parts.push('.html');
            }
            this.view = this.parts.join('/').replace(stdReg, '/').replace(/\/\./, '.');
            console.info(this);
        }
    }
    iignition.RoutingUtility = RoutingUtility;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class RouteHandler extends iignition.RouteComponent {
        run(ctx) {
            const _super = Object.create(null, {
                run: { get: () => super.run }
            });
            return __awaiter(this, void 0, void 0, function* () {
                _super.run.call(this, ctx);
            });
        }
    }
    iignition.RouteHandler = RouteHandler;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class ControllerHandler extends iignition.RouteComponent {
        constructor(ctx) {
            super();
            this.ControllerPath = '';
            if (ctx && ctx.controllerPath) {
                this.ControllerPath = ctx.controllerPath;
            }
        }
        run(ctx) {
            return __awaiter(this, void 0, void 0, function* () {
                let routing = null;
                if (ctx.view) {
                    routing = new iignition.RoutingUtility(ctx.view, $i.Options.controllerPath, $i.Options.domainRoot);
                }
                else {
                    routing = ctx;
                }
                if (ctx.spa == false) {
                    delete routing.view;
                }
                //using await, because we need the view and the controller loaded.
                for (let idx = 0; idx < this._Pipeline.length; idx++) {
                    yield this._Pipeline[idx].handle(routing);
                }
                console.info('Run Route Handler for new context');
                $i.RouteHandler.run(routing);
            });
        }
    }
    iignition.ControllerHandler = ControllerHandler;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Controller {
        constructor() {
        }
        onInit() {
        }
        onLoad(data = null) {
            return new Promise((resolve, reject) => { resolve(); });
        }
        onSubmit(form, data = null) {
            return new Promise((resolve, reject) => { resolve(); });
        }
    }
    iignition.Controller = Controller;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Runtime {
        constructor(route) {
            console.log('constructor loaded');
            this.load();
        }
        load() {
            document.addEventListener('DOMContentLoaded', () => {
                var statusSelector = 'span[data-iistatus]';
                var statusEl = document.querySelector(statusSelector);
                statusEl.innerHTML = 'loaded from load';
            });
        }
    }
    iignition.Runtime = Runtime;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class State {
        constructor() {
        }
    }
    iignition.State = State;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Context {
        constructor() {
        }
    }
    iignition.Context = Context;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Splash {
        constructor() {
        }
        handleRowBind(rowbind, clone, obj) {
            if (rowbind) {
                rowbind(clone, obj);
            }
        }
        prepareTemplate(obj, clone) {
            let dataElements = clone.querySelectorAll('[data-splash]:not([data-splashtemplate])');
            this.applyTemplate(obj, clone);
            dataElements.forEach(dataElement => {
                this.applyTemplate(obj, dataElement);
            });
        }
        applyTemplate(obj, dataElement) {
            let splashAttr = dataElement.getAttribute('data-splash');
            if (splashAttr == null)
                return;
            let splashArr = splashAttr.split(',');
            for (var i = 0; i < splashArr.length; i++) {
                var item = splashArr[i];
                var attr = null;
                var property = item;
                if (item.includes('=')) {
                    var arr = item.split('=');
                    property = arr[1];
                    attr = arr[0];
                }
                else {
                    attr = null;
                }
                let value = obj[property];
                let nodeName = dataElement.nodeName.toLowerCase();
                if (attr) {
                    dataElement.setAttribute(attr, value);
                }
                else {
                    let valueNodes = ['input', 'textarea'];
                    if (valueNodes.includes(nodeName)) {
                        let inputElement = dataElement;
                        if (inputElement.type == 'checkbox') {
                            inputElement.checked = (value == true || value == 1) ? true : false;
                        }
                        else {
                            inputElement.value = value;
                        }
                    }
                    else {
                        dataElement.textContent = value;
                    }
                    //dataElement.setAttribute('data-data', JSON.stringify(obj));
                }
            }
        }
        map(element, data, rowbind) {
            return new Promise((resolve, reject) => {
                try {
                    let container = document.querySelector(element);
                    let splashtemplate = container.querySelector('[data-splashtemplate]');
                    data.forEach(obj => {
                        let clone = container;
                        let mustClone = false;
                        if (splashtemplate) {
                            clone = splashtemplate.cloneNode(true);
                            clone.setAttribute('data-clone', '');
                            mustClone = true;
                        }
                        this.handleRowBind(rowbind, clone, obj);
                        this.prepareTemplate(obj, clone);
                        if (mustClone) {
                            container.appendChild(clone);
                        }
                    });
                    if (container.nodeName.toLowerCase() == 'select') {
                        container.querySelector('[data-splashtemplate]:not([data-clone])').remove();
                    }
                    resolve();
                    iignition.Events.raiseEvent('onDataSplashed', { element: element, data: data });
                }
                catch (error) {
                    reject(error);
                }
            });
        }
    }
    iignition.Splash = Splash;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class FormExtension extends iignition.Extension {
        constructor(ctx = null) {
            super(ctx);
            if (ctx && ctx.selector)
                this.Selector = ctx.selector;
            this.formSubmitHandler = this.formSubmitHandler.bind(this);
            // this.formSubmitHandler = this.formSubmitHandler.bind(this); 
        }
        handle(ctx) {
            console.info('Form Extension');
            return new Promise((resolve, reject) => {
                this.Form = document.querySelector(this.Selector);
                if (this.Form) {
                    this.Form.addEventListener('submit', this.formSubmitHandler, true);
                }
                resolve();
            });
        }
        formSubmitHandler(event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var formData = this.serialize(this.Form);
            let ctx = { form: this.Form, data: formData };
            $i.ControllerHandler.run(ctx);
            iignition.Events.raiseEvent('onFormSubmitted', formData);
        }
        serialize(form) {
            var formData = new FormData(form);
            var object = {};
            formData.forEach((value, key) => {
                if (!object.hasOwnProperty(key)) {
                    object[key] = value;
                    return;
                }
                if (!Array.isArray(object[key])) {
                    object[key] = [object[key]];
                }
                object[key].push(value);
            });
            return object;
        }
    }
    iignition.FormExtension = FormExtension;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class RouteExtension extends iignition.Extension {
        constructor(ctx = null) {
            super(ctx);
            this._Pipeline = [];
            this.clickHandler = this.clickHandler.bind(this);
            this.popstateHandler = this.popstateHandler.bind(this);
            this.hashChangeHandler = this.hashChangeHandler.bind(this);
            window.removeEventListener('popstate', this.popstateHandler);
            window.addEventListener('popstate', this.popstateHandler);
            window.removeEventListener('hashchange', this.hashChangeHandler);
            window.addEventListener('hashchange', this.hashChangeHandler);
            var stateObj = { view: "index" };
            history.pushState(stateObj, "index", location.hash);
            this.Selector = ctx.selector;
        }
        add(extension) {
            if (this._Pipeline.length > 0) {
                let index = this._Pipeline.length - 1;
                this._Pipeline[index].Next = extension;
            }
            this._Pipeline.push(extension);
            this.Pipeline = this._Pipeline[0];
        }
        handle(ctx) {
            console.log('Route Extension');
            return new Promise((resolve, reject) => {
                let elements = document.querySelectorAll(this.Selector);
                if (elements) {
                    elements.forEach(element => {
                        console.log('Click Event Handlers wired');
                        element.removeEventListener('click', this.clickHandler, true);
                        element.addEventListener('click', this.clickHandler, true);
                    });
                }
                resolve();
            });
        }
        clickHandler(event) {
            console.info('Route Click Handler');
            var url = event.target.getAttribute('href');
            if (url == null) {
                let node = event.target.closest('[data-link]');
                url = node.getAttribute('data-link');
            }
            let dataset = { data: {} };
            Object.assign(dataset, event.target.dataset);
            if (event.target.dataset.data) {
                dataset.data = JSON.parse(event.target.dataset.data);
            }
            const stateObj = { view: url, data: dataset.data };
            history.pushState(stateObj, document.title, location.hash);
            console.info(`Route is ${url}`);
            console.group(`Route State is`);
            console.dir(stateObj);
            console.groupEnd();
            $i.ControllerHandler.run(stateObj);
            event.preventDefault();
        }
        hashChangeHandler() {
            const stateObj = { view: location.hash };
            history.pushState(stateObj, document.title, location.hash);
            console.info('Route Hash Change');
            console.group(`Route State is`);
            console.dir(stateObj);
            console.groupEnd();
        }
        popstateHandler(event) {
            console.info('Route PopState Change');
            console.group(`Route State is`);
            console.dir(event.state);
            console.groupEnd();
            const stateObj = event.state;
        }
    }
    iignition.RouteExtension = RouteExtension;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class ViewExtension extends iignition.Extension {
        constructor(ctx = null) {
            super(ctx);
            if (ctx) {
                this.Selector = ctx.container;
            }
        }
        handle(ctx) {
            console.info('View Extension');
            return new Promise((resolve, reject) => {
                this.show(ctx).then(() => {
                    resolve();
                });
            });
        }
        show(ctx) {
            return new Promise((resolve, reject) => {
                if (!ctx.view) {
                    resolve();
                    return;
                }
                var promises = [];
                $i.Data.fetch(ctx.view).then((html) => {
                    if (typeof this.Selector === "string") {
                        this.Selector = document.querySelector(this.Selector);
                    }
                    if (this.Selector instanceof Element) {
                        this.Selector.innerHTML = html;
                        console.log('View Loaded');
                        iignition.Events.raiseEvent('onViewLoaded', { view: ctx.view });
                        resolve();
                    }
                })
                    .catch((e) => {
                        reject();
                    });
            });
        }
    }
    iignition.ViewExtension = ViewExtension;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class ControllerExtension extends iignition.Extension {
        constructor(ctx = null) {
            super(ctx);
            this._ControllerCache = new Map();
            this._CurrentController = null;
            this.ControllerPath = '';
        }
        handle(ctx) {
            console.info('Controller Extension');
            return new Promise((resolve, reject) => {
                if (ctx && ctx.controllerjs) {
                    this.executeController(ctx).then(() => {
                        resolve();
                    });
                }
                else if (ctx && ctx.form) {
                    return this.executeForm(ctx).then((returnedctx) => {
                        resolve();
                    });
                }
                else {
                    resolve();
                }
            });
        }
        executeForm(ctx) {
            return this._CurrentController.onSubmit(ctx.form, ctx.data);
        }
        executeController(ctx) {
            return new Promise((resolve, reject) => {
                let link = document.location.href;
                if (ctx.link) {
                    link = ctx.link;
                }
                var promises = [];
                promises.push(this.loadScript(ctx));
                Promise.all(promises)
                    .then((results) => {
                        let controller = results[0];
                        controller.onInit();
                        controller.onLoad(ctx.data).then((ret) => {
                            this._ControllerCache[controller.constructor.name] = controller;
                            this._CurrentController = controller;
                            console.dir(this._ControllerCache);
                            console.log('Controller Loaded');
                            resolve();
                        }).catch((error) => {
                            console.error('error', error);
                        });
                    });
            });
            return;
        }
        loadScript(routing) {
            return new Promise((resolve, reject) => {
                $i.Data.fetch(routing.controllerjs).then((html) => {
                    let scriptElement = document.getElementsByTagName("script")[0];
                    let script = document.createElement("script");
                    script.textContent = html;
                    script.id = 'controller';
                    scriptElement.parentNode.insertBefore(script, scriptElement);
                    var controller = eval(` new ${routing.controller}()`);
                    resolve(controller);
                });
            });
        }
    }
    iignition.ControllerExtension = ControllerExtension;
})(iignition || (iignition = {}));
var iignition;
(function (iignition) {
    class Core {
        constructor() {
            this._onReadyCallbacks = new Array();
            this._onViewChangedCallbacks = [];
            this._fireReady = false;
            this.Data = new iignition.Data();
            this.Splash = new iignition.Splash();
            this._Pipeline = [];
            iignition.Console.disable();
            this.defaultOptions();
            this.RouteHandler = new iignition.RouteHandler();
            this.RouteHandler.add(new iignition.RouteExtension({ selector: 'a[href^="#!"],[data-link]' }));
            this.RouteHandler.add(new iignition.FormExtension({ selector: 'form:not([data-staticform])' }));
            this.ControllerHandler = new iignition.ControllerHandler({});
            this.ControllerHandler.add(new iignition.ViewExtension({ container: '[data-viewContainer]' }));
            this.ControllerHandler.add(new iignition.ControllerExtension({}));
            if (document.readyState == 'loading') {
                let _This = this;
                document.addEventListener('DOMContentLoaded', function () {
                    _This.fireOnReady.call(_This);
                });
            }
            else {
                this.fireOnReady();
            }
        }
        defaultOptions() {
            this.Options = new iignition.Options();
        }
        register(component) {
            if (component instanceof iignition.DataComponent) {
                this.Data = component;
            }
            if (component instanceof iignition.RouteComponent) {
                this.RouteHandler = component;
            }
            // this.RouteHandler.run();
        }
        fireOnReady() {
            console.log(`debug is ${this.Options.debug}`);
            //var ii_form = new iignition.FormHandler('form:not([data-staticform])');
            //var ii_route = new iignition.Route('a[href^="#!"],[data-link]');
            //this.Pipeline.handle({link:document.location.pathname});
            // this.DefaultFormHandler = ii_form;
            // this.DefaultFormHandler.listen();
            // this.RouteHandler.run();
            if (this._fireReady == false) {
                for (var item in this._onReadyCallbacks) {
                    if (this._onReadyCallbacks[item]) {
                        this._onReadyCallbacks[item].callback();
                    }
                }
                this._fireReady = true;
            }
            this.RouteHandler.run({});
            this.ControllerHandler.run({ "spa": this.Options.spa, "view": "index.html", "controllerPath": this.Options.controllerPath });
        }
        ready(options, callback) {
            if (typeof options === 'function') {
                callback = options;
            }
            else {
                this.Options = Object.assign(this.Options, options);
                this.Options.apply();
            }
            this.init();
            if (callback != undefined) {
                if (this._fireReady == false) {
                    this._onReadyCallbacks.push({ callback: callback });
                }
                else {
                    callback();
                }
            }
        }
        init() {
        }
        test() {
            console.log('This is a test of the exploding sound system.');
            console.log(`iignition Version ${iignition.info.version}`);
        }
    }
    iignition.Core = Core;
})(iignition || (iignition = {}));
let $i = new iignition.Core();
//$i.add(new iignition.ARouteExtension({selector:'a[href^="#!"],[data-link]'}));
//$i.add(new iignition.RouteExtension({selector:'a[href^="#!"],[data-link]'}));
//$i.add(new iignition.ViewExtension({ container: '[data-viewContainer]' }));
//$i.add(new iignition.FormExtension({ selector: 'form:not([data-staticform])' }));
//$i.add(new iignition.FormValidationExtension({ selector: 'form:not([data-staticform])' }));
//$i.add(new iignition.ControllerExtension({ })); // controllerPath:'controllers'
//$i.register(new iignition.RouteExtension());
//$i.register(new iignition.Data());
//# sourceMappingURL=ii.js.map