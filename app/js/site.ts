
function showDontPanic() {
    const body = document.querySelector('body');
    body.classList.add('backdrop');
}
function hideDontPanic() {
    const body = document.querySelector('body');
    body.classList.remove('backdrop');
}
function ShowAlert(alertType, message, callback) {
    console.log('[ShowAlert]::', alertType, message);
    var alertClass = '';
  
    switch (alertType) {
      case 'success':
        alertClass = 'alert-success';
        break;
      case 'warning':
        alertClass = 'alert-warning';
        break;
      case 'error':
        alertClass = 'alert-danger';
        break;
      case 'info':
        alertClass = 'alert-info';
        break;
      default:
        alertClass = 'alert-success';
        break;
    }
  
    var alert = document.getElementById('alert');
    if (!alert) return;
    alert.innerHTML = '';
  
    var classes = ['alert-success', 'alert-warning', 'alert-danger'];
    classes.forEach(function (cls) {
      if (alert.classList.contains(cls)) {
        alert.classList.remove(cls);
      }
    });
  
    alert.classList.add(alertClass);
    alert.innerHTML = message;
  
    alert.removeAttribute('hidden');
  
    if (ShowAlert_timeout > 0) {
      clearTimeout(ShowAlert_timeout);
      console.log("[ShowAlert]:: Clearing Timeout", ShowAlert_timeout, alertType, message);
    }
  
    var timeout_id = setTimeout(function () {
      clearTimeout(timeout_id);
  
      console.log("[ShowAlert]:: Running Timeout", alertType, message);
      alert.setAttribute('hidden', '');
      alert.innerHTML = '';
      if (callback) {
        callback();
        console.log("[ShowAlert]:: Running timeout callback", alertType, message);
      }
    }, 3000);
  
    ShowAlert_timeout = timeout_id;
  }
  