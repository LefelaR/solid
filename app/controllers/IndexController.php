<?php

class IndexController
{

    private $model;
    private $seo;
    public function __construct()
    {  
    // $this->seo ;= array('title' => '', 'keywords' => '', 'description' => '');
  
}

    function Index($id, $data)

    {
       
        global $seahorse;
        $config = new Config('1.0');
        $qry = "SELECT 
        `cit`.`City_Name`,
        `cou`.`name`,
        `cou`.`iso`
         FROM city as `cit` 
         left join country as `cou` 
         on  `cit`.`country_code` = `cou`.`iso`";
        // $db = new DataService($config->dbConnection);
        // $result = $db->ExecStatement($qry);
        // $result = json_encode($result);
        $model = new stdClass();
        // $_SESSION["area"] = $result;
    
        $this->seo['title'] = 'Travel Management Solutions';
        $this->seo['description'] = 'Trade Routes Travel is a Corporate travel company servicing the business traveler across the African Continent.';
        $this->seo['keywords'] = '';
        return new ActionResult('index', null, 'layouts/homepagelayout.php', $this->seo);
    }


    function Result($id, $data)
    {
        $array = [];
        $array1 = explode(",", $data->child);
        
        if (isset($data->children_ages)) {
            $array2 = explode(",", $data->children_ages);
        } else {
            $array2 = [];
        }
        $array3 = explode(",", $data->adults);
        $pos = 0;

        for ($i = 0; $i < count($array1); $i++) {
            $arr = array_slice($array2, $pos, $array1[$i]);
            array_push($array, $arr);
            $pos += $array1[$i];
        }

        $rooms = [];

        foreach ($array as $key => $value) {
            $adult = new stdClass();
            $adult->adults = $array3[$key];
            $children = [];

            if (count($array[$key]) > 0) {
                foreach ($value as $index => $child) {
                    array_push($children, $child);
                }
                $adult->children_ages = $children;
            }
            array_push($rooms, $adult);
        }

        $qry = "SELECT `city_code` FROM `ria-web`.city WHERE `city_name` like '%" . $data->location . "%'";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $cc = $db->ExecStatement($qry);
        $city_code = $cc[0]["city_code"];


        $qry = "SELECT `code` FROM `ria-web`.hotel WHERE `citycode` like '%" . $city_code . "%'LIMIT 50";
        $config = new Config('1.0');
        $db = new DataService($config->dbConnection);
        $result = $db->ExecStatement($qry);

        if ($result != null) {
            $hotel_codes = [];
            foreach ($result as $key => $value) {
                array_push($hotel_codes, $value["code"]);
            }
        } else {
            $hotel_codes = ["H!0022317"];
    }

        global $seahorse;
        $model = new stdClass();
        $model->search = $data;

        $body = [
            "client_nationality" => "ZA", "cutoff_time" => 50000, "currency" => "ZAR", "rates" => "concise",
            "hotel_category" => [3, 7], "rooms" => $rooms
        ];

        $body["hotel_codes"] = $hotel_codes;
        $body["checkin"] = $data->checkin;
        $body["checkout"] = $data->checkout;
        $model->body = $body;
        include_once "./components/Headers.php";
        $headers = new Headers();
        $model->headers = $headers->get();
        $url = "api/v3/hotels/availability";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Post($url,  $model->headers, $model->body);

        if (isset($model->result["errors"])) {
            $errors = new stdClass();
            $errors = $model->result["errors"][0];
            $model->errors = $errors;
        }

        $_SESSION["cp_code"]= '';
        $this->seo['title'] = 'Travel Management Solutions';
        $this->seo['description'] = 'Trade Routes search results.';
        $this->seo['keywords'] = '';
        return new ActionResult('result', $model, 'layouts/resultlayout.php', $this->seo);
    }

    function Contact($id, $data)
    {
        $model = new stdClass();
        global $seahorse;
        require_once './components/Helpers/EmailSender.php';
        if (isset($data)) {
            $name = htmlentities(isset($data->firstname));
            if(isset($data->email)){
                $to = $data->email;
            }

            $message = htmlentities(isset($data->message));
            useClass('Contact', $data);
            Contact::save($data);
            $obj = new stdClass();
            $obj->name = $to;
            $obj->date = date("d-m-Y H:i:s");
            $obj->cta_text = "Contact From Ria";
            $obj->cta_message = $data->message;
            $obj->cta_link = $seahorse->config->siteurl;
            useclass("Email", $data);
            $em = new Email();
            $em->SendEmail($to, 4, $obj);

        }
        $this->seo['title'] = 'Contact Us';
        $this->seo['description'] = 'Trade Routes contact us';
        $this->seo['keywords'] = '';
        return new ActionResult('contact', $model, 'layouts/resultlayout.php');
    }

    function terms()
    {

        return new ActionResult('terms', null, 'layouts/resultlayout.php' );
    }

    function privacy_policy(){


        return new ActionResult('privacy_policy', null);
    }


    function NotFound()
    {
        $this->seo['title'] = 'OOPS: Page not found';
        $this->seo['description'] = 'No need too worry, just follow the jelly beans back home';
        $this->seo['keywords'] = '';
        return new ActionResult('notfound', null, 'layouts/notfoundLayout.php', $this->seo);
    }
}
