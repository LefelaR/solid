<?php
class BookingController
{

    private $model;
    private $seo;
    public function __construct()
    {
        $this->seo = array('title' => '', 'keywords' => '', 'description' => '');
    }

    function Index()
    {
        global $seahorse;
        $config = new Config('1.0');
        $model = new stdClass();
        $sid = $_REQUEST['search_code'];
        $hcode = $_REQUEST['hotel_code'];

        include_once "./components/Headers.php";
        $headers = new Headers();
        $model->headers = $headers->get();
        //refetch
        $url = "api/v3/hotels/availability/" . $sid . "?hcode=" . $hcode."&bundled=true";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Get($url,  $model->headers);
        //get hotel images
        $url = "api/v3/hotels/" . $hcode . "/images";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $image_list = $api->Get($url,  $model->headers);
    
        foreach ($model->result["hotel"]["rates"] as $key => $value) {
 
        if($model->result["hotel"]["rates"][$key]["non_refundable"] == false){

           if(in_array("cancellation_policy_code",$value)){
            $pload = [
                 "cp_code" => $value["cancellation_policy_code"],
                 "rate_key" => $value["rate_key"]
            ];
            $url ="/api/v3/hotels/availability/$sid/rates/cancellation_policies/";
            $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
            $result =  $api->Post($url, $model->headers ,$pload);
            $model->result["hotel"]["rates"][$key]["cancellation_policy"] = $result;
          }
        }
    }

        $model->image_list = $image_list["images"]["regular"];
        $this->seo['title'] = "Bookings";
        $this->seo['description'] = 'Trade Routes Booking system';
        return new ActionResult('index', $model, 'layouts/resultlayout.php', $this->seo);
    }


    function  Reserve($id, $data)
    {
        global $seahorse;
        $config = new Config('1.0');
        $rate_key = $_REQUEST["rate_key"];
        $group_code = $_REQUEST["group_code"];
        $check_in = $_REQUEST["check_in"];
        $checkout = $_REQUEST["checkout"];
        $number_of_rooms = $_REQUEST["number_of_rooms"];
        $sid = $_REQUEST["sid"];
        $room_reference = $_REQUEST["room_reference"];
        $model = new stdClass();
        include_once "./components/Headers.php";
        $headers = new Headers();
        $model->headers = $headers->get();
        $body = [];
        $body = [
            "rate_key" => $rate_key,
            "group_code" => $group_code
        ];
        $model->body = $body;
        $url = "api/v3/hotels/availability/" . $sid . "/rates/?action=recheck";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Post($url,  $model->headers, $body);

        if(isset($model->result["errors"])){
            $errors["code"] = $model->result["errors"][0]["code"];
            $errors["message"] = $model->result["errors"][0]["messages"][0];  
            $alert = new Alert();
            $model->errors = $alert->Errors('danger', $errors);
        }

        $_SESSION["hotelData"] = $model->result;
        $model->Checkin = $check_in;
        $model->Checkout = $checkout;
        $model->room_reference = $room_reference;
        $model->rate_key = $rate_key;
        return new ActionResult('reserve', $model, 'layouts/resultlayout.php', $this->seo);
    }


    function  ProcessBooking($id, $data)
    {
        $request = "process_booking";
        global $seahorse;
        $config = new Config('1.0');
        $model = new stdClass();
        $errors = [];
        $booking_name = "RIA_" . rand();
        include_once "./components/Headers.php";
        $headers = new Headers();
        $model->headers = $headers->get();
        $_SESSION["hotelData"];
        $hotels = $_SESSION["hotelData"];
        $rooms = $hotels["hotel"]["rate"]["rooms"];
        $roomArray = [];
        $arrayTitle = explode(",", $data->title);
        $arrayName = explode(",", $data->name);
        $arraySurname = explode(",", $data->surname);

        if(isset($data->title_child)){

            $arrayTitleChild = explode(",", $data->title_child);
            $arrayNameChild = explode(",", $data->name_child);
            $arraySurnameChild = explode(",", $data->surname_child);
            $arrayAgeChild = explode(",", $data->age);
        }



        foreach ($rooms as $key => $value) {
            $paxes[$key] = [];
            for ($i = 0; $i < $rooms[$key]["no_of_adults"]; $i++) {
                $paxe = new stdClass();
                $paxe->title = $arrayTitle[$i];
                $paxe->name = $arrayName[$i];
                $paxe->surname = $arraySurname[$i];
                $paxe->type = "AD";
                array_push($paxes[$key], $paxe);
            }

            if($rooms[$key]["no_of_children"] > 0){
                for ($i = 0; $i < $rooms[$key]["no_of_children"]; $i++) {
                    $paxeChild = new stdClass();
                    $paxeChild->title = $arrayTitleChild[$i];
                    $paxeChild->name = $arrayNameChild[$i];
                    $paxeChild->surname = $arraySurnameChild[$i];
                    $paxeChild->age = $arrayAgeChild[$i];
                    $paxeChild->type = "CH";
                    array_push($paxes[$key], $paxeChild);
                }
            }

        }
        $newarr = [];
        foreach ($paxes as $key => $value) {
            $obj = new stdClass();
            $obj->paxes = $paxes[$key];
            $obj->room_reference = $rooms[$key]["room_reference"];
            array_push($newarr, $obj);
        }
        
        $bookingIems = [
            "room_code"=>$data->room_code,
            "rate_key" => $data->rate_key,
            "rooms" => $newarr
        ];
        $bookingItemArray = [];
        array_push($bookingItemArray, $bookingIems);

        if($hotels["hotel"]["rate"]["supports_cancellation"] == false){
     
            $pload = new stdClass();
             $pload = [
            "rate_key"=> $hotels["rate"]["rate_key"] ,
             "cp_code"=> $hotels["cancellation_policy_code"]
        ];


        $url = "api/v3/hotels/availability/".$data->search_id."/rates/cancellation_policies/";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $res =  $api->Post($url,  $model->headers, $pload);
        $model->body->cancelation_policy = $res;
       
    }

        $body = new stdClass();
        $body = [
            "search_id" => $data->search_id,
            "payment_type" =>  $data->payment_type,
            "hotel_code" => $data->hotel_code,
            "group_code" => $data->group_code,
            "city_code" => $data->city_code,
            "checkin" => $data->check_in,
            "checkout" => $data->check_out,
            "booking_name" => $booking_name,
            "booking_items" => $bookingItemArray,
            "holder" => [
                "title" => $data->title_holder,
                "surname" => $data->surname_holder,
                "phone_number" => $data->number_holder,
                "name" => $data->name_holder,
                "email" => $data->email_holder,
                "client_nationality" => "ZA"
            ]
        ];

        $model->body = $body;
        $url = "api/v3/hotels/bookings";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Post($url,  $model->headers, $body);


    if($model->result["status"] == "pending" ){
        $booking_reference = $booking_reference;
        $url = "api/v3/hotels/bookings/".$booking_reference."?type=value";
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Get($url,  $model->headers);
    }

        if (isset($model->result["errors"])) {
            $model->errors =  $model->result["errors"];
            session_destroy();
            Header('Location: ' . buildurl('index/index'));
        } else {

            //prepaire the emails
            $result = $model->result;
            $to = $result["holder"]["email"];
            $obj = new stdClass();
            $obj->date = date("d-m-Y H:i:s");
            $obj->booking_date = $result["booking_date"];
            $obj->supports_cancellation = $result["supports_cancellation"];
            $obj->supports_amendment = $result["supports_amendment"];
            $obj->payment_status = $result["payment_status"];
            $obj->non_refundable = $result["non_refundable"];
            $obj->booking_id= $result["booking_id"];
            $obj->booking_name= $result["booking_name"];
            $obj->booking_date = $result["booking_date"];
            $obj->number = $result["holder"]["phone_number"];
            $obj->check_in = $result["checkin"];
            $obj->check_out = $result["checkout"];
            //holder
            $obj->name = $result["holder"]["name"];
            $obj->surname = $result["holder"]["surname"];
            $obj->email = $result["holder"]["email"];
            //hotel    
            $obj->hotel_name = $result["hotel"]["name"];
            $obj->hotel_code = $result["hotel"]["hotel_code"];
            $obj->description = $result["hotel"]["description"];
            $obj->address = $result["hotel"]["address"];
            //booking item
            $obj->room_type = $result["hotel"]["booking_items"][0]["rooms"][0]["room_type"];
            $obj->no_of_rooms = $result["hotel"]["booking_items"][0]["rooms"][0]["no_of_rooms"];
            $obj->no_of_children = $result["hotel"]["booking_items"][0]["rooms"][0]["no_of_children"];
            $obj->no_of_adults = $result["hotel"]["booking_items"][0]["rooms"][0]["no_of_adults"];
            $obj->room_description = $result["hotel"]["booking_items"][0]["rooms"][0]["description"];
            $obj->currency = $result["hotel"]["booking_items"][0]["currency"];
            $obj->price = $result["hotel"]["booking_items"][0]["price"];
            $obj->cta_link = $seahorse->config->siteurl;
           
        
            useclass("Email", $data);
            $em = new Email();
            $em->SendEmail($to, 2, $obj);
            session_destroy();
    
        }
        $this->seo['title'] = 'Get a room :)';
        $this->seo['description'] = 'Our agent has been dispached to process your booking';
        $this->seo['keywords'] = '';
        return new ActionResult('ProcessBooking', $model, "layouts/resultlayout.php", $this->seo);
    }


    public function Cancel($id, $data)
    {
        global $seahorse;
        $config = new Config('1.0');
        $model = new stdClass();
        $body = [];
        include_once "./components/Headers.php";
        include_once "./components/Alert.php";
        $headers = new Headers();
        $model->headers = $headers->get();
        if (isset($data->booking_reference)) {
            $booking_reference = $data->booking_reference;
        }
        $body = [
            "comments" => $data->comments
        ];
        $url = "api/v3/hotels/bookings/" . $booking_reference;
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->delete($url, $model->headers, $body);

    if($model->result["status"] == "pending"){
        $url = "api/v3/hotels/bookings/" . $booking_reference;
        $api =  new ExternalAPIHandler("https://api-sandbox.grnconnect.com/");
        $model->result =  $api->Get($url, $model->headers);
    }

        if(isset($model->result["errors"])){
            $errors[] = $model->result["errors"][0]["messages"][0];
        }
        
        $obj = new stdClass();
        $obj->date = date("d-m-Y H:i:s");
       
        $obj->cancellation_status = $model->result["status"];
        $obj->cancellation_reference = $model->result["cancellation_reference"];
        $obj->cancellation_charge = $model->result["cancellation_charges"]["amount"];
        $obj->cancellation_date = $model->result["cancel_date"];
        $obj->booking_reference = $model->result["booking_reference"];
        $obj->currency = $model->result["booking_price"]["currency"];
        $obj->booking_price = $model->result["booking_price"]["amount"];
        $obj->booking_id = $model->result["booking_id"];
        $obj->cta_text = "Your have requested for your booking to be cancelled.";
       
        //  useclass("Email", $data);
        //  $em = new Email();
        //  $em->SendEmail($to, 3, $obj);
        $this->seo['title'] = 'Cancellations';
        $this->seo['description'] = 'We are sad to se you go, why dont you make another booking? :(';
        $this->seo['keywords'] = '';
        return new ActionResult('cancel', $model, "layouts/resultlayout.php",  $this->seo);
    }

public function CancelBooking()
{

return new ActionResult('cancelbooking',null, "layouts/resultlayout.php",  $this->seo );
}


}
