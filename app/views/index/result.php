<?php
// Results 
$search = $this->context->model;

if($search == null){
Header("Location: index.php");
$errors[] = 'OOPS!!! yours search did not yealid any result. please try using another key word';
}else{  
if(isset($search->errors)){
  $errors->message = $search->errors["messages"][0];
  $errors->code = $search->errors["code"];
  $hotels = null;
}else{
    $hotels = $search->result["hotels"];
}
?>
    <!-- <div class="container"> 
            <?php //include ('layouts/includes/search.php'); ?>
    </div> -->
<section>
    <div class="container">
        <div class="content">
        <div class="center"><?php include 'layouts/includes/alerts.php'; 
        ?>
        </div>
        <div class="tr-card ">
        <div class="p">
        <h1 class="gray"> <?= $hotels==null ?  "0" : count($hotels); ?> results</h1>
            <p class="gray mb-3">Here is a list of stays found in <b><?php echo  $search->search->location; ?></b>
                for your search</p>
           
        </div>
        </div>
        
           <?php
              if($hotels != null){
             foreach ($hotels as $hotel){   
            ?>
            <div class="card-container mt-3" id="result-splash">
                <div class="card" id="result-card">
                    <div class="card-container col3" id="result-card-container">
                        <div class="rel" id="result-rel">
                            <a
                                href="<?php url('booking/index?search_code='.$search->result["search_id"].'&hotel_code='.$hotel["hotel_code"].'')?>">
                                <img id="result-splash-img" src="<?php echo $hotel["images"]["url"]; ?>" >
                            </a>
                            <a href="">                     
                            </a>
                        </div>
                        <div id="result-card-description">
                            <div class="content">
                                <p class="primary bold"><?php echo $hotel["name"];?></p>
                                <ion-icon name="pin" title="<?=$hotel["address"]?>"></ion-icon>  &nbsp; <span class=""><?=$hotel["address"]?></span><br/>
                                    <div class=" small ellipsis gray" title=" <?php echo  $hotel['description'];?> ">
                                        <?php echo  $hotel['description'];?>                                       
                                    </div>
                                <br />
                                 <span class="primary">
                                 </span>
                            </div>
                        </div>
                        <div class="" id="result-card-button">
                            <div class="content text-right mt">
                                <span>
                                    <?php 
                                    if(isset($hotel["category"])){
                                        for ($i=0; $i < $hotel["category"]; $i++) { 
                                            echo  '<ion-icon name="star" class="orange"></ion-icon>' ;
                                        } 
                                    }
                                    ?>
                                </span>
                                <p class="bold mb dark"></p>
                                <p class=" brown bold">
                                    <?php echo  $hotel["min_rate"]["currency"] ." ". $hotel["min_rate"]["price"]; ?></p>
                                <p class="smaller gray"> for <?php if($hotel["min_rate"]["no_of_rooms"] > 1){ echo $hotel["min_rate"]["no_of_rooms"] . " " . "rooms";} else { echo $hotel["min_rate"]["no_of_rooms"] . " "  . "room";}?></p>
                                <button class="input myBtn" id=""><a class="white"
                                        href="<?php url('booking/index?search_code='.$search->result["search_id"].'&hotel_code='.$hotel["hotel_code"].'')?>">View
                                        More</a></button>
                                <span class="oi" data-glyph="icon-name" title="icon name" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } } ?>
        </div>
    </div>
    </div>
</section>
<?php
}
?>
