
<style>
    .input-container {
        display: grid !important;
        margin: 1em;
        /* width: 80%; */
    }
    textarea#message {
    margin: 0px 16px 16px;
    height: 227px;
    width: 787px;
    background: #e0dede33;
    }
</style>
<section id="contact">
    <div class="container">
        <h1 class="center gray">Contact Us</h1>
        <p class="gray center mb-3">Are you looking for a great destination to visit?</p>
        <div class="content card  p-3 bg-light">
            <form action="<?php url('contact')?>" method="Post" id="contact"> 
                <div class="input-container">
                    <label for="firstname" class="dark">First Name</label>
                    <input type="text" name="firstname" id="firstname" required="required">
                </div>
                <div class="input-container">
                    <label for="email" class="dark">Email</label>
                    <input type="email" name="email" id="lastname" required="required">
                </div>
                <div class="input-container">
                    <textarea name="message" id="message" cols="26" rows="10" class="input"
                        placeholder="type your message here..."></textarea>
                </div>

                <div class="input-container">
                    <label for="submit">&nbsp;</label>
                    <input type="submit" name="submit" id="submit" value="submit">
                </div>
            </form>
        </div>
    </div>
</section>