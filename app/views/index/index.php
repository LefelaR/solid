<?php

$area = $_SESSION["area"];
$cities = [];
?>

<header class="masthead">
    <div class="container">
        <div>
            <?php include('layouts/includes/search.php'); ?>
        </div>
    </div>
</header>

<div class="container">
    <div class="content">
        <section id="hotels">
            <div class="card-container col1-2">
                <div class="card row-2 menu vcenter">
                    <div class="img_capetown backdrop">
                    </div>
                    <a href="<?php url('result?location=cape town&rooms=1&adults=1&kids=0&ages=0'); ?>">
                        <h1 class="white bold">Cape Town</h1>
                    </a>
                </div>
                <div class="card menu vcenter">
                    <div class="img_egypt backdrop"></div>
                    <a href="<?php url('result?location= egypt&rooms=1&adults=1&kids=0&ages=0'); ?>">
                        <h1 class="white bold">Cairo</h1>
                    </a>
                </div>
                <div class="card menu vcenter">
                    <div class="img_lagos backdrop"></div>
                    <a href="<?php url('result?location=lagos&rooms=1&adults=1&kids=0&ages=0'); ?>">
                        <h1 class="white bold">Lagos</h1>
                    </a>
                </div>
            </div>
        </section>
    </div>

    <div class="content">
        <div class="center" id="logo-area">
            <div>
                <img src="<?php url('assets/img/logo.png'); ?>" alt="" id="logo">
            </div>
            <div class="p gray">
                <p class="left">
                    Solid Matter Travel is a leading wholesale travel company proving business and leisure travel services across
                    the African Continent. Our cost effective services for our customers, both efficient and reliable ensures our dependability.
                    With an extensive networks of travel suppliers across the continent,
                    Solid Matter travel ensures our clients’ travel needs are met in most of the major hubs in Africa. We are your travel
                    partner across the Continent.
                </p>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row mt-5">
            <div class="col3 mx-auto">
                <div class="favourites">
                    <a href="<?php url('result?location=Radisson Blu Hotel&rooms=1&adults=1&child=0&ages=0') ?>">
                        <img src="<?php url("/assets/img/hotels/325x250/1.jpg"); ?>" alt="">
                    </a>
                </div>
                <div>
                    <h3>Radisson Blu Hotel</h3>
                    <p class="smaller gray mb">Sandton Gauteng</p>
                </div>
            </div>
            <div class="col3 mx-auto">
                <div class="favourites">
                    <a href="<?php url('result?location=lagos&rooms=1&adults=1&child=0&ages=0') ?>">
                        <img src="<?php url("/assets/img/hotels/325x250/3.jpg"); ?>" alt="">
                    </a>
                </div>
                <div>
                    <h3>Protea Hotel</h3>
                    <p class="smaller gray mb">Sandton Gauteng</p>
                </div>
            </div>

            <div class="col3 mx-auto">
                <div class="favourites">
                    <a href="<?php url('result?location=Southern Sun Hotel&rooms=1&adults=1&child=0&ages=0') ?>">
                        <img src="<?php url("/assets/img/hotels/325x250/2.jpg"); ?>" alt="">
                    </a>
                </div>
                <div>
                    <h3>Southern Sun Hotel</h3>
                    <p class="smaller gray mb">Sandton Gauteng</p>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="row my-3">
                <div class="tr-card">
                    <div class="card-header">
                        <h3>Book your trip</h3>
                    </div>
                    <div class="card-body">
                        &nbsp;<br>
                        <p class="dark">
                            For your next African travel, book through our online site. Our passion for travel inspires us to reach millions of travelers across many African cities. So when it comes to booking the perfect hotel, vacation rental, resort, apartment, guest house or safari Lodge - we’ve got you covered. With hundreds of thousands of properties in South Africa and continental Africa, we provide incomparable choices for your next African adventure.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php include('layouts/includes/placespartial.php'); ?>

    </div>