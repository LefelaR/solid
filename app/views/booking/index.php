<?php
$model = $this->context->model;
$hotel = $model->result;
$image_list = $model->image_list;
?>
<section id="booking">
    <div class="container">
        <div class="content">
            <div class="tr-card">
<div class="card-header">
Hotel Information
</div>
<div class="card-body">
<h2 class="dark">
                <?php echo $hotel["hotel"]["name"]; ?> 
            </h2> 
            <span>
                <?php if(isset($hotel["hotel"]["category"])){ 
                            for ($i=0; $i < $hotel["hotel"]["category"]; $i++) { 
                                echo  '<ion-icon name="star" class="orange"></ion-icon>' ;
                            } 
                        }
                    ?>
            </span>
            <img src='https://www.countryflags.io/<?=$hotel["hotel"]["country"]?>/flat/24.png'>
            <p>
                <ion-icon name="pin" id="pin"></ion-icon>
                <?=$hotel["hotel"]["address"]?>
            </p>
</div>
            </div>
          
          
            <div>
                &nbsp;
            </div>
            <div class="slideshow-container">
                <?php 
                foreach ($image_list as $key => $image) {
            ?>
                <div class="mySlides fade">
                    <div class="numbertext">
                        <ion-icon name="camera"></ion-icon> <?=$key+1?>/<?=count($image_list)?>
                    </div>
                    <img src="<?php echo $image["url"] ?>" data-id=<?php echo $key;?> style="width:100%;">
                    <div class="text"></div>
                </div>
                <?php  } ?>
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
            </div>
            <br>
            <div style="text-align:center">
                <?php 
                foreach ($image_list as $key => $image) {
            ?>
                <img src="<?php echo $image["url"] ?>" class="booking-list-images-item center  dot"
                    data-id=<?php echo $key;?> onclick=SwitchImages()>
                <?php 
               }
            ?>
            </div>
            <div class="m">
                <h4 class="dark">Amenities</h4>
            </div>
            <div class="row m ellipsis mouse">
                <?php

           


                    if(isset($hotel["hotel"]["facilities"])){
                            $facList =  explode(';', $hotel["hotel"]["facilities"]);
                           foreach ( $facList as $key => $value )
                            echo '<p class="small-badge gray smaller dark">'.$value.'  </p>';
                    }else{
                         echo '<p class="gray smaller m"> There are no amenities listed for this property </p>';
                    }
                ?>
            </div>
            <hr class="separator">
            <div class="mt-3 bg-lite p">
                <p class="dark ellips" title="<?=$hotel["hotel"]["description"]?>">
                    <?=$hotel["hotel"]["description"]?>
                </p>
            </div>
            <div class=" tr-card mt-3">
                <div class="card-header">
                    <p class="upper dark"> Details for the booking you are looking for. </p>
                </div>
                <div class="card-body">
                    <div class="mx">
                        <div class="row  mobile-row">
                            <div class="col4  my-1">
                                <h5 class="gray"> check in date </h5>
                                <h4 class="primary">
                                    <?php
                        $cinDate = $hotel["checkin"];
                        $check_in =  date("M, d, Y", strtotime( $cinDate));
                        echo $check_in;
                        ?></h4>
                            </div>
                            <div class="col4 my-1">
                                <h5 class="gray">
                                    checkout date
                                </h5>
                                <h4 class="primary">
                                    <?php
                                    $coutDate = $hotel["checkout"];
                                    $check_out =  date("M, d, Y", strtotime( $coutDate));
                                    echo $check_out;
                                    ?>
                                </h4>
                            </div>
                            <div class="col4 my-1">
                                <h5 class="gray">
                                    number of guests
                                </h5>
                                <h4 class="primary">
                                    <?php if (isset($hotel["no_of_adults"])){
                                echo $hotel["no_of_adults"];
                                }?> Adults
                                </h4>
                            </div>
                            <div class="col4 my-1">
                                <h5 class="gray">
                                    Number of rooms
                                </h5>
                                <h4 class="primary">
                                    <?php if (isset($hotel["no_of_rooms"])){
                                echo $hotel["no_of_rooms"];
                                }?> Rooms
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tr-card mt">
            <div class="card-header">
                <p class="upper dark">Room List</p>
            </div>    
            <div class="card-body">

                    <?php 
                    $p = $hotel["hotel"]["rates"];

                    $p = array_slice($p, 0,10);
                    foreach ($p as $key => $value) {
                    ?>
                    <div class="row  mobile-row m  card">
                        <div class="col4 col6 ">
                            <div class="m">
                                <p class="dark bold  bigger" style="display: contents">
                                    <?php echo $value["rooms"][0]["room_type"]?>
                                </p>
                            </div>
                        </div>
                        <div class="col4 col6">
                            <div class="m">
                                    <?php
                                        if ($value["non_refundable"] == true){
                                            echo '<p class="smaller brown bold"> Non-Refundable</p>';
                                        }
                                        if ($value["non_refundable"] == false){
                                            echo '<p class="smaller succcess bold"> Supports Cancellation </p>';
                                        }
                                    ?>
                            </div>
                        </div>


                        <div class="col4 col6">
                            <div class="m">

                                <?php  if(isset($value["boarding_details"][0])){
                                      echo  '<p class="smaller text-success ">'. $value["boarding_details"][0] .'</p>' ; 
                                 } ?>

                            </div>
                        </div>


                        <div class="col4 col6">
                            <div class="right center">
                                <p class="brown bold">
                                    <h2 class="brown">
                                    <?php echo   $value["currency"];?>
                                     <?php echo  $value["price"]; ?>
                                    </h2>
                                </p>
                                <p class="mt ">
                                                    
                                <a href="<?php url("Booking/reserve?rate_key=".$value["rate_key"]."&group_code=".$value["group_code"]."&sid=".$hotel["search_id"]."&check_in=".$hotel["checkin"]."&number_of_rooms=".$value["no_of_rooms"]."&checkout=".$hotel["checkout"]."&room_reference=".$value["rooms"][0]["room_reference"].   " ")  ;?>"
                                    class=" btn ">
                                    Reserve
                                </a>
                                </p>
                            </div>
                        </div>
                        <hr class="separator">
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <?php include('layouts/includes/placespartial.php'); ?>
    </div>
</section>
<?php include('layouts/includes/map_modal.php')?>
<script>
    var slideIndex = 1;
    showSlides(slideIndex);
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
    }
</script>