<?php
 $model = $this->context->model;
 $result = $model->result;
?>

<div class="container">
    <div class="content">
    <?php include 'layouts/includes/alerts.php'; ?>

    <div class="tr-card m-3">
            <div class="card-header">
                Cancellation
            </div>
            <div class="card-body">

                    <div class="center my-5">
                      <div class="info mt-3">
                            <span class="bold my dark smaller"> Booking Id Number:</span> <span class="my gray"> <?php echo $result["booking_id"] ?> <br />
                            <span class="bold my dark smaller">Cancellation Status:</span> <span class="my gray">  <?php echo $result["status"] ?><br />
                            <span class="bold my dark smaller">Booking Cancellation Reference:</span> <span class="my gray">  <?php echo $result["cancellation_reference"] ?> <br />
                            <span class="bold my dark smaller">Booking Cancellation Comments</span> <span class="my gray"> <?php if (isset($result["cancellation_comments"])){
                                    echo $result["cancellation_comments"];
                                } ?></span><br />
                                <span class="bold my dark smaller">Cancellation Date:</span> <span class="my gray">  <?php echo $result["cancel_date"] ?></span><br />
                                <span class="bold my dark smaller">Booking Reference:</span> <span class="my gray">  <?php echo $result["booking_reference"] ?></span><br />
                                <span class="bold my dark smaller">Cancelation Charges:</span> <span class="my gray">  <?php echo $result["cancellation_charges"]["currency"]." ".$result["cancellation_charges"]["amount"] ?></span><br />
                      </div>
                <div class="my-3">
                    <a href="<?php url("index/index")?>" class="butn-default btn">search</a>
                </div>
                                
                    </div>

                </div>
        </div>
    </div>
</div>