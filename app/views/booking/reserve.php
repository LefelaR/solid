<?php
$model = $this->context->model;
$hotel = $model->result;
$room_reference = $model->room_reference;
if ($hotel == null) {      
               header("Location:index.php");
}
?>
<div class="container">
<div class="content">
<div class="center"><?php include 'layouts/includes/alerts.php'; 
?></div>
<form action="<?php url('Booking/ProcessBooking'); ?>" method="Post" id="reserve" name="reserve" class="form">
<?php
if(!empty($model->errors)){
        echo ($model->errors);
}
?>
<div class="tr-card mt-3">
<div class="card-header">
      <p class="upper  dark"> Your Information</p>
</div>
                                <div class="card-body">
                                        <div class="row">
                                                <div class="col12">
                                                        <label for="title_holder">Title</label><br />
                                                        <select name="title_holder" id="title_holder" class="input"
                                                                require="required" style="width:46%;">
                                                                <option value="">- -</option>
                                                                <option value="Mr.">Mr</option>
                                                                <option value="Ms.">Ms</option>
                                                        </select>
                                                </div>
                                                <div class="col6">
                                                        <label for="name_holder">Name</label>
                                                        <input type="text" class="input" name="name_holder"
                                                                id="name_holder" require="required">
                                                </div>
                                                <div class="col6">
                                                        <label for="surname_holder">Surname</label>
                                                        <input type="text" class="input" name="surname_holder"
                                                                id="surname_holder" require="required">
                                                </div>
                                                <div class="col6">
                                                        <label for="email_holder">Email</label>
                                                        <input type="email_holder" name="email_holder" id="email_holder"
                                                                class="input" require="required">
                                                </div>
                                                <div class="col6">
                                                        <label for="number_holder">Phone</label>
                                                        <input type="tel" name="number_holder" id="number_holder"
                                                                class="input" require="required">
                                                </div>
                                        </div>
                                </div>
                                <div class="card-footer">
                                </div>
                        </div>
                        <div class="tr-card mt-3">
                                <div class="card-header">
                                        <p class="upper "> Booking Confirmation </p>
                                </div>

                                <div class="card-body">
                                        <h5 class="gray my">
                                                You are requireds to give the information of the people who will occupy
                                                each room.
                                        </h5>
                                        <?php
                                        if (isset($hotel["hotel"]["rate"]["rooms"])) {

                                                $rooms = $hotel["hotel"]["rate"]["rooms"];

                                                foreach ($rooms as $key => $value) {
                                        
                                        ?>
                                        <div class="col12 mt">
                                                <div class="alert alert-info" role="alert">
                                                        <p class="m bold dark ">Room <?= $key + 1 ?></p>
                                                </div>
                                                <?php
                                                        if ($value["no_of_adults"] > 0) {
                                                                $ad = $value["no_of_adults"];
                                                                for ($a = 0; $a < $ad; $a++) {
                                                                        $b = $a+1;
                                                                        echo ' 
                                                                        <div class="col12">
                                                                         <p class="smaller m bold gray"> Adult '.$b.'</p>
                                                                                <label for="title>" class="smaller gray">Title</label><br />
                                                                                 <select name="title" id="title" class="input" require="required" style="width:46%;">
                                                                      
                                                                                         <option value="">- -</option>
                                                                                         <option value="Mr.">Mr</option>
                                                                                         <option value="Ms.">Ms</option>
                                                                                         </select>
                                                                        </div>
                                                                      
                                                                        <div class="col6">
                                                                                 <label for="name>" class="smaller gray">Name</label>
                                                                                  <input type="text" name="name" id="name" class="input" required="required">
                                                                        </div>
                                                                        <div class="col6">
                                                                                 <label for="surname"  class="smaller gray">Surname</label>
                                                                                 <input type="text" name="surname" id="surname" class="input"
                                                                                 required="required">
                                                                        </div>
                                                                        <div class="col6">
                                                                                 <input type="text" class="input readonly" name="type_adult"
                                                                                value="AD"  hidden>
                                                                        <input type="text" name="adultarray" id="adultarray" value="ArAd" hidden="hidden" readonly>
                                                                        </div> ';
                                                                }
                                                        }
                                                        ?>

                                                <?php
                                                        if ($value["no_of_children"] > 0) {
                                                                $ch = $value["no_of_children"];
                                                                for ($i = 0; $i < $ch; $i++) {
                                                                        $c = $i+1;
                                                                        echo ' <div class="col12">   
                                                                        <p class="smaller m bold gray"> child '.$c.'</p>

                                                                        <div class="col12">
                                                                        <label for="title_child" class="smaller gray">Title</label><br />
                                                                        <select name="title_child" id="title_child" class="input" require="required" style="width:46%;">
                                                                        
                                                                        <option value="">- -</option>
                                                                        <option value="Mr.">Mr</option>
                                                                        <option value="Ms.">Ms</option>
                                                                        </select>
                                                                        </div>
                                                                        <div class="col6">
                                                                        <label for="name" class="smaller gray">Name</label>
                                                                        <input type="text" name="name_child" class="input" required="required">
                                                                        </div>
                                                                        <div class="col6">
                                                                        <label for="name" class="smaller gray">Surname</label>
                                                                        <input type="text" name="surname_child" class="input"
                                                                        required="required">
                                                                        </div>
                                                                        <div class="col6">

                                                                        <input type="text" class="input readonly" name="type_child"
                                                                        value="CH"  hidden>
                                                                        </div>

                                                                        <div class="col6">
                                                                        <input type="text" class="input readonly"name="age"
                                                                        value="' . $value["children_ages"][$i] . '" hidden>
                                                                        <input type="text" name="childrenarray" id="childrenarray" value="ArCh" hidden="hidden" readonly>
                                                                        </div>
                                                                        </div>
                                                                        ';
                                                                                                                                       }
                                                        }
                                                        ?>
                                                <div>
                                                        <input type="text" name="room_reference_<?= $key ?>"
                                                                value="<?php echo $hotel["hotel"]["rate"]["rooms"][$key]["room_reference"]; ?>"
                                                                hidden="hidden">
                                                </div>
                                        </div>
                                        <?php 
                                         } 
                                        }
                                 ?>
                                </div>
                        </div>

                        <div class="tr-card mt-3 pb-3">
                                <div class="card-header">
                                        <p class="upper">Booking Details</p>
                                </div>
                                <div class="card-body">
                                        <div class="row">
                                                <p class="gray mx-auto  m-3">
                                                        Please ensure that your reservation information is correct
                                                        before you make your booking.
                                                </p>
                                        </div>

                <div class="col6">
                        <p class="bold gray p">check in</p>
                        <input type="text" class="input readonly" name="check_in" value="<?php  
                                if(isset($model->Checkin)){
                                        echo $model->Checkin; 
                                }
                                ?>" readonly>
                </div>

                <div class="col6">
                        <p class="bold gray p">check out</p>
                        <input type="text" class="input readonly" name="check_out" value="<?php 
                                if(isset($model->Checkout)){
                                echo $model->Checkout; 
                                }
                                ?>" readonly>
                </div>
             <div class="row">
                        <div class="col3 mx-auto">
                                <span class="">
                                        <img src="<?php if(isset($hotel["hotel"]["images"]["url"])){
                        echo $hotel["hotel"]["images"]["url"]; } ?>" alt="img" style="width:14em; object-fit:cover;">
                                </span>
                        </div>

                        <div class="col3 mx-auto">
                                <p class="bold gray m">
                                        Hotel Name
                                </p>
                                <p class="dark m">
                                        <?php 
                                if(isset($hotel["hotel"]["name"])){       
                                echo $hotel["hotel"]["name"];  }?>

                                </p>
                        </div>
                        <div class="col3 mx-auto">
                                <p class="bold gray m">
                                        Address
                                </p>
                                <p class="text-justify dark m">
                        <?php
                                if(isset($hotel["hotel"]["address"])){
                                         echo $hotel["hotel"]["address"];
                                }
                                ?>
                                </p>
                        </div>
                                        </div> 
                                        <div class="col6 ">
                                                <p class="bold gray mx">
                                                        Hotel Category
                                                </p>
                                                <p class="smaller dark mx">
                                                        <?php if (isset($hotel["hotel"]["category"])) {
                                                                for ($i = 0; $i < $hotel["hotel"]["category"]; $i++) {
                                                                        echo  '<ion-icon name="star" class="orange"></ion-icon>';
                                                                }
                                                        }
                                                        ?>
                                                </p>
                                        </div>
                                        <div class="col6 ">
                                                <p class="bold gray mx">
                                                        Price
                                                </p>
                                                <p class="bold dark py mx">
                                                        <?php
                                                if (isset($hotel["hotel"]["rate"]["price"])) {       
                                                        echo $hotel["hotel"]["rate"]["currency"] . " " . $hotel["hotel"]["rate"]["price"]; 
                                                 }
                                                 ?>
                                                </p>
                                        </div>
                                        <div class="row card">
                                                <p class="bold gray mx p">
                                                        Rate comments
                                                </p>
                                                <p class=" dark py mx ">
                                                        <?php if(isset($hotel["hotel"]["rate"]["rate_comments"])) {
                                                        echo $hotel["hotel"]["rate"]["rate_comments"]["comments"]; }?>
                                                </p>
                                        </div>
                                        <input type="text" class="input" hidden="hidden" name="rate_key" value="<?php 
                                                if(isset($hotel["hotel"]["rate"]["rate_key"])){
                                                echo $hotel["hotel"]["rate"]["rate_key"];
                                                } ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="room_code" value="<?php 
                                                 if(isset($hotel["hotel"]["rate"]["room_code"])){
                                                echo $hotel["hotel"]["rate"]["room_code"]; 
                                                }
                                                 ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="group_code" value="<?php 
                                                 if(isset($hotel["hotel"]["rate"]["group_code"])){
                                                echo $hotel["hotel"]["rate"]["group_code"];
                                                 } ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="payment_type" value="<?php 
                                                 if(isset($hotel["hotel"]["rate"]["payment_type"][0])){
                                                echo $hotel["hotel"]["rate"]["payment_type"][0]; 
                                                } 
                                                ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="city_code" value="<?php 
                                                 if(isset($hotel["hotel"]["city_code"])){
                                                echo $hotel["hotel"]["city_code"];  
                                                }
                                                 ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="hotel_code" value="<?php 
                                                 if(isset($hotel["hotel"]["hotel_code"])){
                                                echo $hotel["hotel"]["hotel_code"]; }
                                                 ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="search_id" value="<?php 
                                                 if(isset($hotel["search_id"])){
                                                echo $hotel["search_id"]; 
                                                } 
                                                ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="room_reference" value="<?php 
                                                 if(isset($room_reference)){
                                                echo $room_reference; } ?>" readonly>
                                        <input type="text" class="input" hidden="hidden" name="no_of_rooms" value="<?php 
                                                 if(isset($hotel["hotel"]["rate"]["no_of_rooms"])){
                                                echo $hotel["hotel"]["rate"]["no_of_rooms"]; 
                                                } ?>" readonly>
                                </div>
                        </div>
                        <div class="tr-card mt-3">
                                <div class="card-header">
                                        <p class="upper "> cancellation policy</p>
                                </div>
                                <div class="card-body">

                                        <?php
                         if(isset($hotel["hotel"]["rate"]["non_refundable"])){
                                if( $hotel["hotel"]["rate"]["non_refundable"] == true ){?>
                                        <div class="m card">
                                                <p class="p  brown">
                                                        This rate is non-refundable and cannot be changed or cancelled.
                                                        If you do choose to change or cancel this booking you will not
                                                        be refunded any of the payment.
                                                </p>
                                        </div>
                                        <div class="m">
                                                <p class="gray smaller">
                                                        All dates of special conditions are based on india time. Please
                                                        consider local time difference and allow extra time where
                                                        applicable. </p>
                                        </div>
                                        <div class="custom-control card p">
                                                <input type="checkbox" id="cpolicy" name="cpolicy"
                                                        onclick="terms_change(this)">
                                                <p class="gray smaller">I agree that i have read, understood, and accept
                                                        the <A>Terms and
                                                                conditions</A>. the important notes and the cancellation
                                                        policy. </p>
                                        </div>
                                        <div>

                                                <textarea name="comments" class="my-1" id="comments" cols="30" rows="5"
                                                        placeholder="Enter remarks..."></textarea>
                                        </div>
                                        <div>
                                                <input type="submit" name="submit" id="reserveBtn" value="Book"
                                                        class="primary gray" hidden="hidden">
                                        </div>

                                        <?php
                                        }else{
                                        ?>
                                        <div class="m">
                                                <div class="col6 ">
                                                        <p class="dark smaller bold p">Room type</p>

                                                        <p class="gray p"> <?php if(isset($hotel["hotel"]["rate"]["rooms"][0]["room_type"])){
                                                        echo   $hotel["hotel"]["rate"]["rooms"][0]["room_type"];
                                                       }?></p>
                                                </div>

                                                <div class="col6 ">
                                                        <p class="dark smaller bold p"> Rate Type</p>
                                                        <p class="gray p"><?= $hotel["hotel"]["rate"]["rate_type"]; ?>
                                                        </p>
                                                </div>
                                                <div class="row">
                                                        <?php
        
                                                if($hotel["hotel"]["rate"]["cancellation_policy"]['under_cancellation'] == true){
                                                        ?>
                                                        <div>
                                                                <p class="gray  bold  p">This booking is under
                                                                        cancellation and will attract canellation
                                                                        charges</p>
                                                        </div>
                                                        <?php  }else{ ?>
                                                        <div>

                                                                <?php
                                                            if(isset($hotel["hotrel"]["rate"]["cancellation_policy"]["cancel_by_date"])){
                                                          
                                                                echo '<p> Free cancellation until ' . $hotel["hotrel"]["rate"]["cancellation_policy"]["cancel_by_date"] . ' </p> <br/>

                                                                <p>
                                                                If you change or cancel your booking after ' . $hotel["hotrel"]["rate"]["cancellation_policy"]["cancel_by_date"] . ', you will be charged for 1 night (including tax)
                                                                        We will not be able to refund any payment for no-shows or early check-out.
                                                                </p>';
                                                            }
                                                    ?>
                                                        </div>
                                                        <?php
                                                 } ?>
                                                </div>

                                                <div class="row mt-3 card p">
                                                        <p class="smaller dark">
                                                                <?=$hotel["hotel"]["rate"]["rate_comments"]["comments"]?>
                                                        </p>
                                                </div>
                                        </div>
                                        <div class="m">
                                                <p class="gray smaller">
                                                        All dates of special conditions are based on india time. Please
                                                        consider local time difference and allow extra time where
                                                        applicable. </p>
                                        </div>
                                        <div class="custom-control card p">

                                                <p class="smaller gray">

                                                </p>

                                                <input type="checkbox" id="cpolicy" name="cpolicy"
                                                        onclick="terms_change(this)">
                                                <p class="gray smaller">I agree and understand <a
                                                                href="<?php url('index/terms');?>">Terms and
                                                                conditions</a> & the important notes and the
                                                        cancellation
                                                        policy. </p>
                                        </div>
                                        <div>
                                                <label for="comments" class="mt-1 gray ">Comment</label>
                                                <textarea name="comments" class="my-1" id="comments" cols="30" rows="5"
                                                        placeholder="write your comments here..."></textarea>
                                        </div>
                                        <div>
                                                <input type="submit" name="submit" id="reserveBtn" value="Book"
                                                        class="primary gray" hidden="hidden">
                                        </div>
                                        <?php }
                                }?>
                                </div>
                        </div>
                </form>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                        <p>Some text in the modal.</p>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                </div>
                        </div>
                </div>
        </div>
</div>
<script>

var al = document.querySelectorAll("div.alert.bg-danger");
if(al.length > 0){
        debugger

}


</script>