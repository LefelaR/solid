<?php
 $model = $this->context->model;
 $result = $model->result;
?>
<div class="container">
        <div class="content">
                <div class="">
                        <div class="mx-auto mt-5">
                                <div class="tr-card mt-3">
                                        <div class="card-body">
                                                <div class="my-3">
                                                        <h4 class="gray">Your booking has succesfully been processed
                                                        </h4>
                                                </div>
                                                <div class="logo">
                                                        <img src="<?php url("assets/img/traderoutes_logo.jpg") ?>"
                                                                alt="" style="width: 10em;">
                                                </div>
                                                <div class="info mt-3">
                                                        <h3 class="dark"><?php echo $result["hotel"]["name"]?></h3>
                                                        <p class="my gray"><?php echo $result["hotel"]["address"]?></p>
                                                        <p class="smaller gray">
                                                                <?php echo $result["hotel"]["description"] ?></p>
                                                        <div class="wrap my">
                                                                <h3 class="dark"> </h3>
                                                                <span class="bold my dark smaller">Hotel Category
                                                                        :</span> <span class="my gray">

                                                                        <?php if(isset($result["hotel"]["category"])){ 
                                                                                for ($i=0; $i < $result["hotel"]["category"]; $i++) { 
                                                                                        echo  '<ion-icon name="star" class="orange"></ion-icon>' ;
                                                                                } 
                                                                                }
                                                                        ?>
                                                                        <br />
                                                                        <span class="bold my dark smaller">Phone Number
                                                                                :</span>
                                                                        <span class="my gray">
                                                                                <?php echo $result["holder"]["phone_number"]?></span><br>
                                                                        <span class="bold my dark smaller">Check In
                                                                                :</span>
                                                                        <span class="my gray">
                                                                                <?php echo $result["checkin"] ?>
                                                                        </span><br>
                                                                        <span class="bold my dark smaller">Check Out
                                                                                :</span>
                                                                        <span class="my gray">
                                                                                <?php echo $result["checkout"] ?></span><br>
                                                                        <span class="bold my dark smaller">Number Of
                                                                                Adults
                                                                                :</span> <span class="my gray">

                                                                                <?php echo $result["hotel"]["booking_items"][0]["rooms"][0]["no_of_adults"] ?>

                                                                        </span>
                                                                        <br>
                                                                        <span class="bold my dark smaller"> Booking
                                                                                Price
                                                                                :</span> <span class="my gray">

                                                                                <?=$result["hotel"]["booking_items"][0]["currency"][0]?>  <?=$result["price"]["total"]; ?>                    
                                                                                </span><br>
                                                        </div>
                                                </div>
                                                <div class="confirmation_info">
                                                        <h3 class="dark">Conformation Details</h3>
                                                        <span class="bold my dark smaller">room_type:</span> <span
                                                                class="my gray"><?php echo $result["hotel"]["booking_items"][0]["rooms"][0]["room_type"]?>
                                                        </span> <br>
                                                        <span class="bold my dark smaller"><?php if (isset($result["hotel"]["booking_items"][0]["rooms"][0]["room_reference"])){echo 'Room_reference:';}?>
                                                        </span> <span class="my gray">
                                                                <?php if(isset($result["hotel"]["booking_items"][0]["rooms"][0]["room_reference"])){
                                                                         echo $result["hotel"]["booking_items"][0]["rooms"][0]["room_reference"];}?>
                                                        </span> <br>
                                                        <span class="bold my dark smaller">Description:</span> <span
                                                                class="my gray"><?php echo $result["hotel"]["booking_items"][0]["rooms"][0]['description']?>
                                                        </span> <br>
                                                        <span class="bold my dark smaller">Number of Rooms:</span> <span
                                                                class="my gray"><?php echo $result["hotel"]["booking_items"][0]["rooms"][0]["no_of_rooms"] ?>
                                                        </span> <br>
                                                        <span class="bold my dark smaller">Payment Status:</span> <span
                                                                class="my gray"><?php echo $result["payment_status"] ?></span>
                                                        <br>
                                                </div>
                                                <div class="booking_info">
                                                        <h3 class="dark mt">Booking Information</h3>
                                                </div>
                                                <div class="guest_info">
                                                        <span class="bold my dark smaller">Booking Code:</span> <span
                                                                class="my gray"><?php echo $result["booking_name"]; ?>
                                                        </span> <br>
                                                      
                                                        <span class="bold my dark smaller">Booking Reference:</span>
                                                        <span class="my gray"><?php echo $result["booking_reference"] ?>
                                                        </span> <br>
                                                        <span class="bold my dark smaller">Booking Date:</span> <span
                                                                class="my gray">
                                                                <?php echo  date($result["booking_date"])  ?></span>
                                                        <br>
                                                </div>
                                                <div class="notes">
                                                        <h3 class="dark mt">Important Note</h3>
                                                </div>
                                                <div class="cancellation my">
                                                        <form action="<?php url("booking/Cancel");?> " method="Post">
                                                                <p class="dark gray">
                                                                        <input type="text" name="booking_reference"
                                                                                value="<?php echo $result["booking_reference"]; ?>"
                                                                                id="booking_reference" hidden="hidden">

                                                                        <textarea name="comments" id="comments"
                                                                                cols="30" rows="5"></textarea>
                                                                        <input type="submit" name="submit"
                                                                                value="cancel Booking"
                                                                                class="btn-background">
                                                                        <a href="<?php url("index/index")?>"
                                                                                class="butn-default btn"> Home </a>
                                                                </p>
                                                        </form>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<?php
        // $obj = ob_clean();
?>