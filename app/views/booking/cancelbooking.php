<div class="container">
<div class="content">
    <div class="tr-card mt-5">
            <div class="card-header">
                cancel a booking
            </div>
            <div class="card-body">
                <form action="<?php url("booking/Cancel");?>" method="POST" id="cancel_booking"> 
               <div class="row">
               <div class="center"><?php include 'layouts/includes/alerts.php'; 
                 ?></div>
               </div>
               <div class="my-3">
                        <p class="gray">
                    To cancel a booking you will need  to have a valid booking reference provided to you on the time of the booking.</p>
                    </div>
                    <div class="form-group">
                        <label for="booking_reference">Your booking reference</label>
                        <input type="text" name="booking_reference" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                    <input type="submit" name="submit" value="cancel Booking" class="btn-background">
                    </div>
                </form>
            </div>
    </div>
</div>
</div>
<script>

    var cb = document.forms["cancel_booking"];
        cb.addEventListener("submit", function(event){
            debugger
        var result = valForm();
        if(result == true){
            showDontPanic();
            processTrans();
        }else{
       event.stopPropagation();
       event.preventDefault();
        }
        });





        function valForm(){
      var bref = document.forms["cancel_booking"]["booking_reference"];
       var br = bref.value;
       if (br == "") {
           bref.classList.add("validate-danger");
           ShowAlert( "error" , "you must provide a booking reference");
           bref.focus();
           return false;
         }
         return true;
        }


function processTrans(){
    
}


    
</script>