-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 01:15 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ria-web`
--

-- --------------------------------------------------------

--
-- Table structure for table `message_template`
--

CREATE TABLE `message_template` (
  `message_template_id` int(11) NOT NULL,
  `event_type` int(225) NOT NULL,
  `priority` int(11) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `body` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_template`
--

INSERT INTO `message_template` (`message_template_id`, `event_type`, `priority`, `subject`, `body`) VALUES
(1, 1, 1, 'Welcome to Nova', '<p>Welcome to Nova</p> <p>Your account has been successfully created and you can log onto your <br /> dashboard using your email and password. <br /><br /></p>'),
(2, 2, 2, 'RE:: Nova - Forgotten Password Request ', '<p>A password change was requested at  {date}</p><p>Follow the link to reset your password <a href=\" {cta_link}\">Reset</a></p><p>If this was not you, please change your password immediately.</p> '),
(3, 3, 2, 'RE:: Nova - An inspection was shared with you ', '<p>A inspection was recently shared with you </p> <p>To view the report please follow the link below: </p>  '),
(4, 4, 2, 'RE:: Routes Into Africa - Contact Request Received ', ' <p>Hi  {user_name}, your contact request has been recieved and we will be contacting you  soon.  </p>'),
(5, 5, 2, 'RE:: Nova - Login Activity Detected ', '<p>A new signin was detected at  {date}</p> <p>If this was not you, please change your password immediately.</p> '),
(6, 6, 2, 'RE:: Nova - Your password has been changed ', '<p>Your password has changed at  {date}</p><p>If this was not you, please contact us immediately.</p> '),
(7, 7, 2, 'RE:: Nova - You have been added to  (group_name} ', '<p>Welcome to Nova</p><p>You have been added to  {group_name}. You will not be able to login until you have been approved by an Administrator of  (group_name}. '),
(8, 8, 2, 'RE:Nova -  You have been removed from  (group_name} ', ' <p>Your membership from the group  {group_name} has been revoked</p><p>Dont worry, your profile lives on </p> '),
(9, 9, 2, 'RE:: Nova - Permissions revoked for invoice:   (invoice_ref} ', '<p>Viewing permissions for invoice  {invoice_ref} has been revoked.</p> <p>If this was in error please contact the administrator or contact of your team for assistance</p> '),
(10, 10, 2, 'RE:: Nova - An invoice was shared with you:  (invoice_ref} ', '<p>A invoice was recently shared with you </p> <p>To view the invoice please follow the link below: </p>  '),
(11, 11, 2, 'RE:: Nova - Permissions revoked for inspection report:  (address} ', '<p>Viewing permissions inspection report with address ( (address}) has been revoked.</p> <p>If this was in error please contact the administrator or contact of your team for assistance</p> '),
(12, 12, 1, 'RE:: Nova - I am arriving soon for an inspection of  (address} ', '<p>I will be arriving soon for an inspection scheduled at  (booking_start} at  (address}. </p> '),
(13, 13, 1, 'RE:: Nova - I have arrived for an inspection of  (address} ', '<p>I have arrived for an inspection scheduled at  (booking_start} at  (address} </p> '),
(14, 14, 2, 'RE:: Nova - A scheduled inspection for  (address} will start soon. ', '<p>A scheduled inspection for  (address} will start at  (booking_start}.</p><p>The inspector will contact you soon</p> '),
(16, 15, 2, 'RE:: Nova - An quote was shared with you:  (quote_ref} ', '<p>A quote was recently shared with you </p> \n<p>To view the quote please follow the link below: </p>  '),
(17, 16, 2, 'RE:: Nova - You have been successfully subscribed ', '<p>As per your subscription, you will have been added to our Newsletter mailing list </p> '),
(18, 17, 2, 'RE:: Nova - You have requested to get our newsletter ', '<p> A newsletter will be sent to you as from our next publication.</p> '),
(19, 18, 2, 'RE:: Nova - You have requested to terminate your subscription ', '<p> Your Subscription with NOVA has been terminated on  (date}.</p> '),
(20, 15, 2, 'RE:: Nova - An quote was shared with you:  (quote_ref} ', '<p>A quote was recently shared with you </p> \n<p>To view the quote please follow the link below: </p> ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message_template`
--
ALTER TABLE `message_template`
  ADD PRIMARY KEY (`message_template_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message_template`
--
ALTER TABLE `message_template`
  MODIFY `message_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
