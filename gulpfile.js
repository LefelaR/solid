var gulp = require('gulp');
var sass = require('gulp-sass');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require('gulp-autoprefixer');
var pkg = require('./package.json');
var browserSync = require('browser-sync').create();
var regex = require('regex');

// Set the banner content
var banner = ['/*!\n',

    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Parts Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * parts under license <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
    ' */\n',
    '\n'
].join('');




// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function() {

    // Bootstrap
    gulp.src([
            './node_modules/bootstrap/dist/**/*',
            '!./node_modules/bootstrap/dist/css/bootstrap-grid*',
            '!./node_modules/bootstrap/dist/css/bootstrap-reboot*'
        ])
        .pipe(gulp.dest('./app/vendor/bootstrap'))

    // Font Awesome 5
    gulp.src([
            './node_modules/fontawesome-free/**/*'
        ])
        .pipe(gulp.dest('./app/vendor'))

    // jQuery
    gulp.src([
            './node_modules/jquery/dist/*',
            '!./node_modules/jquery/dist/core.js'
        ])
        .pipe(gulp.dest('./app/vendor/jquery'))


    // datepicker

    gulp.src([
            './node_modules/flatpickr/dist/*'
        ])
        .pipe(gulp.dest('./app/vendor/flatpickr'))

    // jQuery Easing
    gulp.src([
            './node_modules/jquery.easing/*.js'
        ])
        .pipe(gulp.dest('./app/vendor/jquery-easing'))



    // Simple Line Icons
    gulp.src([
            './node_modules/simple-line-icons/fonts/**',
        ])
        .pipe(gulp.dest('./app/vendor/simple-line-icons/fonts'))

    gulp.src([
            './node_modules/simple-line-icons/css/**',
        ])
        .pipe(gulp.dest('./app/vendor/simple-line-icons/css'))

    gulp.src([
            './node_modules/tocbot/css/**',
        ])
        .pipe(gulp.dest('./app/vendor/tocbot/css'))

});

// Compile SCSS
gulp.task('css:compile', function() {
    return gulp.src('./scss/**/*.scss')

    .pipe(sass.sync({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(
            rename({
                // rename app.css to icons.min.css
                suffix: ".min"
            })
        )

    .pipe(gulp.dest('./app/css'))

});

// Minify CSS
gulp.task('css:minify', ['css:compile'], function() {
    return gulp.src([
        './app/css/*.css',
        '!./app/css/*.min.css'
    ])

    .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))


    .pipe(browserSync.stream());
});

// CSS
gulp.task('css', ['css:compile', 'css:minify']);

// Minify JavaScript
gulp.task('js:minify', function() {


    // // iignition
    // gulp.src([
    //   './app/vendor/iignition/iignition.js'
    // ])

    //   .pipe(rename({
    //     suffix: '.min'
    //   }))
    //   .pipe(uglify())
    //   .pipe(gulp.dest('./app/vendor/iignition'))

    return gulp.src([
            './app/js/*.js',
            '!./app/js/*.min.js'
        ])
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(gulp.dest('./app/js'))
        .pipe(browserSync.stream());
});

// JS
gulp.task('js', ['js:minify']);


// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// Dev task
gulp.task('dev', ['css', 'js', 'browserSync'], function() {
    gulp.watch('./scss/*.scss', ['css']);
    gulp.watch('./app/js/*.js', ['js']);
    gulp.watch('./app/*.html', browserSync.reload);
});


// gulp.task('calendar' , function(){

//   return new Promise(function (resolve, reject) {

//       gulp.src([
//         'node_modules/@fullcalendar/**/*'
//       ])

//       .pipe(gulp.dest('./app/vendor/calendar'))

//       gulp.src([
//         'app/views/admin/calendar/**/*'
//       ])
//       .pipe(gulp.dest('./app/vendor/dest'))


//     });
// });





gulp.task("build-server-api", function() {

    var file = 'lucidocean-web.postman_collection.json';
    var files = [
        ['lucidocean-web.postman_collection.json', 'serverapi.json'],
        ['prop.postman_collection.json', 'propapi.json']
    ];
    //var file = process.argv[3];

    for (var i = 0; i < files.length; i++) {
        var item = files[i];
        var file = item[0];
        var outputfile = item[1];

        var fs = require('fs'), // needed to read JSON file from disk
            pm = require('postman-collection'),
            Request = pm.Request,
            Collection = pm.Collection,
            Item = pm.Item,
            ItemGroup = pm.ItemGroup,
            myCollection;

        requests = [];

        // Load a collection to memory from a JSON file on disk (say, sample-collection.json)
        myCollection = new Collection(JSON.parse(fs.readFileSync(file).toString()));

        function removeMatching(originalArray, expression) {
            var j = 0;
            newArray = [];

            var regex = new RegExp(expression);
            originalArray.find(function(element) {
                item = regex.test(element);
                // console.log(item);
                if (item == false) {
                    newArray.push(element);
                }
            });
            return newArray;

        }
        dfs = function(item, requests) { // fn -> Depth first search

            // Check if this is a request
            if (Item.isItem(item)) {
                if (item.request && Request.isRequest(item.request)) {
                    requests.push(item.request);
                }
            }
            // Check if this is a nested folder
            else if (ItemGroup.isItemGroup(item)) {
                // Do a depth first search for requests on the nested folder
                item.items.each(function(item) {
                    dfs(item, requests);
                })
            }

            return requests;
        };


        myCollection.items.each(function(item) {
            // Check if this is a request at the top level
            if (Item.isItem(item)) {
                dfs(item, requests);
            }
            // Check if this is a folder at the top level
            else if (ItemGroup.isItemGroup(item)) {
                requests.push(dfs(item, requests));
            }
        });


        endpoints = {};
        requests.forEach(function(item) {
            var endpoint = {};

            if (item.url && item.url.path) {

                var url = removeMatching(item.url.path, /\{.*/i);
                endpoint.key = item.method.toLowerCase() + '_' + url.join('_');
                endpoint.key = endpoint.key.replace(/[_+]$/g, '');

                url = item.url.host + "/" + item.url.path.join('/');

                endpoints[endpoint.key] = item.method.toUpperCase() + ':' + url;
            }

        });

        fs.writeFileSync('app/' + outputfile, JSON.stringify(endpoints));
    }
});


// Default task
gulp.task('default', ['css', 'js', 'vendor', 'build-server-api']);