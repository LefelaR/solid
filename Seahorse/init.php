<?php
error_reporting(E_ALL);

global $context;

function lastresort_handler($e)
{
    if (contains($e->getMessage(), 'Call to undefined method')) {
        header("HTTP/1.1 404 Not Found");
        print "<h2>404 not found</h2>";
        die();
    } else {
        echo $e->getMessage();
    }
}
set_exception_handler('lastresort_handler');

function enable_noExecute()
{
    $block = true;

    if (isset($_REQUEST['noun'])) {
        if ($_REQUEST['noun'] !== '') {
            $block = false;
        }
    }

    if ($block !== false) {
        header('HTTP/1.0 403 Forbidden');
        exit('Forbidden');
    }
}

function autoload($class_name)
{
    if (file_exists('components/' . $class_name . '.php')) {
        require_once 'components/' . $class_name . '.php';
        return;
    }

    $seahorsepath = 'seahorse/';

    if (file_exists($seahorsepath . '/components/' . $class_name . '.php')) {
        require $seahorsepath . '/components/' . $class_name . '.php';
        return;
    }

    global $seahorse;

    foreach($seahorse->autoloadfolders as $key =>$val){
        $dir = getcwd();
        if ( strpos($dir, 'app') !== false){
            $val = '../' . $val;
        }
         if (file_exists($val . '/'. $class_name . '.php')) {
              require  $val . '/' . $class_name . '.php';
         }
         if (file_exists( $val . '/components/'. $class_name . '.php')) {
              require $val . '/components/' . $class_name . '.php';
         }
    }

    if (file_exists('crest/components/' . $class_name . '.php')) {
        require 'crest/components/' . $class_name . '.php';
        return;
    }

    $x = 1;
}

spl_autoload_register('autoload');

function addPHPExtension($file)
{
    if (!contains($file, '.php')) {
        return $file . '.php';
    }
    return $file;
}

function removePHPExtension($file)
{
    if (contains($file, '.php')) {
        return str_replace('.php', '', $file);
    }
    return $file;
}

function url($string)
{
    global $context;
    if (contains($string, '.')) {
        $string = 'app/' . $string;
    }
    echo $context->siteroot . $string;
}

function pluginUrl($plugIn,$string)
{
    global $context;
    $string = 'p-' . $plugIn . '/' . $string;
    echo $context->siteroot . $string;
}

function buildPluginUrl($plugIn,$string)
{
    global $context;
    $string = 'p-' . $plugIn . '/' . $string;
    return $context->siteroot . $string;
}

function buildRealPluginUrl($plugIn,$string)
{
    global $context;
    $string =  $_SERVER["DOCUMENT_ROOT"] . $context->siteroot  . 'plugins/' . $plugIn . '/' . $string;
    return  $string;
}

function root($string)
{
    global $context;
    if (contains($string, '.')) {
        $string = $string;
    }
    echo $context->siteroot . $string;
}

function buildurl($string)
{
    
    global $context;
    return $context->siteroot . $string;
}

function redirect($url=null, $params=null)
{
    ob_clean();
    if (isset($url)) {
        Header('Location: ' . buildurl($url) . '?' . $params);
    }
    else{
        global $seahorse;
        Header('Location: ' .  buildurl($seahorse->server->RedirectPage) . '?' . $params);
    }
    die();
}

function useClass($classname)
{

    $classes = explode(',', $classname);
    foreach ($classes as $item) {
        $class = $item;
        if (contains($item, '/')) {
            $items = explode('/', $item);
            $class = $items[1];
        }

        if (class_exists($class) == false) {
            global $context;
            if (file_exists($context->siteroot . 'components/' . trim($item) . '.php')) {
                require $context->siteroot . 'components/' . trim($item) . '.php';
            } else {
                require  'api/v1/components/' . trim($item) . '.php';
            }
        }
    }
}

function enable_authorize($role = null)
{
    $ret = false;
    global $seahorse;

    if (!isset($seahorse->profile)) return $seahorse->server->redirectToLogin();

    if (!isset($role)) {
        return;
    }

    // if ($seahorse->profile->locked) {
    //     if ($seahorse->profile->locked == 1) {
    //         global $seahorse;
    //         global $context;
    //         if ($context->realurl != 'account/profile') {
    //             ob_clean();
    //             Header('Location: ' . buildurl('account/profile'));
    //             die();
    //         }
    //     }
    // }

    $requiredRoles = explode(',', strtolower($role));
    $userRoles = explode(',', strtolower($seahorse->profile->user_role));
    foreach ($userRoles as $userRole) {
        if (in_array($userRole, $requiredRoles) === true) {
            return;
        }
    }

    $seahorse->server->redirectToLogin();
}
