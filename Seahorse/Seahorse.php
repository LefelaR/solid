<?php

require_once 'init.php';
require_once 'includes/strings.inc.php';
require_once 'includes/array.inc.php';

class Seahorse
{
    public $server;
    public $config;
    public $profile;
    public $autoloadfolders=[];
    public $Version = '2.0';

    public function __construct(SeahorseConfig $config)
    {
        $this->config = $config;
        $this->server = new Server();
        $this->profile = null; // not used?
       // set_include_path( $_SERVER['DOCUMENT_ROOT'] .  $this->config->siteroot . 'app/');
    }

    public function listen(string $siteroot, string $layout): void
    {
       // ob_start("Minify_Html");
       
        $this->server->listen($siteroot, $layout);
    }

    public function registerProfileHandler(string $profileHandlerClass): void
    {
        $this->server->registerProfileHandler($profileHandlerClass);
    }

    public function registerAPI(string $key, string $baseurl, string $jsonEndpoints): void
    {
        $this->server->registerAPI($key, $baseurl, $jsonEndpoints);
    }

    public function registerViewAction(string $actionClass) : void
    {
        $this->server->registerViewAction($actionClass);
    }

    public function registerLoginPage(string $loginUrl): void
    {
        $this->server->registerLoginPage($loginUrl);
    }

    public function registerNotFoundPage(string $loginUrl): void
    {
        $this->server->registerNotFoundPage($loginUrl);
    }

    public function registerRedirectPage(string $loginUrl): void
    {
        $this->server->registerRedirectPage($loginUrl);
    }

    public function registerPlugin(string $name, string $actionClass) : void
    {
        $this->server->addPlugin($name,$actionClass);
    }

    public function redirect(string $url, string $toUrl): void
    {
        $this->server->addRedirect($url, $toUrl);
    }
    
    public function addLayoutMap($url, $toUrl){
        $this->server->addLayoutMap($url, $toUrl);
    }
   
    // public function setAuthCookie($profile, $duration=360){
    //     $authcookiename = $this->server->ProfileHandler->getAuthCookieName();
    //     setcookie($authcookiename, $profile, time()+$duration);
    // }

}
