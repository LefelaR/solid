<?php

class SeahorseController
{

    public function __construct()
    {

    }

    public function ClientContext2($id)
    {

        $content = 'if(!window.seahorse){
           window.seahorse = {};
        }';
        $content .= 'window.seahorse.profile = ' . json_encode($this->seahorse->profile) . ';';

        $content .= 'window.seahorse.siteroot = "' . buildurl('') . '";';
        $content .= 'window.seahorse.siteversion = "' . $this->seahorse->config->siteversion . '";';
        $content .= 'window.seahorse.seahorseversion = "' . $this->seahorse->config->siteversion . '";';

        if (strtolower($this->context->action) != "clientcontext") {
            // $content .= 'window.seahorse.id = "' . $this->context->id . '";';
            // $content .= 'window.seahorse.view = "' . strtolower($this->context->action) . '";';
            // $viewjs = '/views/' . $this->context->path . '/' . strtolower($this->context->action) . '.js';
            // if (file_exists($viewjs)) {
            //     $content .= 'window.seahorse.viewjs = "' . $viewjs . '";';
            // }
            //$content .= 'window.seahorse.viewjs = "/views/' . $this->context->path . '/' . strtolower($this->context->action) . '.js' . '";';
        }

        if (isset($this->seahorse->server->API)) {
            foreach ($this->seahorse->server->API as $key => $item) {
                $myfile = fopen($item['endpoints'], "r") or die("Unable to open file!");
                $content .= ' window.seahorse.' . $key . ' = ';
                $content .= fread($myfile, filesize($item['endpoints']));
                $content = preg_replace('/\{\{url\}\}\//i', $item['url'], $content);
                $content .= ';';
                fclose($myfile);
            }
        }

        // $content .= '
        
        // ';
        ob_clean();
        return new ContentResult('text/javascript', $content);

    }

}