<?php

class Context
{
    public $siteroot;
    public $seahorsepath;
    public $path;
    public $controller;
    public $action;
    public $view;
    public $realurl;
    public $file;
    public $bypass = false;
    public $id=0;

    function debug()
    {
        foreach ($this as $key => $value) {
            $val = $key . ' : ' . $this->$key;
            $val = '<p>' . $val . '</p>';
            echo $val;
        }
    }
}
?>