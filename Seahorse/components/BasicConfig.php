<?php

class BasicConfig extends SeahorseConfig
{

    public function __construct($ver)
    {
        parent::__construct($ver);
    }

    public function InitEnvironment($state)
    {
        if ($state == 'dev') {
            $this->manifest = 'manifest_dev.json';
            $this->siteroot = '/seahorse/';            
            $this->siteurl = 'http://localhost:8080' . $this->siteroot;
        }
    }

    public function GetDatabase($state)
    {
        
        if($state == 'dev'){
            return array(
                'dbServer' => '192.168.1.50',
                'dbName' => 'database',
                'dbUser' => 'root',
                'dbPassword' => 'LOv1p3r',
            );
        }

     
    }

}
