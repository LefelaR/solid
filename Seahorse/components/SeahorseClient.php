<script>
<?php

    global $context;
    global $seahorse;

    $dir = getcwd();
     chdir($_SERVER['DOCUMENT_ROOT'] . $seahorse->config->siteroot . 'app/');

    unset($seahorse->profile->user_id);
    ?>

    if (!window.seahorse) {
        window.seahorse = {};
    }


    window.seahorse.profile = <?php echo json_encode($seahorse->profile); ?>;
    window.seahorse.siteroot = '<?php url(''); ?>';
    window.seahorse.id = '<?php echo $context->id; ?>';
    window.seahorse.view = '<?php echo strtolower($context->action); ?>';
    window.seahorse.returnUrl = '<?php echo $seahorse->server->RedirectPage; ?>';

    <?php
        $viewjs = buildurl( $context->prefix . 'views/' . $context->path . '/' . strtolower($context->action) . '.js');
        $realviewjs = $_SERVER['DOCUMENT_ROOT'] . $viewjs;
        $controllerpath = buildurl( $context->prefix . 'views/' . $context->path . '/' );
        if (file_exists($realviewjs)){
        ?>
            window.seahorse.viewjs = '<?php echo $viewjs; ?>';
        <?php
        }
        ?>
//   if (window.seahorse.profile == undefined) {
//       navigator.serviceWorker.controller.postMessage({
//           "name": "clear-cache"
//       });
//   }

    if (navigator.serviceWorker.controller) {
        navigator.serviceWorker.controller.postMessage({
            "name": "upgrade",
            "value": "<?php echo $seahorse->config->siteversion; ?>"
        });
    }
<?php

        $content = '';
        if (isset($seahorse->server->API)) {
 
            foreach ($seahorse->server->API as $key => $item) {
                $myfile = fopen($_SERVER["DOCUMENT_ROOT"] . $context->siteroot . $item['endpoints'], "r") or die("Unable to open file!");
                $content .= ' window.seahorse.' . $key . ' = ';
                $content .= fread($myfile, filesize($item['endpoints']));
                $content = preg_replace('/\{\{url\}\}\//i', $item['url'], $content);
                $content .= ';';
                fclose($myfile);
            }
        }
        echo $content;
        ?>
        
        if (window.seahorse.viewjs != null) {
            $i.ready({debug:3, spa:false, "controllerPath":"", "domainRoot": "<?php echo $controllerpath; ?>" , "controllerjs": window.seahorse.viewjs },function(){
                //$i.View.setController('route');
                //$i.DefaultRoute.handle({});
                // $i.ControllerHandler.run({ "spa":false, "view": "index.html", "controllerPath": "" });
                //ret = controller[method].call(controller, data);
                bindConnectionEvents();
            }); 
        }


function bindConnectionEvents() {
    window.addEventListener('online', checkOnlineStatus);
    window.addEventListener('offline', checkOnlineStatus);

    checkOnlineStatus();
}

function checkOnlineStatus(event) {
    var online = navigator.onLine;
    var saveButtons = document.querySelectorAll('[data-save],input[type="submit"]');
    if (!online)
        ShowAlert('warning',
            'Oops you are currently offline. Any changes you make while offline wont be synced until you reconnect.');

    for (var i = 0; i < saveButtons.length; i++) {
        var item = saveButtons[i];
        if (!online) {
            item.style.display = 'none';
        } else {
            item.style.display = '';
        }
    }
}

function getScript(source, callback) {
    var script = document.createElement("script");
    var prior = document.getElementsByTagName("script")[0];
    script.async = 1;

    script.onload = script.onreadystatechange = function(_, isAbort) {
        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            script = undefined;

            if (!isAbort) {
                if (callback) callback();
            }
        }
    };

    script.src = source;
    prior.parentNode.insertBefore(script, prior);
}

window.addEventListener('load', function() {

    // var $form = $("form[id]");
    // var id = $form.attr("id");
    // $i.bindForm();
});



function handleClick(selector, callback) {
    var td = document.querySelectorAll(selector);
    for (var i = 0; i < td.length; i++) {
        td[i].addEventListener("click", onClick, false);
    }

    function onClick(event) {
        event.stopPropagation();
        callback(event.target);
    }
}

function handleClickWithoutStop(selector, callback) {
    var td = document.querySelectorAll(selector);
    for (var i = 0; i < td.length; i++) {
        td[i].addEventListener("click", onClick, false);
    }

    function onClick(event) {
        callback(event.target);
    }
}


function buildFilteredUrl(endPoint, replacement) {
    var url = endPoint;
    var filter = "filter:";
    if (replacement) {
        for (var k in replacement) {
            if (replacement.hasOwnProperty(k)) {
                filter += k + ":" + replacement[k] + ";"
                url = url.replace("{{" + k + "}}", replacement[k]);
                url = url.replace("{" + k + "}", replacement[k]);
            }
        }
        url = url.replace("filter:filter", filter);

        var idx = url.lastIndexOf(";");
        if (idx != -1) {
            url = url.substr(0, idx);
            url += ".json";
        }
    } else {
        var idx = url.lastIndexOf("/");
        if (idx != -1) {
            url = url.substr(0, idx);
            url += ".json";
        }
    }
    return url;
}

function buildQsFilteredUrl(endPoint, replacement) {
    var url = endPoint;
    var filter = "?"
    if (replacement) {
        for (var k in replacement) {
            if (replacement.hasOwnProperty(k)) {
                filter += k + "=" + replacement[k] + "&"
                url = url.replace("{{" + k + "}}", replacement[k]);
                url = url.replace("{" + k + "}", replacement[k]);
            }
        }
        url = url.replace("filter:filter", filter);

        var idx = url.lastIndexOf("&")
        if (idx != -1) {
            url = url.substr(0, idx);
        }
    } else {
        var idx = url.lastIndexOf("/");
        if (idx != -1) {
            url = url.substr(0, idx);
        }
    }
    return url;
}

function checkPushSubscription() {
    if (typeof enableDialog === "undefined") {
        console.error('Cannot show push subscription dialog. Modal not found');
        return;
    }

    if (!Notification) return;
    if (Notification.permission != 'default') return;
    enableDialog("Enable Notifications",
        "Do you want to enable notifications? This will send you notifications when actions happen. For example when a booking is created.",
        "Enable Notifications", null,
        function(data) {
            if (data.result === true) {
                enableNotifications();
                enableDefaultNotifications();
            }
        });

    function enableDefaultNotifications() {

        var data = {
            "notifications":1,
            "articles":1,
            "newsletters":1,
            "status":1,
            "activities":1
        }

        var url = buildQsFilteredUrl(window.seahorse.admin["put_profiles_settings"] + 'filter:filter', {
            user_id: 0
        });

        ajax(url, data, function(data) {});
    }

    function enableNotifications() {

        if (window.seahorse) {
            if (window.seahorse.profile.access_token) {

                showNotification('', '');

                var d = {
                    siteroot: window.seahorse.siteroot,
                    access_token: window.seahorse.profile.access_token
                };
                navigator.serviceWorker.controller.postMessage({
                    "name": "register_user",
                    "value": d
                });
                navigator.serviceWorker.controller.postMessage({
                    "name": "checksubscription"
                });
            }
        }
    }
}

function ajax(url, postdata, onSuccess, onErrorHandler) {
    $i.ready({}, function() {
        $i.Data.getData(
            url,
            postdata,
            function(data) {
                //  console.table(data.Data);
                if (onSuccess) onSuccess(data);
            },
            errorHandler,
            setHeader
        );

        function errorHandler(xhr, err) {
            //  console.log(err);

            var code = 400;
            var message = '';
            if (xhr['responseJSON']) {
                if (xhr['responseJSON']['Status']) {
                    if (xhr['responseJSON']['Status']['Code']) {
                        code = xhr['responseJSON']['Status']['Code'];
                    }
                    if (xhr['responseJSON']['Status']['Message']) {
                        message = xhr['responseJSON']['Status']['Message'];
                    }

                }
            }

            if (message == 'Invalid access token provided.') {
                if (xhr.status == 401) {
                    signout();
                    return;
                }
            }

            if (onErrorHandler) {
                onErrorHandler(xhr, err);
            }
        }

        function setHeader(xhr) {
            // window.seahorse.profile = {};
            // window.seahorse.profile.access_token = '1ae8vtg4y5aa39d27t564aqfu6yvc215';
            if (window.seahorse.profile) {
                xhr.withCredentials = true;

                xhr.setRequestHeader(
                    "Authorization",
                    "Bearer " + window.seahorse.profile.access_token
                )
            }
        }
    });
}

function calculateTimeAgo(time) {
    switch (typeof time) {
        case 'number':
            break;
        case 'string':
            time = +new Date(time);
            break;
        case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
        default:
            time = +new Date();
    }
    var time_formats = [
        [60, 'seconds', 1], // 60
        [120, '1 minute ago', '1 minute from now'], // 60*2
        [3600, 'minutes', 60], // 60*60, 60
        [7200, '1 hour ago', '1 hour from now'], // 60*60*2
        [86400, 'hours', 3600], // 60*60*24, 60*60
        [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
        [604800, 'days', 86400], // 60*60*24*7, 60*60*24
        [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
        [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
        [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
        [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
        [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
        [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
        [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
        token = 'ago',
        list_choice = 1;

    if (seconds == 0) {
        return 'Just now'
    }
    if (seconds < 0) {
        seconds = Math.abs(seconds);
        token = 'from now';
        list_choice = 2;
    }
    var i = 0,
        format;

    while (format = time_formats[i++])
        if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
        }
    return time;
}


function signout() {
    document.cookie = "crest_auth=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
    ShowAlert('success', 'you are signed out', function() {
        document.location.href = window.seahorse.siteroot;
        $i.Cache.clear();
    });

}




function renderBreadcrumb(items) {

    if (items.length == 0) return;

    var container = document.querySelector('[data-breadcrumbcontainer] .breadcrumb');
    if (!container) return;

    container.innerHTML = '';

    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
    var breadcrumb_item = document.createElement('li');
    breadcrumb_item.classList.add('breadcrumb-item');

    items.forEach(item => {
        var item_clone = breadcrumb_item.cloneNode(true);
        if (item.active == 1) {
            item_clone.classList.add('active');
            item.url = '';
        }

        if (item.url) {
            var urlNode = document.createElement('a');
            urlNode.setAttribute('href', item.url);
            urlNode.textContent = item.title;
            item_clone.appendChild(urlNode);
        } else {
            item_clone.textContent = item.title;

        }

        container.appendChild(item_clone);
    });

}

function loadMenu() {
    var menu = document.querySelectorAll('[data-menutarget]');
    var mustHidePlaceholder = false;
    menu.forEach(function(element) {
        var targetName = element.getAttribute('data-menutarget');
        var target = document.querySelectorAll(targetName);

        if (target) {
            target.forEach(el => {
                musthidePlaceholder = true;
                el.appendChild(element);
                element.style.display = '';
                el.style.display = '';
                el.style.visibility = '';
            });
        }
    });

    if (mustHidePlaceholder == true) {
        var target = document.querySelector('[data-menuplaceholder]');
        if (target) {
            target.style.display = 'none';
        }
    }
}


function handleSaveForm() {

    var hForm = document.querySelector("form");
    var event; // The custom event that will be created

    if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent("submit", true, true);
    } else {
        event = document.createEventObject();
        event.eventType = "submit";
    }

    event.eventName = "submit";

    if (document.createEvent) {
        hForm.dispatchEvent(event);
    } else {
        hForm.fireEvent("on" + event.eventType, event);
    }


    event.stopPropagation();
}

function touchsense() {

    navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;

    if (navigator.vibrate) {
        // vibration API supported

        var elements = document.querySelectorAll('a, button, [data-id]');

        elements.forEach(element => {
            element.addEventListener('click', function() {
                window.navigator.vibrate(20);
                var el = this.querySelector('i');
                if (el) {
                    el.classList.add('fa-beat');
                }
            })

        });
    }


}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function toggleAuthorizedElements() {
    if (window.seahorse.profile) {
        var elements = document.querySelectorAll('[data-role]');
        elements.forEach(function(element) {
            var elRoles = element.getAttribute('data-role').split(',');
            var userRoles = window.seahorse.profile.user_role.split(',');
            userRoles.forEach(function(role) {
                if (elRoles.indexOf(role) !== -1) {
                    element.removeAttribute('hidden');
                    element.style.visibility = '';
                    element.style.display = '';
                }
            });
        });

        var elementl = document.querySelectorAll('[data-loggedout]');
        elementl.forEach(function(elementlogout) {
            elementlogout.setAttribute('hidden', '');
        });
    }
}

window.addEventListener('onDataSplashed', function() {
    touchsense();
});

document.addEventListener('DOMContentLoaded', function() {
    touchsense();
    toggleAuthorizedElements();
});
 </script>

 <?php 
    chdir($dir);
 ?>