<?php

class ActionResult implements IActionResult
{
    public function __construct($view, $model = null, $layout = '', $seo = null)
    {
        $this->view = $view;
        $this->model = $model;

        if (is_string($layout)) {
            $this->layout = $layout;
        } else if (is_array($layout)) {
            $this->seo = $layout;
        }
 
        if (is_array($seo)) {
            $this->seo = $seo;
            if (is_array($layout)) {
                throw new Exception('Layout is used as seo, please make sure your params are correct.');
            }
        }

    }

    public $view;
    public $model;
    public $layout;
    public $seo = null;
}
