<?php

class FormHandler {

    public $param = array();

    public function __construct() {



        $postData = file_get_contents('php://input');
        $post = null;


        if (!empty($postData)) {
            //$postData = array_map('stripslashes', $postData);
            $post = explode('&', $postData);

            for ($idx = 0; $idx < sizeof($post); $idx++) {
                try {

                    
                    $pair = explode("=", $post[$idx]);
                    
                    if (endswith($pair[0],'%5B%5D'))
                    {
                        $index = isset($pair[0]) ? $pair[0] : "";
                        $index = str_replace('%5B%5D','', $index);
                    }
                    else
                    {
                        $index = isset($pair[0]) ? $pair[0] : "";
                       
                    }
                    
                    $value = isset($pair[1]) ? $pair[1] : "";
                     
                    
                    if (array_key_exists($index, $this->param)) {
                        if ($value == "Select an option")
                            $value = "0";

                        // if (!empty($value)) {
                            $this->param [$index] = $this->param [$index] . "," . urldecode(trim($value));
                        // } 
                    } else {

                        if ($value == "Select+an+option")
                            $value = "0";

                        $this->param [$index] = urldecode( trim($value ));
                    }
                    
                    
                    
                } catch (Exception $e) {
                    $x = $e . getMessage();
                    Logger::Log($x);
                    return false;
                }
            }
        } else {
            $this->param = $_POST;
        }
     
    }

    public function __get($name) {

        if (isset($this->param[$name])) {
            $res = $this->param[$name];
        } else {
            $res = false;
            //die ("The property " . $name . " does not exist within the form");
        }
        return trim($res);
    }

    public function __set($name, $val) {
        if (isset($this->param[$name])) {
            $this->param[$name] = $val;
            $res = true;
        } else {
            $this->param[$name] = $val;
            $res = false;
        }
        return $res;
    }

    public function __isset($name) {
        return isset($this->param[$name]);
    }

    public function __unset($name) {
        unset($this->param[$name]);
    }

    public function __call($name, $var) {
        // add code to simulate function call
        // return TRUE for success
    }

    // public function onPosted($trigger = '') {
    //     if (empty($trigger)) {
    //         if (array_count_values($this->param) > 0) {
    //             return TRUE;
    //         }
    //         return false;
    //     } else {
    //         return ( isset($this->param[$trigger]) && $this->param ? true : false );
    //     }
    // }
  
}

?>