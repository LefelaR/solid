<?php

interface IViewAction{

    function handleAction($command, $context) : string;
}

?>