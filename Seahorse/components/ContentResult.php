<?php


class ContentResult implements IActionResult
{
    public function __construct($header, $content)
    {
        $this->header = $header;
        $this->content = $content;
    }

    public $header;
    public $content;    
}

?>