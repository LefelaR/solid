<?php

abstract class SeahorseConfig
{

    public $manifest = 'manifest.json';
    public $siteroot = '/';
    public $siteurl = 'http:/localhost:8080/';
    public $state = 'dev';
    public $dbConnection;
    public $siteversion;
    public $seahorseversion = '1.1';

    abstract protected function InitEnvironment($state);
    abstract protected function GetDatabase($state);

    public function __construct($ver)
    {


        $this->siteversion = $ver;
        $this->state = $this->GetReleaseState();

        $this->dbConnection = $this->InitEnvironment($this->state);
        $this->dbConnection = $this->GetDatabase($this->state);
    }

    public function GetReleaseState()
    {
        $host = $_SERVER['HTTP_HOST'];

        if (contains($host, 'localhost') || contains($host, '127.0.0.1')) {
            return 'dev';
        } else if (contains($host, 'staging')) {
            return 'staging';
         } else {
            return 'live';
        }
    }
}
 