<?php

//enable_noExecute();

class ErrorResult extends Result
{
    /**
     * constructor accepting either a message or an error
     **/
    public function __construct($message = null, $code = 400)
    {
        global $seahorse;
        if ($seahorse->config->state =='dev'){
            var_dump($message);
        }
        parent::__construct($this->data);
    }

}