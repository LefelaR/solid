<?php

include 'seahorse/Seahorse.php';

 session_start();
//global $config;
$config = new Config('1.90');

global $seahorse;
$seahorse = new Seahorse($config);
$seahorse->registerProfileHandler(UserProfileHandler::class);

if ($config->state == "live") {
    $baseurl = "https://api-sandbox.grnconnect.com";
} else if ($config->state == "staging") {
    $baseurl = "https://api-sandbox.grnconnect.com";
} else {
 $baseurl = 'http://localhost:8080/ria-web/api/v1/';
  
}
 $file = 'serverapi.json';
$seahorse->registerAPI('admin', $baseurl, $file);
// $seahorse->registerAPI('ria', $baseurl, $file);
//$seahorse->registerViewAction(ViewAction::class);
//$seahorse->registerLoginPage('account/signin');
$seahorse->registerNotFoundPage('index/notfound');
$seahorse->listen($config->siteroot, 'layouts/layout.php');




//read url
// split it into controller/action
// or folder/folder/controller/action
// or defaults to index/index

//seahorse: load controller 
//seahorse: calls action
//action returns actionresult(view,model,layout (optional))
//seahorse: load page returned by controller
//seahorse: loads layout (as defaulted or specified in controller)
//seahorse: merges view into @page located in layout
 