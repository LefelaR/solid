<?php


require_once 'crest/Crest.php';
include 'components/Config.php';


$options = [
    'Debug' => true,
    'Version' => 'v1'
];

$crest = new CRest($options);
$crest->addNouns('projects/activity');
$crest->addNouns('projects/iterations');
$crest->addNouns('link');

$crest->addNouns('site');
$crest->addNouns('help/topics');
$crest->addNouns('help/categories');
$crest->addNouns('profiles');
$crest->addNouns('profiles/settings');
$crest->addNouns('profiles/members');
$crest->addNouns('projects/comments');
$crest->addNouns('public/articles');



//$crest->enable_BearerAuthentication();
//$crest->RegisterAspect(HTTPCache::class);
//$crest->RegisterAspect(Performance::class);
//$crest->RegisterAspect(ProtectorFile::class);
//$crest->RegisterAspect(LogAll::class);
//$crest->RegisterAspect(Tests::class);


$crest->Init();

$exceptions = $exceptions = ["public","authentication","projects","activity","site","articles"];
$crest->enable_BearerAuthentication($exceptions);
setProfile($exceptions);

$crest->execute_RestHandler();


function setProfile($exceptions)
{
    global $Context;
    $containsSearch = count(array_intersect($Context->Route->noun, $exceptions)) == count($Context->Route->noun);

    $headers = getallheaders();   

    if ($containsSearch === true && !isset($headers[Header_Authorization])) {
        return;
    }
    $Context->Profile = getProfile();

}

function getProfile()
{
    global $Context;

    $headers = getallheaders();

    if (!empty($headers[Header_Authorization])) {
        $token = $headers[Header_Authorization];
        $token = str_replace('Bearer ', '', $token);
    } else {
        new ErrorResult('No ' . Header_Authorization . ' header was provided. You need to add the header you received from the login.', 401);
    }

    useClass('User,Helpers/FileHelper', $Context);
    $user = new User();
    $data = $user->AuthenticateToken($token);
    $data = json_decode($data->GetResult());
    $Context->Profile = new stdClass();
    $Context->Profile = $data->Data[0];
    return $Context->Profile;

}
